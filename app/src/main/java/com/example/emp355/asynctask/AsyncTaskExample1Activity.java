package com.example.emp355.asynctask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.listener.AsyncTaskListener;


public class AsyncTaskExample1Activity extends AppCompatActivity implements AsyncTaskListener {

    private EditText mEnterNumber;
    private TextView mResult;
    private ProgressDialog mProgressDialog;
    private DataLoader1 mDataLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_example1);

        mEnterNumber = findViewById(R.id.edittext_enternum);
        Button enter = findViewById(R.id.button_enter);
        mResult = findViewById(R.id.textview_result);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              mDataLoader=  new DataLoader1(AsyncTaskExample1Activity.this);
              mDataLoader.execute(mEnterNumber.getText().toString());
            }
        });


    }

    @Override
    public void onTaskStarted() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Uploading");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    public void taskProgress(Integer... integers) {
        Log.d("update", String.valueOf(integers[0]));
        mResult.setText(String.valueOf(integers[0]));

    }

    @Override
    public void onTaskCompleted(String result) {
        mProgressDialog.dismiss();
        Log.d("comp", "Completed");
        mResult.setText(result);
    }

    @Override
    protected void onDestroy() {

        //removing listener from DataLoader1
        //not cancelling asynctask
        //no activity methods are being called
        mDataLoader.removeListener();
        super.onDestroy();
    }

    //AysncTask class
    public static class DataLoader1 extends AsyncTask<String, Integer, String> {


        private AsyncTaskListener asyncTaskListener;

        private DataLoader1(AsyncTaskListener asyncTaskListener) {
            this.asyncTaskListener = asyncTaskListener;
        }

        private void removeListener()
        {
            asyncTaskListener=null;
        }

        // invoked on the main UI thread before the task is executed.
        @Override
        protected void onPreExecute() {
            if (asyncTaskListener != null)
                asyncTaskListener.onTaskStarted();
        }

        //invoked on the background thread immediately after onPreExecute() finishes its execution.
        @Override
        protected String doInBackground(String... values) {

            for (int i = 0; i <= Integer.parseInt(values[0]); i++) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return values[0] + " Completed";
        }


        //invoked on the main UI thread after a call to publishProgress(Progress…).
        @Override
        protected void onProgressUpdate(Integer... integers) {
            if (asyncTaskListener != null)
                asyncTaskListener.taskProgress(integers);
        }

        //invoked on the main UI thread after the background operation finishes in the doInBackground method.
        @Override
        protected void onPostExecute(String values) {
            if (asyncTaskListener != null)
                asyncTaskListener.onTaskCompleted(values);
        }
    }
}
