package com.example.emp355.uipractice;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.example.emp355.R;

public class FontTextView extends TextView {

    /*https://stackoverflow.com/questions/6631105/when-is-view-onmeasure-called*/
    //refer to this link for custom view methods

    private Paint mTextPaint;

    public FontTextView(Context context) {
        this(context,null,0,0);
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0,0);
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public FontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    //initializes paint object and its properties
    private void init(Context context, AttributeSet attrs) {

        mTextPaint=new Paint();

        mTextPaint.setStyle(Paint.Style.FILL);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth,parentHeight);

    }


    //less calculations should be included in this method
    //should be clean as possible
    @Override
    protected void onDraw(Canvas canvas) {

        mTextPaint.setTextSize(getHeight());
        Log.d("dimensions", "height"+getHeight());
        Log.d("dimensions", "width"+getWidth());

        canvas.drawText("1",0,getHeight(),mTextPaint);
    }
}
