package com.example.emp355.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.emp355.R;

public class CircleView extends View{

    private int mCircleColor;
    private Paint mCirclePaint;

    public CircleView(Context context) {
        super(context);
        init(context,null);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);

    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }


    private void init(Context context,AttributeSet attributeSet)
    {
        mCirclePaint=new Paint();
        TypedArray mArray=context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CircleView,0,0);
        try {
            mCircleColor=mArray.getInteger(R.styleable.CircleView_circlecolor,0);
        }
        finally {
            mArray.recycle();
        }


        mCirclePaint.setStyle(Paint.Style.FILL);
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(mCircleColor);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int mYCoordinate=this.getHeight()/2;//(X,Y)to mark center
        int mXCoordinate=this.getWidth()/2;

        int mRadius=0;
        if(mXCoordinate>mYCoordinate) {
            mRadius = mYCoordinate - 10;
        }
        else {
            mRadius=mXCoordinate-10;
        }


        canvas.drawCircle(mXCoordinate,mXCoordinate,mRadius,mCirclePaint);

    }
}
