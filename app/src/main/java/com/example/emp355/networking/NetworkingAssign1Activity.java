package com.example.emp355.networking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.JSONUtils;
import com.example.emp355.R;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Matched_Substring;
import com.example.emp355.model.ObjectData;
import com.example.emp355.model.Prediction;
import com.example.emp355.model.Structured_Formatting;
import com.example.emp355.model.Term;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.emp355.Constant.SEARCH_DATA;

public class NetworkingAssign1Activity extends AppCompatActivity {


    private ProgressDialog mProgressDialog;
    private EditText mSearchText;

    //API  KEY
    private static final String API_KEY="AIzaSyAPiG2fwDSl9qn5EhkY48EMFU67nKzOciI";

    //BASE URL for autocomplete place details
    private static final String URL="https://maps.googleapis.com/maps/api/place/queryautocomplete/json?key="+API_KEY+"&input=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_assign1);

        mSearchText=findViewById(R.id.edittext_search);

    }

    //builds the final url to fetch data
    private String returnFinalUrl(String text)
    {
        return URL+text;

    }

    //onClick for search button
    public void searchData(View view)
    {
        Log.d("text", mSearchText.getText().toString());
        if(mSearchText.getText().toString().equals("") )
        {

            Toast.makeText(this, "Type Something", Toast.LENGTH_LONG).show();
        }
        else {
            String finalUrl=returnFinalUrl(mSearchText.getText().toString());
            Log.d("final", finalUrl);
            if(checkInternetConnectivity())
            {
                //networking operation is carried out on a background thread for autocomplete
                new MyAsync().execute(finalUrl);
            }
        }


    }

    // Checks for the internet connection.
    private boolean checkInternetConnectivity()
    {
        // Get Connectivity Manager
        ConnectivityManager connectivityManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Details about the currently active default data network
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        if(networkInfo==null)
        {
            Toast.makeText(this, "No default network is currently active", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!networkInfo.isAvailable()) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
            return false;
        }

        if(!networkInfo.isConnected())
        {
            Toast.makeText(this, "Network is not connected", Toast.LENGTH_LONG).show();
            return false;

        }

        Toast.makeText(this, "Network Connected", Toast.LENGTH_LONG).show();
        return true;

    }

    //async task to fetch data from url
    public class MyAsync extends AsyncTask<String,Void,ObjectData>
    {
        //shows the progress dialog while fetching data from url
        @Override
        protected void onPreExecute() {
            mProgressDialog=new ProgressDialog(NetworkingAssign1Activity.this);
            mProgressDialog.setMessage("Fetching");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        //fetches the data from url
        @Override
        protected ObjectData doInBackground(String... values) {

            InputStream inputStream=null;
            HttpURLConnection urlConnection=null;
            String response=null;
            ObjectData objectData=null;

            try
            {
                /* forming the java.net.URL object */
                URL url=new URL(values[0]);

                urlConnection=(HttpURLConnection) url.openConnection();

                /* for Get request */
                urlConnection.setRequestMethod("GET");

                int statusCode=urlConnection.getResponseCode();

                Log.d("status", "doInBackground: "+statusCode);
                /* 200 represents HTTP OK */
                if(statusCode==200)
                {
                    inputStream=new BufferedInputStream(urlConnection.getInputStream());
                    response=convertInputStreamToString(inputStream);
                    objectData=jsonParsing(response);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            Log.d("resp", response);
            return objectData;

        }

        //parsing of json to java object
        private ObjectData jsonParsing(String jsonStringData)
        {

            ObjectData objectData=new ObjectData();

            JSONObject jsonObject= JSONUtils.getJSONObject(jsonStringData);


            objectData.setStatus(JSONUtils.getStringfromJSON(jsonObject,"status"));

            objectData.setError_message(JSONUtils.getStringfromJSON(jsonObject,"error_message"));


            JSONArray jsonPredictionsArray =JSONUtils.getJSONArray(jsonObject,"predictions");

            List<Prediction> predictionsList=new ArrayList<Prediction>();

            for (int i = 0; i < JSONUtils.getLengthofJSONArray(jsonPredictionsArray); i++)
            {


                JSONObject jsonPredictionObject =JSONUtils.getJSONObject(jsonPredictionsArray,i);

                Prediction prediction=new Prediction();

                prediction.setDescription(JSONUtils.getStringfromJSON(jsonPredictionObject,"description"));
                prediction.setId(JSONUtils.getStringfromJSON(jsonPredictionObject,"id"));
                prediction.setPlace_id(JSONUtils.getStringfromJSON(jsonPredictionObject,"place_id"));
                prediction.setReference(JSONUtils.getStringfromJSON(jsonPredictionObject,"reference"));



                JSONArray jsonTypesArray = JSONUtils.getJSONArray(jsonPredictionObject,"types");


                List<String> typesList=new ArrayList<String>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonTypesArray); j++)
                {

                    typesList.add(JSONUtils.getStringObject(jsonTypesArray,j));
                }


                prediction.setTypes(typesList);



                JSONArray jsonMatchedSubstringsArray = JSONUtils.getJSONArray(jsonPredictionObject,"matched_substrings");

                List<Matched_Substring> matchedSubstringsList=new ArrayList<Matched_Substring>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonMatchedSubstringsArray); j++)
                {

                    JSONObject jsonMatchedSubstringObject=JSONUtils.getJSONObject(jsonMatchedSubstringsArray,j);

                    Matched_Substring matched_substring=new Matched_Substring();

                    matched_substring.setLength(JSONUtils.getIntegerfromJSON(jsonMatchedSubstringObject,"length"));

                    matched_substring.setOffset(JSONUtils.getIntegerfromJSON(jsonMatchedSubstringObject,"offset"));


                   matchedSubstringsList.add(matched_substring);
                }


                prediction.setMatched_substrings(matchedSubstringsList);







                JSONArray jsonTermsArray = JSONUtils.getJSONArray(jsonPredictionObject,"terms");

                List<Term> termsList=new ArrayList<Term>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonTermsArray); j++)
                {

                    JSONObject jsonTermsObject=JSONUtils.getJSONObject(jsonTermsArray,j);

                    Term term=new Term();

                    term.setOffset(JSONUtils.getIntegerfromJSON(jsonTermsObject,"offset"));

                    term.setValue(JSONUtils.getStringfromJSON(jsonTermsObject,"value"));


                    termsList.add(term);
                }


                prediction.setTerms(termsList);










                JSONObject jsonStructFormatObject =JSONUtils.getJSONObject(jsonPredictionObject,"structured_formatting");


                Structured_Formatting structured_formatting = new Structured_Formatting();


                structured_formatting.setMain_text(JSONUtils.getStringfromJSON(jsonStructFormatObject,"main_text"));
                structured_formatting.setSecondary_text(JSONUtils.getStringfromJSON(jsonStructFormatObject,"secondary_text"));


                JSONArray jsonMainTextSubStringsArray=JSONUtils.getJSONArray(jsonStructFormatObject,"main_text_matched_substrings");

                List<Matched_Substring> mainTextSubStringsList=new ArrayList<Matched_Substring>();

                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonMainTextSubStringsArray); j++)
                {
                    JSONObject jsonMainTextSubStringsObject=JSONUtils.getJSONObject(jsonMainTextSubStringsArray,j);

                    Matched_Substring mainTextSubstrings=new Matched_Substring();

                    mainTextSubstrings.setOffset(JSONUtils.getIntegerfromJSON(jsonMainTextSubStringsObject,"offset"));
                    mainTextSubstrings.setLength(JSONUtils.getIntegerfromJSON(jsonMainTextSubStringsObject,"length"));

                    mainTextSubStringsList.add(mainTextSubstrings);
                }

                structured_formatting.setMain_text_matched_substrings(mainTextSubStringsList);


                prediction.setStructured_formatting(structured_formatting);



                predictionsList.add(prediction);
            }

           objectData.setPredictions(predictionsList);


            return objectData;
        }


        //converts the inputstream from url to string
        private String convertInputStreamToString(InputStream inputStream)
        {
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            String line="";
            String result="";

            try
            {
                while((line=bufferedReader.readLine())!=null)
                {
                    result=result+line;
                }
                if(inputStream!=null)
                {
                    inputStream.close();
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            return result;
        }

        //dismisses the dialog
        @Override
        protected void onPostExecute(ObjectData objectData) {
            mProgressDialog.dismiss();


            if(objectData!=null)
            {
                //control moves to next NetworkingShowDetailsActivity for showing results
                if(objectData.getError_message().equals("error_message"))
                {
                    Intent intent=new Intent(NetworkingAssign1Activity.this,NetworkingShowDetailsActivity.class);
                    intent.putExtra(SEARCH_DATA, objectData);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(NetworkingAssign1Activity.this,objectData.getError_message(),Toast.LENGTH_LONG).show();
                }


            }
            else {
                Toast.makeText(NetworkingAssign1Activity.this,"Failed to Fetch data",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }
}

