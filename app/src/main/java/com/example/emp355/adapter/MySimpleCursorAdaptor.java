package com.example.emp355.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.contentprovider.UserEntry;
import com.example.emp355.listener.OnItemClickListener;

public class MySimpleCursorAdaptor extends SimpleCursorAdapter {

    private Context mContext;
    private int mLayout;
    private OnItemClickListener onItemClickListener;

    public MySimpleCursorAdaptor(Context context, int layout, Cursor c, String[] from, int[] to, int flags,OnItemClickListener onItemClickListener) {
        super(context, layout, c, from, to, flags);

        mContext=context;
        mLayout=layout;
        this.onItemClickListener=onItemClickListener;
    }


    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();//return 1
    }


    @Override
    public int getCount() {
        return super.getCount();
    }



    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder=(ViewHolder) view.getTag();


            holder.fullName.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_FULLNAME)));
            holder.age.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_AGE)));
            holder.contactno.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_CONTACTNO)));
            holder.email.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_EMAIL)));
            holder.gender.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_GENDER)));

            int pos =cursor.getPosition();
            holder.update.setTag(pos);
            holder.delete.setTag(pos);


    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
         View view=LayoutInflater.from(mContext).inflate(mLayout,parent,false);

        ViewHolder viewHolder=new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    /*If you are overriding newView() and bindView(), you don't need to do anything
    extra in getView(). CursorAdapter has an implementation of getView() that delegates
    to newView() and bindView() to enforce the row recycling.
*/

    /*findViewById() maybe called frequently during the scrolling of ListView, which can slow
    down performance. Even when the Adapter returns an inflated view for recycling, you
    still need to look up the elements and update them. To avoid this,
    ViewHolder pattern is useful.
*/
    public class ViewHolder implements View.OnClickListener {
        public TextView fullName,age,contactno,email,gender;
        Button update,delete;
        public ViewHolder(View view)
        {
            fullName = view.findViewById(R.id.text_user_fname);
            age = view.findViewById(R.id.text_user_age);
            contactno = view.findViewById(R.id.text_user_contactno);
            email = view.findViewById(R.id.text_user_email);
            gender = view.findViewById(R.id.text_user_gender);
            update = view.findViewById(R.id.btn_update);
            delete = view.findViewById(R.id.btn_delete);

            update.setOnClickListener(this);
            delete.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

                if(onItemClickListener!=null)
                {
                    if(v.getTag()!= null && v.getTag() instanceof Integer) {
                        Cursor cursor= (Cursor) getItem((Integer) v.getTag());
                        onItemClickListener.onItemClick(v,cursor);
                    }
                }

        }
    }





}
