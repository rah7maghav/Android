package com.example.emp355.email;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SendEmailActivity extends AppCompatActivity {

    private EditText mEditTextTo, mEditTextSubject, mEditTextBody;
    private Uri mSelectedImage;
    private ArrayList<Uri> list=new ArrayList<>();
    private static final int REQUEST_TAKE_PHOTO_GALLERY=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);

        mEditTextTo =findViewById(R.id.edittext_to);
        mEditTextSubject =findViewById(R.id.edittext_subject);
        mEditTextBody =findViewById(R.id.edittext_body);

    }

    public void selectAttachment(View view)
    {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , REQUEST_TAKE_PHOTO_GALLERY);

    }

    public void sendEmail(View v)
    {

        String to=mEditTextTo.getText().toString();
        String subject=mEditTextSubject.getText().toString();
        String body=mEditTextBody.getText().toString();

        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{to});
        intent.putExtra(Intent.EXTRA_SUBJECT,subject);
        intent.putExtra(Intent.EXTRA_TEXT,body);

        // If using the ACTION_SEND_MULTIPLE action,
        // this should instead be an ArrayList containing multiple Uri objects.
        //refer to docs
        intent.putExtra(Intent.EXTRA_STREAM,mSelectedImage);



        //intent.setType("message/rfc822");
        intent.setType("*/*");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        else {
            Toast.makeText(this," gterror", Toast.LENGTH_SHORT).show();
        }

        //startActivity(Intent.createChooser(intent,"Choose an email client"));


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode)
        {

            case REQUEST_TAKE_PHOTO_GALLERY:
                if(data!=null)
                {
                    //uri of image is obtained from intent
                    mSelectedImage = data.getData();
                    list.add(mSelectedImage);
                    Toast.makeText(this,mSelectedImage.toString(), Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(this,"error", Toast.LENGTH_SHORT).show();
                }
                break;
        }



    }
}
