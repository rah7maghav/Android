package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.roompersistencelib.NewUser;

import java.util.ArrayList;
import java.util.List;

public class AllUsersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<NewUser> list;
    private OnItemClickListener onItemClickListener;

    public AllUsersRecyclerAdapter(Context mContext, List<NewUser> list, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(mContext).inflate(R.layout.alluserslayout,parent,false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(list!=null)
        {
            NewUser newUser=list.get(position);

            if(newUser!=null)
            {
                ((UserHolder) holder).name.setText(newUser.getName());
                ((UserHolder) holder).contactno.setText(newUser.getContactno());
                ((UserHolder) holder).email.setText(newUser.getEmail());
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name,contactno,email;
        Button update,delete;


        public UserHolder(View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.text_username);
            email=itemView.findViewById(R.id.text_useremail);
            contactno=itemView.findViewById(R.id.text_usercontactno);

            update=itemView.findViewById(R.id.button_update);
            delete=itemView.findViewById(R.id.button_delete);

            update.setOnClickListener(this);
            delete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if(onItemClickListener!=null)
            {
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }


        }
    }
}
