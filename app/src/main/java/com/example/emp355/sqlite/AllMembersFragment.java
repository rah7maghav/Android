package com.example.emp355.sqlite;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.emp355.R;
import com.example.emp355.adapter.MembersRecyclerAdapter;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.sharedpref.MySharedPreferences;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

//All Members Fragment
public class AllMembersFragment extends Fragment implements OnItemClickListener{


    private MembersRecyclerAdapter membersRecyclerAdapter;
    private ArrayList<MyUser> list=new ArrayList<MyUser>();
    private ArrayList<Integer> likedlist=new ArrayList<Integer>();
    private ProgressDialog mProgressDialog;
    private MySharedPreferences mSharedPreferences;
    private SqliteHelper mSqliteHelper;
    private long id;
    private HashSet<Integer> hashSet=new HashSet<Integer>();
    MyUser myUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AfterLoginActivity) getActivity()).changeToolbarIcons();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_members, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView allMembers;

        mSharedPreferences=MySharedPreferences.getInstance(getActivity());
        mSqliteHelper=SqliteHelper.getInstance(getActivity());

        id= mSharedPreferences.fetchId();

        allMembers=view.findViewById(R.id.recyclerview_allmembers);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);

        allMembers.setLayoutManager(linearLayoutManager);
        membersRecyclerAdapter=new MembersRecyclerAdapter(getActivity(),list,likedlist,hashSet,this);
        allMembers.setAdapter(membersRecyclerAdapter);

        new MyAsyncTask(1,-1).execute();
    }


    //OnClick for RecyclerView Adapter views
    @Override
    public void onItemClick(View view, int position) {

        myUser=list.get(position);
        int viewId=view.getId();
        switch (viewId)
        {

            case R.id.imageview_likeicon:

                new MyAsyncTask(3,position).execute();
                break;

            case R.id.imageview_unlikeicon:

                new MyAsyncTask(2,position).execute();
                break;

        }

    }

    @Override
    public void onItemClick(View view, Cursor cursor) {

    }

    //AsyncTask class for fetching data regarding
    // number of likes done by current logged in user from database
    public class MyAsyncTask extends AsyncTask<Void,Void,Void>
    {

        int i;
        int listPosition;

        public MyAsyncTask(int i,int listPosition) {

            this.i=i;
            this.listPosition=listPosition;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(i==1)
            {
                mProgressDialog=new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Uploading");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }



        }


        @Override
        protected Void doInBackground(Void... voids) {

            if(i==1)
            {
                ArrayList<MyUser> list = mSqliteHelper.getAllUsersExceptCurrent(id);
                AllMembersFragment.this.list.addAll(list);

                HashSet<Integer> hashSet=mSqliteHelper.checkCurrentUserLikesOrNot((int)id);
                AllMembersFragment.this.hashSet.addAll(hashSet);

                ArrayList<Integer> likedlist = mSqliteHelper.getAllLikedToData();
                AllMembersFragment.this.likedlist.addAll(likedlist);

            }
            else if(i==2)
            {
                mSqliteHelper.likesTableInsert((int)id,(int)myUser.getId());

            }
            else if(i==3)
            {
                mSqliteHelper.deleteRecordOnUnlike((int)id,(int)myUser.getId());

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(i==1)
            {
                mProgressDialog.dismiss();
                membersRecyclerAdapter.notifyDataSetChanged();
            }
            else if(i==2)
            {
                myUser.setIslike(2);
                myUser.setLikescount(myUser.getLikescount()+1);
                membersRecyclerAdapter.notifyItemChanged(listPosition);


            }
            else if(i==3)
            {
                myUser.setIslike(1);
                myUser.setLikescount(myUser.getLikescount()-1);
                membersRecyclerAdapter.notifyItemChanged(listPosition);

            }


        }

    }
}
