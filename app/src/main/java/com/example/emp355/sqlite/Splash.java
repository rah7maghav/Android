package com.example.emp355.sqlite;


import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.emp355.R;

import com.example.emp355.sharedpref.MySharedPreferences;
//splash
public class Splash extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final int SPLASH_TIME=3000;
        MySharedPreferences mSharedPreferences;

        mSharedPreferences=MySharedPreferences.getInstance(Splash.this);
        final long id=mSharedPreferences.fetchId();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {

                    startActivity(new Intent(Splash.this, id == -1
                            ? EnterEmailActivity.class : AfterLoginActivity.class));
                    finish();
                }

            }
        },SPLASH_TIME);
    }
}
