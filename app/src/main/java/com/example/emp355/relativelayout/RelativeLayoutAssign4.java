package com.example.emp355.relativelayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.framelayout.FrameLayoutAssign1;

public class RelativeLayoutAssign4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout_assign4);
    }

    public void sendMessage(View view)
    {
        Intent i=new Intent(this,FrameLayoutAssign1.class);

        startActivity(i);

    }
}
