package com.example.emp355.uiassignment;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import android.view.View;

import android.widget.TextView;


import com.example.emp355.R;


public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        Toolbar toolbar1=(Toolbar) findViewById(R.id.signuptoolbar);

        setSupportActionBar(toolbar1);


       ActionBar actionBar=getSupportActionBar();

        // actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        SpannableString privacypolicy=new SpannableString(getString(R.string.privacypolicy));
        privacypolicy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.signuptheme)),30,45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacypolicy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.signuptheme)),47,64,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView=(TextView)findViewById(R.id.signuptext);
        textView.setText(privacypolicy);
    }


    public void Next(View view)
    {
        Intent i=new Intent(this, ChildView.class);
        startActivity(i);
    }


}
