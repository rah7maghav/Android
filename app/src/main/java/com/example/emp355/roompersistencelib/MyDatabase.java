package com.example.emp355.roompersistencelib;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

//list of entities included in the database
@Database(entities = {NewUser.class},version = 1)
public abstract class MyDatabase extends RoomDatabase {

    private static MyDatabase myDatabase;

    // Database name to be used
    public static final String NAME = "MyDataBase";

    //returns UserDao Object
    public abstract UserDao userDao();

    //returns object of MyDatabase class
    public static MyDatabase getInstance(Context  context)
    {
       if(myDatabase==null)
       {
           myDatabase=Room.databaseBuilder(context,MyDatabase.class,MyDatabase.NAME)
                   .build();
       }

       return myDatabase;

    }
}
