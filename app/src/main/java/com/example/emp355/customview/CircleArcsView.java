package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class CircleArcsView extends View{


    private int mViewHeight;
    private int mViewWidth;
    private int mArcTop;
    private int mArcLeft;
    private int mArcBottom;
    private int mArcRight;
    private Paint mArcPaint;

    public CircleArcsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);
    }


    private void init(Context context,AttributeSet attributeSet)
    {

        mArcPaint=new Paint();
        mArcPaint.setStyle(Paint.Style.FILL);
        mArcPaint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mViewHeight=getHeight();
        mViewWidth=getWidth();





        if(getWidth()<getHeight())
        {
            mArcLeft=0;
            mArcTop=mViewHeight/2-mViewWidth/2;
            mArcRight=mViewWidth;
            mArcBottom=mViewHeight/2+mViewWidth/2;
        }
        else
        {
            mArcLeft=mViewWidth/2-mViewHeight/2;
            mArcTop=0;
            mArcRight=mViewWidth/2+mViewHeight/2;
            mArcBottom=mViewHeight;
        }

        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,0,60,true,mArcPaint);
        mArcPaint.setColor(Color.GREEN);

        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,60,60,true,mArcPaint);
        mArcPaint.setColor(Color.YELLOW);

        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,120,60,true,mArcPaint);
        mArcPaint.setColor(Color.BLUE);
        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,180,60,true,mArcPaint);

        mArcPaint.setColor(Color.BLACK);
        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,240,60,true,mArcPaint);

        mArcPaint.setColor(Color.MAGENTA);
        canvas.drawArc(mArcLeft,mArcTop,mArcRight,mArcBottom,300,60,true,mArcPaint);

        mArcPaint.setColor(Color.RED);
    }
}
