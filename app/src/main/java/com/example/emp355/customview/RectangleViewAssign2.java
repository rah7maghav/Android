package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class RectangleViewAssign2 extends View {

    private Paint mLargeRectanglePaint;
    private Paint mSmallRectanglePaint;
    private Paint mLinePaint;
    private Paint mTextPaint;

    private int mLargeRectangleTop;
    private int mLargeRectangleLeft;
    private int mLargeRectangleRight;
    private int mLargeRectangleBottom;

    private int mLargeRectangleHeight;
    private int mLargeRectangleWidth;

    private float mSmallRectangleTop;
    private float mSmallRectangleLeft;
    private float mSmallRectangleRight;
    private float mSmallRectangleBottom;

    private float mSmallRectangleHeight;
    private float mSmallRectangleWidth;

    public RectangleViewAssign2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);
    }


    private void init(Context context,AttributeSet attributeSet)
    {
        mLargeRectanglePaint=new Paint();
        mSmallRectanglePaint=new Paint();
        mLinePaint=new Paint();
        mTextPaint=new Paint();

        mLargeRectanglePaint.setStyle(Paint.Style.STROKE);
        mLargeRectanglePaint.setColor(Color.BLUE);
        mLargeRectanglePaint.setStrokeWidth(5);

        mSmallRectanglePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mSmallRectanglePaint.setColor(Color.GREEN);
        mSmallRectanglePaint.setStrokeWidth(5);

        mLinePaint.setStyle(Paint.Style.FILL);
        mLinePaint.setColor(Color.BLUE);
        mLinePaint.setStrokeWidth(5);

        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(60);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mLargeRectangleTop=170;
        mLargeRectangleLeft=20;
        mLargeRectangleRight=getWidth()-20;
        mLargeRectangleBottom=getHeight()-170;

        mLargeRectangleWidth=mLargeRectangleRight-mLargeRectangleLeft;
        mSmallRectangleWidth=(float)(mLargeRectangleWidth-(mLargeRectangleWidth*0.4));

        mSmallRectangleLeft=mLargeRectangleLeft+(mLargeRectangleWidth-mSmallRectangleWidth)/2;
        mSmallRectangleRight=mSmallRectangleLeft+mSmallRectangleWidth;

        mLargeRectangleHeight=mLargeRectangleBottom-mLargeRectangleTop;
        mSmallRectangleHeight=(float)(mLargeRectangleHeight-(mLargeRectangleHeight*0.4));

        mSmallRectangleTop=mLargeRectangleTop+(mLargeRectangleHeight-mSmallRectangleHeight)/2;
        mSmallRectangleBottom=mSmallRectangleTop+mSmallRectangleHeight;


        canvas.drawRect(mLargeRectangleLeft,mLargeRectangleTop,mLargeRectangleRight,mLargeRectangleBottom,mLargeRectanglePaint);

        canvas.drawRect(mSmallRectangleLeft,mSmallRectangleTop,mSmallRectangleRight,mSmallRectangleBottom,mSmallRectanglePaint);
        canvas.drawRect(mSmallRectangleLeft,mSmallRectangleTop,mSmallRectangleRight,mSmallRectangleBottom,mLargeRectanglePaint);

        canvas.drawLine(mLargeRectangleLeft,mLargeRectangleTop,mSmallRectangleLeft,mSmallRectangleTop,mLinePaint);
        canvas.drawLine(mSmallRectangleRight,mSmallRectangleTop,mLargeRectangleRight,mLargeRectangleTop,mLinePaint);
        canvas.drawLine(mLargeRectangleLeft,mLargeRectangleBottom,mSmallRectangleLeft,mSmallRectangleBottom,mLinePaint);
        canvas.drawLine(mSmallRectangleRight,mSmallRectangleBottom,mLargeRectangleRight,mLargeRectangleBottom,mLinePaint);


        canvas.drawText("This is a",mSmallRectangleLeft+(mSmallRectangleWidth/2),mSmallRectangleTop+(mSmallRectangleHeight/2),mTextPaint);
        canvas.drawText("custom View",mSmallRectangleLeft+(mSmallRectangleWidth/2),mSmallRectangleTop+(mSmallRectangleHeight/2)+70,mTextPaint);

    }
}
