package com.example.emp355.listener;

import android.database.Cursor;
import android.view.View;

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
    public void onItemClick(View view, Cursor cursor);
}
