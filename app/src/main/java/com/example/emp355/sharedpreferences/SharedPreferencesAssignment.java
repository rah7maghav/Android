package com.example.emp355.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.File;

import static com.example.emp355.Constant.EMAIL;
import static com.example.emp355.Constant.FIRST_NAME;
import static com.example.emp355.Constant.LAST_NAME;
import static com.example.emp355.Constant.LOGIN_SHARED_PREFERENCE_FILE;
import static com.example.emp355.Constant.PASSWORD;
import static com.example.emp355.Constant.PHONE_NUMBER;
import static com.example.emp355.Constant.REF_CODE;
import static com.example.emp355.Constant.USERNAME;

public class SharedPreferencesAssignment extends AppCompatActivity {


    private EditText mFirstName, mLastName, mUserName, mPhoneNumber, mEmail, mPassword, mConfirmPassword, mRefCode;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences_assignment);
        Button signUp;

        SpannableString privacyPolicy = new SpannableString(getResources().getString(R.string.privacypolicy));
        privacyPolicy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.signuptheme)), 30, 45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacyPolicy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.signuptheme)), 47, 64, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) findViewById(R.id.textview_privacypolicy);
        textView.setText(privacyPolicy);

        signUp = findViewById(R.id.button_signup);

        mFirstName = findViewById(R.id.edittext_firstname);
        mLastName = findViewById(R.id.edittext_lastname);
        mUserName = findViewById(R.id.edittext_username);
        mPhoneNumber = findViewById(R.id.edittext_phnumber);
        mEmail = findViewById(R.id.edittext_email);
        mPassword = findViewById(R.id.edittext_password);
        mConfirmPassword = findViewById(R.id.edittext_confirmpassword);
        mRefCode = findViewById(R.id.edittext_refcode);


        SharedPreferences sharedPreferences = getSharedPreferences(LOGIN_SHARED_PREFERENCE_FILE, MODE_PRIVATE);

        String firstName=sharedPreferences.getString(FIRST_NAME, "");
        String lastName=sharedPreferences.getString(LAST_NAME, "");
        String userName=sharedPreferences.getString(USERNAME, "");
        String phoneNumber=sharedPreferences.getString(PHONE_NUMBER, "");
        String email=sharedPreferences.getString(EMAIL, "");
        String password=sharedPreferences.getString(PASSWORD, "");
        String refCode=sharedPreferences.getString(REF_CODE, "");

        checkEmpty(firstName,lastName,userName,phoneNumber,email,password,refCode);




        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateData(mFirstName.getText().toString(),mLastName.getText().toString(),
                        mUserName.getText().toString(), mPhoneNumber.getText().toString(),mEmail.getText().toString(),mPassword.getText().toString(),mConfirmPassword.getText().toString()))
                    StoreInSharedPreferences();

            }
        });

    }


    private void StoreInSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences(LOGIN_SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(FIRST_NAME, mFirstName.getText().toString());
        editor.putString(LAST_NAME, mLastName.getText().toString());
        editor.putString(USERNAME, mUserName.getText().toString());
        editor.putString(PHONE_NUMBER, mPhoneNumber.getText().toString());
        editor.putString(EMAIL, mEmail.getText().toString());
        editor.putString(PASSWORD, mPassword.getText().toString());
        editor.putString(REF_CODE, mRefCode.getText().toString());

        //editor.apply();asynchronously
        editor.commit();//synchronously


        finish();
    }



    private void checkEmpty(String firstName, String lastName, String userName,String phoneNumber, String email, String password,String refCode)
    {
        if (!TextUtils.isEmpty(firstName)) {
            mFirstName.setText(firstName);
        }

        if (!TextUtils.isEmpty(lastName)) {
            mLastName.setText(lastName);
        }

        if (!TextUtils.isEmpty(userName)) {
            mUserName.setText(userName);
        }

        if (!TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumber.setText(phoneNumber);
        }

        if (!TextUtils.isEmpty(email)) {
            mEmail.setText(email);
        }

        if (!TextUtils.isEmpty(password)) {
            mPassword.setText(password);
            mConfirmPassword.setText(password);
        }
        if (!TextUtils.isEmpty(refCode))
        {
            mRefCode.setText(refCode);
        }

    }



    private boolean validateData(String firstName, String lastName, String userName,String phoneNumber, String email, String password,String confirmPassword) {

        if (TextUtils.isEmpty(firstName)) {
            mFirstName.setError("FirstName Cannot be Empty");
            return false;
        }
        if (TextUtils.isEmpty(lastName)) {
            mLastName.setError("LastName Cannot be Empty");
            return false;
        }
        if (TextUtils.isEmpty(userName)) {
            mUserName.setError("UserName Cannot be Empty");
            return false;
        }
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumber.setError("PhoneNumber Cannot be Empty");
            return false;
        }

        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Email Cannot be Empty");
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Password Cannot be Empty");
            return false;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }


}
