package com.example.emp355.multithreading;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.emp355.R;

public class HandlerExampleActivity extends AppCompatActivity {

    TextView mFruit,mLogout;
    Button mClick;
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler_example);


        mFruit=findViewById(R.id.text_fruit);
        mLogout=findViewById(R.id.text_logout);
        mClick=findViewById(R.id.button_click);
        messageHandler();

        mClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Thread thread=new Thread(new MyHandler(mHandler));
                thread.start();

            }
        });


    }

    public void messageHandler()
    {
        if(mHandler==null)
        {
            mHandler=new Handler(Looper.getMainLooper())
            {
                @Override
                public void handleMessage(Message msg) {


                    mFruit.setText((String)msg.obj);
                }

            };
        }
    }

    public class MyHandler implements Runnable{

        Handler handler;

        public MyHandler(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void run() {

            String var="Orange";
            Message message=handler.obtainMessage(1,var);
            message.sendToTarget();

            mLogout.post(new Runnable() {
                @Override
                public void run() {
                    mLogout.setText("Login");
                }
            });
        }

    }
}
