package com.example.emp355.kotlin

import android.app.ProgressDialog
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.emp355.JSONUtils
import com.example.emp355.R
import com.example.emp355.model.DataObject
import com.example.emp355.model.Image
import kotlinx.android.synthetic.main.activity_assignment1_third_party.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference

class Assignment1ThirdPartyActivity : AppCompatActivity() {


    var mProgressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assignment1_third_party)

        val jsonFileName="json_imagesdata.json"
        ParseJsonData(this).execute(jsonFileName)

        tablayout.setupWithViewPager(viewpager)

    }


    class ParseJsonData(activity: Assignment1ThirdPartyActivity): AsyncTask<String, Void, DataObjectKotlin>()
    {
        private var weakReference:WeakReference<Assignment1ThirdPartyActivity>?=null

        init {
            weakReference=WeakReference(activity)
        }

        override fun onPreExecute() {

            weakReference?.get()?.mProgressDialog= ProgressDialog(weakReference?.get())
            weakReference?.get()?.mProgressDialog?.setMessage("Uploading")
            weakReference?.get()?.mProgressDialog?.setCancelable(false)
            weakReference?.get()?.mProgressDialog?.show()

        }

        override fun doInBackground(vararg params: String): DataObjectKotlin {

            val jsonStringData:String?=loadJsonFromAssets(params[0])

            val jsonObject:JSONObject=JSONUtils.getJSONObject(jsonStringData)

            val dataObjectKotlin=DataObjectKotlin()

            val jsonImagesArray:JSONArray=JSONUtils.getJSONArray(jsonObject,"images")

            val imageList:MutableList<ImageKotlin> = ArrayList()

            for(i in 0 until JSONUtils.getLengthofJSONArray(jsonImagesArray))
            {

                val jsonImagesObject:JSONObject= JSONUtils.getJSONObject(jsonImagesArray, i)

                val imageKotlin= ImageKotlin()

                imageKotlin.name=JSONUtils.getStringfromJSON(jsonImagesObject,"name")

                val jsonValuesArray:JSONArray= JSONUtils.getJSONArray(jsonImagesObject, "values")

                val valuesList:MutableList<String> = ArrayList()

                for(j in 0 until JSONUtils.getLengthofJSONArray(jsonValuesArray))
                {
                    valuesList.add(JSONUtils.getStringObject(jsonValuesArray, j))
                }

                imageKotlin.values=valuesList

                imageList.add(imageKotlin)

                try {
                    Thread.sleep(1000)

                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }

            dataObjectKotlin.images=imageList

            return dataObjectKotlin
        }



        private fun loadJsonFromAssets(fileName:String):String?
        {
            val json:String

            try {
                val inputStream:InputStream=weakReference!!.get()!!.assets.open(fileName)

                val size:Int=inputStream.available()

                val buffer= ByteArray(size)

                inputStream.read(buffer)
                inputStream.close()

                json= String(buffer, charset("UTF-8"))
            }
            catch (e:IOException)
            {
                e.printStackTrace()
                return null
            }

            return json


        }


        override fun onPostExecute(result: DataObjectKotlin) {

            weakReference?.get()?.mProgressDialog?.dismiss()

            val thirdPartyAdapter= ThirdPartyAdapterViewPager(weakReference!!.get()!!.supportFragmentManager, result)

            weakReference?.get()?.viewpager?.adapter=thirdPartyAdapter

        }


    }
}
