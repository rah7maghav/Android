package com.example.emp355.linearlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.relativelayout.RelativeLayoutAssign1;

public class LinearLayoutAssign4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout_assign4);
    }

    public void sendMessage(View view)
    {
        Intent i=new Intent(this,RelativeLayoutAssign1.class);

        startActivity(i);

    }
}
