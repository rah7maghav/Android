package com.example.emp355.actionbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.snackbar.SnackbarAssign1;

public class ActionBarAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_bar_assign1);
        //setTitle(R.string.action_bar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.search_ic:

                return true;

            case R.id.gps:

                return true;

            case R.id.refresh:

                return true;

            case R.id.help:

                return true;

            case R.id.chk_upd:

                return true;

        }

        return(super.onOptionsItemSelected(item));

    }

    public void Next(View view)
    {
        Intent i=new Intent(this, SnackbarAssign1.class);
        startActivity(i);
    }



}
