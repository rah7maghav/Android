package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Open implements Parcelable {

    private String time;
    private int day;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.time);
        dest.writeInt(this.day);
    }

    public Open() {
    }

    protected Open(Parcel in) {
        this.time = in.readString();
        this.day = in.readInt();
    }

    public static final Parcelable.Creator<Open> CREATOR = new Parcelable.Creator<Open>() {
        @Override
        public Open createFromParcel(Parcel source) {
            return new Open(source);
        }

        @Override
        public Open[] newArray(int size) {
            return new Open[size];
        }
    };
}
