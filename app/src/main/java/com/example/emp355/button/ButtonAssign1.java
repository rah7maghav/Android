package com.example.emp355.button;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.compoundbutton.CompundButtonAssign1;


public class ButtonAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_assign1);
    }


    public void Next(View view)
    {
        Intent i=new Intent(this, CompundButtonAssign1.class);
        startActivity(i);
    }
}
