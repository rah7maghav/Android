package com.example.emp355.contentprovider;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.adapter.MySimpleCursorAdaptor;
import com.example.emp355.listener.OnItemClickListener;

public class FetchData extends AppCompatActivity implements OnItemClickListener {
    private MySimpleCursorAdaptor simpleCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_data);

        ListView listView=findViewById(R.id.listview_data);

        Cursor cursor=getContentResolver().query(UserEntry.CONTENT_URI,null,null,null,null);

        // Defines a list of columns to retrieve from the Cursor and load into an output row
        String[] from =
                {
                        UserEntry.COLUMN_FULLNAME,
                        UserEntry.COLUMN_AGE,
                        UserEntry.COLUMN_EMAIL,
                        UserEntry.COLUMN_CONTACTNO,
                        UserEntry.COLUMN_GENDER

                };


        // Defines a list of View IDs that will receive the Cursor columns for each row
        int[] to = { R.id.text_user_fname, R.id.text_user_age,R.id.text_user_email,R.id.text_user_contactno,R.id.text_user_gender};

        // Creates a new SimpleCursorAdapter
       simpleCursorAdapter=new
               MySimpleCursorAdaptor(FetchData.this,
                R.layout.viewdata,cursor,from,to,0, this);

        // Sets the adapter for the ListView
        listView.setAdapter(simpleCursorAdapter);

    }

    @Override
    public void onItemClick(View view, int position) {

    }

    //handles Onclick Operation on SimpleCursorAdapter for Update and Delete
    @Override
    public void onItemClick(View view, Cursor cursor) {


       int id= cursor.getInt(cursor.getColumnIndex(UserEntry._ID));

        switch (view.getId())
        {
            //Update Onclick
            case R.id.btn_update:
                startActivity(new Intent(this,UpdateData.class).putExtra("id",id));


                break;

            //Delete Onclick
            case R.id.btn_delete:
                String where=UserEntry._ID+"=?";
                String selectionArgs[]={id+""};
                int rowsdeleted=getContentResolver().delete(UserEntry.CONTENT_URI,where,selectionArgs);
                Log.d("d", "onCreate: "+id);
                if(rowsdeleted==1)
                {
                    simpleCursorAdapter.notifyDataSetChanged();
                    Toast.makeText(this,"Deleted Record",Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(this,"Could Not Delete",Toast.LENGTH_LONG).show();
                }


                break;
        }

    }
}
