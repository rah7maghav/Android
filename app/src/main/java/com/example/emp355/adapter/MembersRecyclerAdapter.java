package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.sqlite.MyUser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
//All Members Fragment RecyclerView Adapter
public class MembersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<MyUser> list;
    private ArrayList<Integer> likedlist;
    private OnItemClickListener onItemClickListener;
    private HashSet<Integer> hashSet;

    public MembersRecyclerAdapter(Context context,ArrayList<MyUser> list, ArrayList<Integer> likedlist , HashSet<Integer> hashSet,OnItemClickListener onItemClickListener) {

        mContext=context;
        this.list=list;
        this.onItemClickListener=onItemClickListener;
        this.hashSet=hashSet;
        this.likedlist=likedlist;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(mContext).inflate(R.layout.allmemberslayout,parent,false);

        return new MembersHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        boolean flag=true;
        String age="";

        if (list!=null)
        {
            MyUser myUser=list.get(position);


            if (myUser!=null)
            {
                //calculates age of user if dob is  specified by user
                if(myUser.getDob()!=null)
                {


                 DateFormat formatter = new SimpleDateFormat("DD/MM/yyyy");
                 Date date=null;
                    try {
                        date= (Date)formatter.parse(myUser.getDob());
                    }
                    catch (ParseException p)
                    {
                        p.printStackTrace();
                    }
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);

                    age=getAge(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)==0?11:(calendar.get(Calendar.MONTH)-1),calendar.get(Calendar.DAY_OF_MONTH));
                }
                //on unliking any user
                if(myUser.getIslike()==2)
                {
                    ((MembersHolder) holder).unLikeIcon.setVisibility(GONE);
                    ((MembersHolder) holder).likeIcon.setVisibility(VISIBLE);
                    ((MembersHolder) holder).like.setText(R.string.unlike);
                    ((MembersHolder) holder).likesCount.setText(String.valueOf(myUser.getLikescount()));
                    flag=false;
                }

                //on liking any user
                else if(myUser.getIslike()==1)
                {
                    ((MembersHolder) holder).unLikeIcon.setVisibility(VISIBLE);
                    ((MembersHolder) holder).likeIcon.setVisibility(GONE);
                    ((MembersHolder) holder).like.setText(R.string.like);
                    ((MembersHolder) holder).likesCount.setText(String.valueOf(myUser.getLikescount()));

                    flag=false;
                }

                //fetches all data corresponding to likes done by particular user
                if(likedlist!=null && flag)
                {
                    ((MembersHolder) holder).likesCount.setText(String.valueOf(Collections.frequency(likedlist,(int)myUser.getId())));
                    myUser.setLikescount(Collections.frequency(likedlist,(int)myUser.getId()));
                    if((hashSet!=null && hashSet.contains((int)myUser.getId())))
                    {
                        ((MembersHolder) holder).unLikeIcon.setVisibility(GONE);
                        ((MembersHolder) holder).likeIcon.setVisibility(VISIBLE);
                        ((MembersHolder) holder).like.setText(R.string.unlike);

                    }
                }





                ((MembersHolder) holder).age.setText(age);
                ((MembersHolder) holder).firstName.setText(myUser.getFirstname());
                ((MembersHolder) holder).lastName.setText(myUser.getLastname());
                ((MembersHolder) holder).email.setText(myUser.getEmail());
                ((MembersHolder) holder).phoneNumber.setText(myUser.getPhonenumber());
            }

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //calculates age of user using dob
    private String getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        return String.valueOf(age);
    }


    public class MembersHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView firstName,lastName,email,phoneNumber,like,likesCount,age;
        ImageView likeIcon,unLikeIcon;

        public MembersHolder(View itemView) {
            super(itemView);
            firstName=itemView.findViewById(R.id.text_userfirstname);
            lastName=itemView.findViewById(R.id.text_userlastname);
            email=itemView.findViewById(R.id.text_useremail);
            phoneNumber=itemView.findViewById(R.id.text_userphone);
            age=itemView.findViewById(R.id.text_userage);

            like=itemView.findViewById(R.id.text_like);

            likesCount=itemView.findViewById(R.id.text_likescount);

            unLikeIcon=itemView.findViewById(R.id.imageview_unlikeicon);
            unLikeIcon.setOnClickListener(this);

            likeIcon=itemView.findViewById(R.id.imageview_likeicon);
            likeIcon.setOnClickListener(this);
        }

        //onClick for Adapter Views
        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null)
            {
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }
        }
    }
}
