package com.example.emp355.sqlite;

//Table for storing likes data
public class LikesTable {

    public static final String TABLE_NAME="likes";
    public static final String LIKED_BY="liked_by";
    public static final String LIKED_TO="liked_to";

    public static final String CREATE_TABLE_QUERY=
            "CREATE TABLE "+TABLE_NAME+" ( "+LIKED_BY+" INTEGER , "+LIKED_TO+" INTEGER );";
}
