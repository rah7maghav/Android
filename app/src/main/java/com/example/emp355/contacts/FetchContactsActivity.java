package com.example.emp355.contacts;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.emp355.R;

public class FetchContactsActivity extends AppCompatActivity {

    private static final int PICK_CONTACT =1 ;
    private TextView mTextName, mTextHome,mTextMobile,mTextWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_contacts);
        mTextName =findViewById(R.id.textview_name);
        mTextHome =findViewById(R.id.textview_home);
        mTextMobile =findViewById(R.id.textview_mobile);
        mTextWork =findViewById(R.id.textview_work);
    }

    public void fetchContact(View view) {

        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode)
        {
            case (PICK_CONTACT):

                if (resultCode == RESULT_OK)
                {
                    Uri contactData = data.getData();
                    Log.d("uri", contactData.toString());

                    long id=ContentUris.parseId(contactData);
                    Log.d("id", id+"");

                    Cursor cursor=getContentResolver().query(contactData,null,null,null,null);

                    if(cursor!=null && cursor.getCount()==1 && cursor.moveToFirst())
                    {
                        String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        mTextName.setText(contactName);
                        String hasPhone=cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1"))
                        {
                            String selection=ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"=?";
                            String selectionArgs[]={String.valueOf(id)};

                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    selection,selectionArgs, null);


                            if (phones != null)
                            {

                                Log.d("c", ""+phones.getCount());

                                while (phones.moveToNext())
                                {
                                    int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                    switch (type)
                                    {
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:

                                            mTextHome.setText("home="+phoneNumber);
                                            break;

                                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:

                                            mTextMobile.setText("mobile="+phoneNumber);
                                            break;

                                        case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:

                                            mTextWork.setText("work="+phoneNumber);
                                            break;

                                    }

                                }


                                phones.close();
                            }


                        }

                        cursor.close();
                    }







                    /*Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
                    if (phones != null) {
                        while (phones.moveToNext())
                        {
                            int home,mobile,work;
                            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                            switch (type)
                            {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:

                                    Log.d("num", name+" home="+phoneNumber);
                                    break;

                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:

                                    Log.d("num", name+" mobile="+phoneNumber);
                                    break;
                                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:

                                    Log.d("num", name+" work="+phoneNumber);
                                    break;

                            }

                        }
                        phones.close();
                    }*/



                    //Following code shows an easy way to read all phone numbers and names:
                    //Significant faster but contact name may be duplicated if that contact has more than 1 number
                    /*Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
                    if (phones != null) {
                        while (phones.moveToNext())
                        {
                            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            Log.d("ph", name+" "+phoneNumber);
                        }
                        phones.close();
                    }*/



                }

        }

    }

}
