package com.example.emp355.contentprovider;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;


//Observer class
public class ProviderObserver extends ContentObserver {
    String TAG = getClass().getSimpleName();
    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public ProviderObserver(Handler handler) {
        super(handler);
    }




    //Returns true if this observer is interested receiving self-change notifications.
    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }


    //method called when a content change occurs.
    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange: ");
    }


    //method called when a content change occurs. Includes the changed content Uri when available.
    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        Log.d(TAG, "onChange: " + uri);
    }
}
