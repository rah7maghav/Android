package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Matched_Substring implements Parcelable {

    private int length;
    private int offset;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.length);
        dest.writeInt(this.offset);
    }

    public Matched_Substring() {
    }

    protected Matched_Substring(Parcel in) {
        this.length = in.readInt();
        this.offset = in.readInt();
    }

    public static final Parcelable.Creator<Matched_Substring> CREATOR = new Parcelable.Creator<Matched_Substring>() {
        @Override
        public Matched_Substring createFromParcel(Parcel source) {
            return new Matched_Substring(source);
        }

        @Override
        public Matched_Substring[] newArray(int size) {
            return new Matched_Substring[size];
        }
    };
}
