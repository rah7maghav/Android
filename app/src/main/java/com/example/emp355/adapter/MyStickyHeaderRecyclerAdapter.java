package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Image;
import com.squareup.picasso.Picasso;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//Adapter for image data and headers
public class MyStickyHeaderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    private Context mContext;

    //Title List for headers
    private List<String> mTitleList;

    //All Url List for images
    private List<String> mUrlList;

    //initializes the Title(Header) list and Url list
    public MyStickyHeaderRecyclerAdapter(Context Context, List<String> UrlList, List<String> TitleList) {
        this.mContext = Context;
        this.mUrlList=UrlList;
        this.mTitleList=TitleList;

    }

    //returns a unique id for grouping items in one header
    @Override
    public long getHeaderId(int position) {

        int len =mTitleList.get(position).length();

        //returns ascii value(unique for every title) of second last letter of the title
         return mTitleList.get(position).charAt(len-2);
    }


    //inflates the layout for header
    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.stickyheaderlayout,parent,false);


        return new StickyHeaderHolder(view);
    }


    //binds the header data based on position
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(mTitleList!=null)
        {

            ((StickyHeaderHolder) holder).stickyHeaderText.setText(mTitleList.get(position));
        }

    }


    //inflates the layout for images
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.imagelayout,parent,false);


        return new ImageHolder(view);
    }


    //binds the url for images based on position
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if(mUrlList!=null)
        {
            //using Picasso library to load and display images
                Picasso.get()
                        .load(mUrlList.get(position))//string url
                        .placeholder(R.mipmap.ic_launcher)//image on loading
                        .error(R.drawable.loadingimagefail)//image on loading failed
                        .into(((ImageHolder) holder).imageView);//imageview for showing image
        }
    }

    //returns the total items in the recycler view
    @Override
    public int getItemCount() {

        return mUrlList.size();
    }


    //holder for images
    public class ImageHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;


        public ImageHolder(View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageview_data);
        }


    }

    //holder for header
    public class StickyHeaderHolder extends RecyclerView.ViewHolder{

        private TextView stickyHeaderText;


        public StickyHeaderHolder(View itemView) {
            super(itemView);

            stickyHeaderText=itemView.findViewById(R.id.textview_stickyheader);
        }


    }

}
