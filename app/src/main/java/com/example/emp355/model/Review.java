package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Review implements Parcelable {
    private String author_name;
    private String author_url;
    private String language;
    private String profile_photo_url;
    private String relative_time_description;
    private String text;
    private long time;
    private int rating;

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_url() {
        return author_url;
    }

    public void setAuthor_url(String author_url) {
        this.author_url = author_url;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getProfile_photo_url() {
        return profile_photo_url;
    }

    public void setProfile_photo_url(String profile_photo_url) {
        this.profile_photo_url = profile_photo_url;
    }

    public String getRelative_time_description() {
        return relative_time_description;
    }

    public void setRelative_time_description(String relative_time_description) {
        this.relative_time_description = relative_time_description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.author_name);
        dest.writeString(this.author_url);
        dest.writeString(this.language);
        dest.writeString(this.profile_photo_url);
        dest.writeString(this.relative_time_description);
        dest.writeString(this.text);
        dest.writeLong(this.time);
        dest.writeInt(this.rating);
    }

    public Review() {
    }

    protected Review(Parcel in) {
        this.author_name = in.readString();
        this.author_url = in.readString();
        this.language = in.readString();
        this.profile_photo_url = in.readString();
        this.relative_time_description = in.readString();
        this.text = in.readString();
        this.time = in.readLong();
        this.rating = in.readInt();
    }

    public static final Parcelable.Creator<Review> CREATOR = new Parcelable.Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel source) {
            return new Review(source);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };
}
