package com.example.emp355.sqlite;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emp355.R;
import com.example.emp355.adapter.LikesRecyclerAdapter;
import com.example.emp355.sharedpref.MySharedPreferences;

import java.util.ArrayList;

//Likes Fragment for logged in user
public class LikesFragment extends Fragment  {

    private ArrayList<MyUser> list=new ArrayList<MyUser>();
    private MySharedPreferences mSharedPreferences;
    private SqliteHelper mSqliteHelper;
    private long id;
    private ProgressDialog mProgressDialog;
    private LikesRecyclerAdapter mLikesRecyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AfterLoginActivity) getActivity()).changeToolbarIcons();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_likes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView likesMembers;

        mSharedPreferences= MySharedPreferences.getInstance(getActivity());
        mSqliteHelper=SqliteHelper.getInstance(getActivity());

        id= mSharedPreferences.fetchId();

        likesMembers=view.findViewById(R.id.recyclerview_likes);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);

        likesMembers.setLayoutManager(linearLayoutManager);
        mLikesRecyclerAdapter=new LikesRecyclerAdapter(getActivity(),list);
        likesMembers.setAdapter(mLikesRecyclerAdapter);


    }


    //fetches data of users who have liked the current logged in user
    public void getAllLikedByData(ArrayList<MyUser> list)
    {
        LikesFragment.this.list.addAll(list);
    }

}
