package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.sqlite.MyUser;

import java.util.ArrayList;
//Likes Fragment Recycler View Adapter
public class LikesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<MyUser> list;

    public LikesRecyclerAdapter(Context mContext, ArrayList<MyUser> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.allmemberslayout,parent,false);

        return new LikesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((LikesHolder) holder).likeDetails.setVisibility(View.GONE);
        ((LikesHolder) holder).age.setVisibility(View.GONE);

        if (list!=null) {
            MyUser myUser = list.get(position);
            if (myUser != null) {


                ((LikesHolder) holder).firstName.setText(myUser.getFirstname());
                ((LikesHolder) holder).lastName.setText(myUser.getLastname());
                ((LikesHolder) holder).phoneNumber.setText(myUser.getPhonenumber());
                ((LikesHolder) holder).email.setText(myUser.getEmail());

            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class LikesHolder extends RecyclerView.ViewHolder{

        TextView firstName,lastName,email,phoneNumber,age;
        LinearLayout likeDetails;

        public LikesHolder(View itemView) {
            super(itemView);
            firstName=itemView.findViewById(R.id.text_userfirstname);
            lastName=itemView.findViewById(R.id.text_userlastname);
            email=itemView.findViewById(R.id.text_useremail);
            phoneNumber=itemView.findViewById(R.id.text_userphone);
            likeDetails=itemView.findViewById(R.id.linearlayout_likedetails);
            age=itemView.findViewById(R.id.text_userage);
        }

    }
}
