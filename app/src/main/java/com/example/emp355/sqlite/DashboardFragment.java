package com.example.emp355.sqlite;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.emp355.R;

import com.example.emp355.sharedpref.MySharedPreferences;

import static com.example.emp355.Constant.LIKES_NUMBER;

//User Dashboard Fragment after log in
public class DashboardFragment extends Fragment {

    private Button likesIcon;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AfterLoginActivity) getActivity()).changeToolbarIcons();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MySharedPreferences mSharedPreferences;
        SqliteHelper mSqliteHelper;
        TextView firstName,lastName,email,phoneNumber;



        mSharedPreferences=MySharedPreferences.getInstance(getActivity());
        mSqliteHelper=SqliteHelper.getInstance(getActivity());

        firstName=view.findViewById(R.id.textview_userfirstname);
        lastName=view.findViewById(R.id.textview_userlastname);
        email=view.findViewById(R.id.textview_useremail);
        phoneNumber=view.findViewById(R.id.textview_userphone);
        likesIcon=view.findViewById(R.id.button_likesicon);

        likesIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((AfterLoginActivity) getActivity()).OpenLikesFragment();
            }
        });

        long id= mSharedPreferences.fetchId();
        MyUser myUser=mSqliteHelper.getUser(id);

        if(myUser!=null)
        {
            firstName.setText(myUser.getFirstname());
            lastName.setText(myUser.getLastname());
            email.setText(myUser.getEmail());
            phoneNumber.setText(myUser.getPhonenumber());
        }



    }



    //fetching number of likes of logged in user from bundle
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
           int no = b.getInt(LIKES_NUMBER);
          likesIcon.setText(String.valueOf(no));
        }
        Log.d("oA", "onActivityCreated: called");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("at", "onAttach: called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("de", "onDestroyView: called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("det", "onDetach: called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("pa", "onPause: called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("st", "onStop: called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("re", "onResume: called");
    }
}
