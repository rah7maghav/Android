package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Address_Component implements Parcelable {

    private String long_name;
    private String short_name;
    private List<String> types;



    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.long_name);
        dest.writeString(this.short_name);
        dest.writeStringList(this.types);
    }

    public Address_Component() {
    }

    protected Address_Component(Parcel in) {
        this.long_name = in.readString();
        this.short_name = in.readString();
        this.types = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Address_Component> CREATOR = new Parcelable.Creator<Address_Component>() {
        @Override
        public Address_Component createFromParcel(Parcel source) {
            return new Address_Component(source);
        }

        @Override
        public Address_Component[] newArray(int size) {
            return new Address_Component[size];
        }
    };
}
