package com.example.emp355.multithreading;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.emp355.JSONUtils;
import com.example.emp355.R;
import com.example.emp355.adapter.AllDetailsRecyclerAdapter;
import com.example.emp355.model.Geometry;
import com.example.emp355.model.Location;
import com.example.emp355.model.MyObject;
import com.example.emp355.model.OpeningHour;
import com.example.emp355.model.Photo;
import com.example.emp355.model.Result;
import com.example.emp355.model.ViewPort;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MultiThreadAssignThreads extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private AllDetailsRecyclerAdapter allDetailsRecyclerAdapter;
    private RecyclerView allDetailsRecyclerView;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_thread_assign_threads);


        allDetailsRecyclerView=findViewById(R.id.recyclerview_alldetails);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        allDetailsRecyclerView.setLayoutManager(linearLayoutManager);
        messageHandler();


        Thread thread=new Thread(new MyHandler(mHandler));
        thread.start();
        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setMessage("Uploading");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }



    public void messageHandler()
    {
        if(mHandler==null)
        {
            mHandler=new Handler(Looper.getMainLooper())
            {
                @Override
                public void handleMessage(Message msg) {


                    MyObject myObject=(MyObject) msg.obj;
                    allDetailsRecyclerAdapter=new AllDetailsRecyclerAdapter(MultiThreadAssignThreads.this,myObject);
                    allDetailsRecyclerView.setAdapter(allDetailsRecyclerAdapter);
                    mProgressDialog.dismiss();
                }

            };
        }
    }

    public class MyHandler implements Runnable{


        Handler handler;

        public MyHandler(Handler handler) {
            this.handler = handler;
        }


        private String loadJsonFromAssets()
        {
            String json;

            try{
                InputStream inputStream=MultiThreadAssignThreads.this.getAssets().open("json_parser.json");

                int size=inputStream.available();
                byte[] buffer=new byte[size];

                inputStream.read(buffer);
                inputStream.close();

                json=new String(buffer,"UTF-8");


            }
            catch (IOException e)
            {
                e.printStackTrace();
                return null;
            }

            return json;

        }

        @Override
        public void run() {

            String jsonstringdata=loadJsonFromAssets();

            MyObject myObject=new MyObject();

            JSONObject jsonObject= JSONUtils.getJSONObject(jsonstringdata);

            JSONArray html_attributionsarray =JSONUtils.getJSONArray(jsonObject,"html_attributions");

            List<String> html_attributionsList=new ArrayList<String>();

            for (int i=0;i < JSONUtils.getLengthofJSONArray(html_attributionsarray);i++)
            {

                html_attributionsList.add(JSONUtils.getStringObject(html_attributionsarray,i));
            }

            myObject.setHtml_attributions(html_attributionsList);


            myObject.setNext_page_token(JSONUtils.getStringfromJSON(jsonObject,"next_page_token"));


            myObject.setStatus(JSONUtils.getStringfromJSON(jsonObject,"status"));




            JSONArray jsonResultArray = JSONUtils.getJSONArray(jsonObject,"results");


            List<Result> resultList=new ArrayList<Result>();

            for (int i = 0; i < JSONUtils.getLengthofJSONArray(jsonResultArray); i++)
            {


                JSONObject jsonResultObject =JSONUtils.getJSONObject(jsonResultArray,i);

                Result result = new Result();

                JSONObject jsonGeometryObject =JSONUtils.getJSONObject(jsonResultObject,"geometry");


                Geometry geometry = new Geometry();


                JSONObject jsonLocationObject =JSONUtils.getJSONObject(jsonGeometryObject,"location");


                Location location = new Location();


                location.setLat(JSONUtils.getDoublefromJSON(jsonLocationObject,"lat"));

                location.setLng(JSONUtils.getDoublefromJSON(jsonLocationObject,"lng"));

                geometry.setLocation(location);


                JSONObject jsonViewPortObject = JSONUtils.getJSONObject(jsonGeometryObject,"viewport");

                ViewPort viewPort = new ViewPort();


                JSONObject jsonNorthEastObject = JSONUtils.getJSONObject(jsonViewPortObject,"northeast");

                Location northEast = new Location();


                northEast.setLat(JSONUtils.getDoublefromJSON(jsonNorthEastObject,"lat"));


                northEast.setLng(JSONUtils.getDoublefromJSON(jsonNorthEastObject,"lng"));
                viewPort.setNortheast(northEast);


                JSONObject jsonSouthWestObject =JSONUtils.getJSONObject(jsonViewPortObject,"southwest");

                Location southWest = new Location();


                southWest.setLat(JSONUtils.getDoublefromJSON(jsonSouthWestObject,"lat"));

                southWest.setLng(JSONUtils.getDoublefromJSON(jsonSouthWestObject,"lng"));

                viewPort.setSouthwest(southWest);


                geometry.setViewPort(viewPort);

                result.setGeometry(geometry);




                result.setIcon(JSONUtils.getStringfromJSON(jsonResultObject,"icon"));

                result.setId(JSONUtils.getStringfromJSON(jsonResultObject,"id"));

                result.setName(JSONUtils.getStringfromJSON(jsonResultObject,"name"));



                JSONObject jsonOpeningHoursObject =JSONUtils.getJSONObject(jsonResultObject,"opening_hours");

                OpeningHour openingHour=new OpeningHour();


                openingHour.setOpen_now(JSONUtils.getBooleanfromJSON(jsonOpeningHoursObject,"open_now"));


                JSONArray jsonWeekdayArray=JSONUtils.getJSONArray(jsonOpeningHoursObject,"weekday_text");


                List<String> weekdayList=new ArrayList<String>();


                for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonWeekdayArray); k++)
                {

                    weekdayList.add(JSONUtils.getStringObject(jsonWeekdayArray,k));
                }

                openingHour.setWeekday_text(weekdayList);


                result.setOpening_hours(openingHour);


                JSONArray jsonPhotosArray = JSONUtils.getJSONArray(jsonResultObject,"photos");


                List<Photo> photoList=new ArrayList<Photo>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonPhotosArray); j++)
                {

                    JSONObject jsonPhotosObject = JSONUtils.getJSONObject(jsonPhotosArray,j);


                    Photo photo = new Photo();


                    photo.setPhoto_reference(JSONUtils.getStringfromJSON(jsonPhotosObject,"photo_reference"));


                    photo.setHeight(JSONUtils.getIntegerfromJSON(jsonPhotosObject,"height"));


                    photo.setWidth(JSONUtils.getIntegerfromJSON(jsonPhotosObject,"width"));



                    JSONArray jsonhtml_attributionsArray = JSONUtils.getJSONArray(jsonPhotosObject,"html_attributions");


                    List<String> html_attrList=new ArrayList<String>();


                    for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonhtml_attributionsArray); k++)
                    {

                        html_attrList.add(JSONUtils.getStringObject(jsonhtml_attributionsArray,k));
                    }

                    photo.setHtml_attributions(html_attrList);

                    photoList.add(photo);



                }


                result.setPhotos(photoList);



                result.setPlace_id(JSONUtils.getStringfromJSON(jsonResultObject,"place_id"));

                result.setPrice_level(JSONUtils.getIntegerfromJSON(jsonResultObject,"price_level"));

                result.setRating(JSONUtils.getDoublefromJSON(jsonResultObject,"rating"));

                result.setReference(JSONUtils.getStringfromJSON(jsonResultObject,"reference"));

                result.setScope(JSONUtils.getStringfromJSON(jsonResultObject,"scope"));




                JSONArray jsonTypesArray = JSONUtils.getJSONArray(jsonResultObject,"types");


                List<String> typesList=new ArrayList<String>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonTypesArray); j++)
                {

                    typesList.add(JSONUtils.getStringObject(jsonTypesArray,j));
                }


                result.setTypes(typesList);



                result.setVicinity(JSONUtils.getStringfromJSON(jsonResultObject,"vicinity"));

                resultList.add(result);

            }

            myObject.setResults(resultList);


            Message message=handler.obtainMessage(1,myObject);
            message.sendToTarget();

        }
    }
}
