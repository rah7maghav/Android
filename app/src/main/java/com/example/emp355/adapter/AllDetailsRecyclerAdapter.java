package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.model.MyObject;
import com.example.emp355.model.Result;

import java.util.List;

public class AllDetailsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<Result> ResultsList;
    private Context mContext;

    public AllDetailsRecyclerAdapter(Context mContext,MyObject myObject) {
        if(myObject!=null)
        {
            ResultsList=myObject.getResults();
        }

        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(mContext).inflate(R.layout.alldetailslayout,parent,false);

        return new DetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


            if(ResultsList!=null)
            {
                Result result=ResultsList.get(position);

                if(result!=null)
                {
                    ((DetailsHolder) holder).name.setText(result.getName());
                    ((DetailsHolder) holder).vicinity.setText(result.getVicinity());

                }
            }


    }

    @Override
    public int getItemCount() {
        return ResultsList.size();
    }

    public class DetailsHolder extends RecyclerView.ViewHolder{

        TextView name,vicinity;

        public DetailsHolder(View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.textview_name);
            vicinity=itemView.findViewById(R.id.textview_vicinity);
        }
    }

}
