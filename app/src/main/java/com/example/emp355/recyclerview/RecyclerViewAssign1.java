package com.example.emp355.recyclerview;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.adapter.RecyclerViewAdapter;

public class RecyclerViewAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_assign1);




        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recyclerview1);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);

        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerViewAdapter adapter=new RecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);


      /*  DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(recyclerView.getContext(),linearLayoutManager.getOrientation());
       dividerItemDecoration.setDrawable(getDrawable(R.drawable.hollowrectangle));
        recyclerView.addItemDecoration(dividerItemDecoration);*/

    }

    public void Next(View view)
    {
        Intent i=new Intent(this, RecyclerViewAssign2.class);
        startActivity(i);
    }

}
