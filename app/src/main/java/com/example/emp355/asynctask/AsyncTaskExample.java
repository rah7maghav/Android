package com.example.emp355.asynctask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.multithreading.AsyncExampleActivity;

import java.lang.ref.WeakReference;

public class AsyncTaskExample extends AppCompatActivity {


    private EditText mEnterNumber;
    private TextView mResult;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_example);

        mEnterNumber=findViewById(R.id.edittext_enternum);
        Button enter = findViewById(R.id.button_enter);
        mResult=findViewById(R.id.textview_result);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DataLoader(AsyncTaskExample.this).execute(mEnterNumber.getText().toString());
            }
        });

    }


    //AysncTask class
    public static class DataLoader extends AsyncTask<String,Integer,String>{

        private WeakReference<AsyncTaskExample> activityWeakReference;

        private DataLoader(AsyncTaskExample activity)
        {
            activityWeakReference= new WeakReference<>(activity);
        }


        // invoked on the main UI thread before the task is executed.
        @Override
        protected void onPreExecute() {

            if(activityWeakReference.get()!=null)
            {
                activityWeakReference.get().mProgressDialog=new ProgressDialog(activityWeakReference.get());
                activityWeakReference.get().mProgressDialog.setMessage("Uploading");
                activityWeakReference.get().mProgressDialog.setCancelable(true);
                activityWeakReference.get().mProgressDialog.show();
            }


        }

        //invoked on the background thread immediately after onPreExecute() finishes its execution.
        @Override
        protected String doInBackground(String... values) {
           for (int i=0;i<=Integer.parseInt(values[0]);i++) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return values[0]+" Completed";
        }


        //invoked on the main UI thread after a call to publishProgress(Progress…).
        @Override
        protected void onProgressUpdate(Integer... integers) {

            if(activityWeakReference.get()!=null) {
                Log.d("prog", String.valueOf(integers[0]));
                activityWeakReference.get().mResult.setText(String.valueOf(integers[0]));
            }
        }

        //invoked on the main UI thread after the background operation finishes in the doInBackground method.
        @Override
        protected void onPostExecute(String values) {

            if(activityWeakReference.get()!=null) {

                activityWeakReference.get().mProgressDialog.dismiss();
                activityWeakReference.get().mResult.setText(values);
            }
        }
    }
}
