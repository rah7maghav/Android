package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Prediction implements Parcelable {

    private String description;
    private List<Matched_Substring> matched_substrings;
    private List<Term> terms;
    private Structured_Formatting structured_formatting;


    private String id;
    private String place_id;
    private String reference;
    private List<String> types;



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Matched_Substring> getMatched_substrings() {
        return matched_substrings;
    }

    public void setMatched_substrings(List<Matched_Substring> matched_substrings) {
        this.matched_substrings = matched_substrings;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public Structured_Formatting getStructured_formatting() {
        return structured_formatting;
    }

    public void setStructured_formatting(Structured_Formatting structured_formatting) {
        this.structured_formatting = structured_formatting;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeList(this.matched_substrings);
        dest.writeList(this.terms);
        dest.writeParcelable(this.structured_formatting, flags);
        dest.writeString(this.id);
        dest.writeString(this.place_id);
        dest.writeString(this.reference);
        dest.writeStringList(this.types);
    }

    public Prediction() {
    }

    protected Prediction(Parcel in) {
        this.description = in.readString();
        this.matched_substrings = new ArrayList<Matched_Substring>();
        in.readList(this.matched_substrings, Matched_Substring.class.getClassLoader());
        this.terms = new ArrayList<Term>();
        in.readList(this.terms, Term.class.getClassLoader());
        this.structured_formatting = in.readParcelable(Structured_Formatting.class.getClassLoader());
        this.id = in.readString();
        this.place_id = in.readString();
        this.reference = in.readString();
        this.types = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Prediction> CREATOR = new Parcelable.Creator<Prediction>() {
        @Override
        public Prediction createFromParcel(Parcel source) {
            return new Prediction(source);
        }

        @Override
        public Prediction[] newArray(int size) {
            return new Prediction[size];
        }
    };
}
