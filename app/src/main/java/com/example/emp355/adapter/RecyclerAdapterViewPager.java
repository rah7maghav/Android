package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emp355.R;

import java.util.ArrayList;

import static com.example.emp355.Constant.DATE;
;
import static com.example.emp355.Constant.SHARE;
import static com.example.emp355.Constant.WASHINGTON_JOURNEY;

public class RecyclerAdapterViewPager extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
  //  private String third_text;
    private ArrayList<String> list;
    private int third_icon;

    public  RecyclerAdapterViewPager(Context context,ArrayList<String> list,int third_icon)
    {
        mContext=context;
        this.list=list;
       // this.third_text=third_text;
        this.third_icon=third_icon;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


       View view= LayoutInflater.from(mContext).inflate(R.layout.viewpagercourseslayout,parent,false);

        return new CoursesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        ((CoursesHolder) holder).coursename.setText(list.get(0));
        ((CoursesHolder) holder).coursedate.setText(DATE);
        ((CoursesHolder) holder).courselikes.setText(list.get(1));
        ((CoursesHolder) holder).courseshare.setText(SHARE);
        ((CoursesHolder) holder).coursethirdtext.setText(list.get(2));

        ((CoursesHolder) holder).courselikeicon.setImageResource(R.drawable.viewpagerlikeicon);

        ((CoursesHolder) holder).courseshareicon.setImageResource(R.drawable.viewpagershareicon);

        ((CoursesHolder) holder).coursethirdicon.setImageResource(third_icon);



    }

    @Override
    public int getItemCount() {
        return 10;
    }





    public class CoursesHolder extends RecyclerView.ViewHolder{

        TextView coursename,coursedate,courselikes,courseshare,coursethirdtext;
        ImageView courselikeicon,courseshareicon,coursethirdicon;



        public CoursesHolder (View itemView)
        {
            super(itemView);

            coursename=(TextView)itemView.findViewById(R.id.course_name);
            coursedate=(TextView)itemView.findViewById(R.id.course_date);
            courselikes=(TextView)itemView.findViewById(R.id.course_likes);
            courseshare=(TextView)itemView.findViewById(R.id.course_share);
            coursethirdtext=(TextView)itemView.findViewById(R.id.course_thirdtext);

            courselikeicon=(ImageView)itemView.findViewById(R.id.course_likeicon);
            courseshareicon=(ImageView)itemView.findViewById(R.id.course_shareicon);
            coursethirdicon=(ImageView)itemView.findViewById(R.id.course_thirdicon);

        }




    }
}
