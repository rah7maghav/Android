package com.example.emp355.edittext;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.button.ButtonAssign1;

public class EditTextAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_assign1);

        final EditText editText=(EditText)findViewById(R.id.name);
        final EditText editText1=(EditText)findViewById(R.id.password);
        final EditText editText2=(EditText)findViewById(R.id.phone);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String name=editText.getText().toString();
                TextView tv1=(TextView)findViewById(R.id.input_name);
                tv1.setText(name);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }





        });


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String password=editText1.getText().toString();
                TextView tv2=(TextView)findViewById(R.id.input_password);
                tv2.setText(password);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String phone=editText2.getText().toString();
                TextView tv3=(TextView)findViewById(R.id.input_phone);
                tv3.setText(phone);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void Next(View view)
    {
        Intent i=new Intent(this, ButtonAssign1.class);
        startActivity(i);
    }

}
