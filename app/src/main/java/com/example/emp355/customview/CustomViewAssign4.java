package com.example.emp355.customview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.emp355.R;

public class CustomViewAssign4 extends AppCompatActivity {


    private SeekBar mSeekbar;
    private TextView mSeekbarProgShow;
    private CircleViewAssign4 mCircleViewAssign4;
    private LinearLayout mLinearLayoutParent;

    private int mWidthParentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view_assign4);


            mSeekbar=(SeekBar)findViewById(R.id.seekbar_progress);
            mSeekbarProgShow=(TextView)findViewById(R.id.textview_showprogress);
            mCircleViewAssign4=(CircleViewAssign4)findViewById(R.id.customview_seekbarprogress);


            mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    int mProgress=mSeekbar.getProgress();

                    mSeekbarProgShow.setText(String.valueOf(mProgress));

                    mCircleViewAssign4.UpdateView(mProgress);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

    }
}
