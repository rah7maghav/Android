package com.example.emp355.listview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.emp355.R;
import com.example.emp355.adapter.ListViewMultipleAdapter;
import com.example.emp355.gridview.GridViewSimple;

public class ListViewMultipleView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_multiple_view);

        ListView list = findViewById(R.id.list_view3);

        ListViewMultipleAdapter adapter = new ListViewMultipleAdapter(this);
        list.setAdapter(adapter);

    }


    public void Next(View view) {
        Intent i = new Intent(this, GridViewSimple.class);
        startActivity(i);
    }
}
