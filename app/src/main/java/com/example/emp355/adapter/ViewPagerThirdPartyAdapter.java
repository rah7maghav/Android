package com.example.emp355.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.emp355.Constant;
import com.example.emp355.R;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Image;
import com.example.emp355.thirdpartylibraries.ThirdPartyAssignFragment;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerThirdPartyAdapter extends FragmentPagerAdapter {

    //Title List containing title for each tab
    private List<String> mTitleList=new ArrayList<String>();

    //Image List containing required title and string url for images
    private List<Image> mImageList;


    //initializes the Title List and Image List
    public ViewPagerThirdPartyAdapter(FragmentManager fm, DataObject dataObject) {
        super(fm);

        mImageList=dataObject.getImages();


        for (int i=0;i<mImageList.size();i++)
        {
            mTitleList.add(mImageList.get(i).getName());

        }

    }


    //returns the fragment with required data
    @Override
    public Fragment getItem(int position) {

        //image Url List for particular tab(header)
         ArrayList<String> imageUrlList=new ArrayList<String>();

        switch (position)
        {
            case 0:
                imageUrlList=getList(position);
                break;
            case 1:

                imageUrlList=getList(position);
                break;
            case 2:

                imageUrlList=getList(position);
                break;
            case 3:

                imageUrlList=getList(position);
                break;
            case 4:

                imageUrlList=getList(position);
                break;
        }
        Bundle bundle = new Bundle();

        bundle.putStringArrayList(Constant.IMAGES_URL_DATA, imageUrlList);

        ThirdPartyAssignFragment thirdPartyAssignFragment = new ThirdPartyAssignFragment();
        thirdPartyAssignFragment.setArguments(bundle);
        return thirdPartyAssignFragment;
    }


    //returns a list with required data for each tab
    private ArrayList<String> getList(int pos)
    {
            return (ArrayList<String>) mImageList.get(pos).getValues();
    }

    //Returns total number of tabs(header)
    @Override
    public int getCount() {
        return mTitleList.size();
    }


    //sets the title for each tab
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }
}
