package com.example.emp355.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;

public class TextViewAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view_assign1);
    }

    public void Next(View view)
    {
        Intent i=new Intent(this,TextViewAssign2.class);
        startActivity(i);

    }
}
