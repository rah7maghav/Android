package com.example.emp355.roompersistencelib;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.adapter.AllUsersRecyclerAdapter;
import com.example.emp355.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import static com.example.emp355.Constant.ADAPTER_POSITION;
import static com.example.emp355.Constant.USER_CONTACT;
import static com.example.emp355.Constant.USER_EMAILID;
import static com.example.emp355.Constant.USER_ID;
import static com.example.emp355.Constant.USER_NAME;
import static com.example.emp355.Constant.USER_PASS;

public class FetchUser extends AppCompatActivity implements OnItemClickListener {

    private AllUsersRecyclerAdapter mAdapter;
    private List<NewUser> list=new ArrayList<NewUser>();
    private UserDao mUserDao;
    private ProgressDialog mProgressDialog;

    private static final int USER_REQ=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_user);

        MyDatabase myDatabase;

        //gets the instance of MyDatabase class
        myDatabase=MyDatabase.getInstance(getApplicationContext());
        mUserDao=myDatabase.userDao();

        RecyclerView allUsers;

        allUsers=findViewById(R.id.recyclerview_allusers);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        allUsers.setLayoutManager(linearLayoutManager);

        // Creates a new RecyclerViewAdapter
        mAdapter=new AllUsersRecyclerAdapter(this,list,this);

        // Sets the adapter for the ListView
        allUsers.setAdapter(mAdapter);

        new NewAysnc(1,-1).execute();


    }

    //gets the result from UpdateUserActivity to Update record
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==USER_REQ && resultCode==RESULT_OK && data!=null)
        {
            String username=data.getStringExtra(USER_NAME);
            String useremail=data.getStringExtra(USER_EMAILID);
            String userpass=data.getStringExtra(USER_PASS);
            String usercontact=data.getStringExtra(USER_CONTACT);
            int listposition=data.getIntExtra(ADAPTER_POSITION,-1);
            int userid=data.getIntExtra(USER_ID,-1);

            if(listposition!=-1 && userid!=-1)
            {
                NewUser newUser=list.get(listposition);

                newUser.setId(userid);
                newUser.setName(username);
                newUser.setEmail(useremail);
                newUser.setPassword(userpass);
                newUser.setContactno(usercontact);

                new NewAysnc(3,listposition).execute(newUser);


            }

        }

    }

    //handles Onclick Operation on RecyclerViewAdapter for Update and Delete
    @Override
    public void onItemClick(View view, int position) {

        NewUser newUser=list.get(position);
        int id=newUser.getId();//Id is automatically set while inserting data
        Log.d("ID", id+"");
        switch (view.getId())
        {
            //Update Onclick
            case R.id.button_update:

                Intent intent=new Intent(FetchUser.this,UpdateUserActivity.class);
                intent.putExtra(USER_ID,id);
                intent.putExtra(ADAPTER_POSITION,position);
                startActivityForResult(intent,USER_REQ);

                break;

            //Delete Onclick
            case R.id.button_delete:

                new NewAysnc(2,position).execute(newUser);

                break;
        }

    }

    @Override
    public void onItemClick(View view, Cursor cursor) {

    }

    public class NewAysnc extends AsyncTask<NewUser,Void,Integer>
    {
        int i,listposition;

        public NewAysnc(int i,int listposition) {
            this.listposition=listposition;
            this.i=i;
        }

        @Override
        protected void onPreExecute() {

            //progress dialog while fetching all records
            if(i==1)
            {
                mProgressDialog=new ProgressDialog(FetchUser.this);
                mProgressDialog.setMessage("Uploading");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }

        }


        @Override
        protected Integer doInBackground(NewUser... newUsers) {
           int returnvalue=0;

            //fetches all records
            if(i==1)
            {
                //gets all users
                list.addAll(mUserDao.getAllUsers());
            }

            //deletes record
            else if(i==2)
            {
                //returns number of rows deleted
                returnvalue=mUserDao.delete(newUsers[0]);
            }

            //updates record
            else if(i==3)
            {
                //returns number of rows updated
                returnvalue=mUserDao.update(newUsers[0]);
            }


            return returnvalue;
        }

        @Override
        protected void onPostExecute(Integer value) {

            //notifies adapter for read all records operation
            if(i==1)
            {
                mProgressDialog.dismiss();
                mAdapter.notifyDataSetChanged();
            }

            //notifies adapter for delete operation
            else if(i==2 && value>0)
            {
                list.remove(listposition);
                mAdapter.notifyItemRemoved(listposition);
                Toast.makeText(FetchUser.this,"Successfully Deleted",Toast.LENGTH_SHORT).show();
            }

            //notifies adapter for update operation
            else if(i==3 && value>0)
            {
                mAdapter.notifyItemChanged(listposition);
                Toast.makeText(FetchUser.this,"Successfully Updated",Toast.LENGTH_SHORT).show();
            }

        }

    }

}
