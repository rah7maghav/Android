package com.example.emp355.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.AllDataObject;
import com.example.emp355.model.Item;
import com.example.emp355.model.ItemImage;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

public class ImageDetailsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //NetworkingAssign2Activity Context
    private Context mContext;

    //Click Listener for an item
    private OnItemClickListener onItemClickListener;

    //list containing all the urls of all images
    private List<Item> mItemList;

    //the text that is being searched
    private String mSearchedText;

    //list containing the all the filenames of given directory in the device
    private List<String> mDeviceFileList;


    //initializes variables
    public ImageDetailsRecyclerAdapter(Context context, List<Item> itemList,String searchedText,List<String> deviceFileList,OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mItemList = itemList;
        this.onItemClickListener=onItemClickListener;
        mSearchedText=searchedText;
        mDeviceFileList=deviceFileList;
    }



    //inflates the layout for searched images
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.imagedownloadlayout,parent,false);

        return new ImageDetailsHolder(view);
    }





    //binds the imageview to obtained url using Universal Image Loader
    // based on position
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(mItemList!=null)
        {
            ItemImage itemImage=mItemList.get(position).getImage();

            if(itemImage!=null)
            {
                if(itemImage.getThumbnailLink()!=null)
                {
                    //filename of image to be saved
                    String fileName=mSearchedText+"_"+position+".jpg";

                    //checks if the image is already downloaded or not
                    if(mDeviceFileList.contains(fileName))
                    {
                        //if downloaded then removing the download icon on the image
                        ((ImageDetailsHolder) holder).downloadIcon.setVisibility(View.GONE);
                    }
                    else{

                        //if not downloaded then showing the download icon on the image
                    ((ImageDetailsHolder) holder).downloadIcon.setVisibility(View.VISIBLE);
                    }


                    //setting the imageview to required url using universal image laoder
                    ImageLoader.getInstance()
                            .displayImage(itemImage.getThumbnailLink()//string url
                                    ,((ImageDetailsHolder) holder).imageView);//imageview for showing image

                }


            }

        }

    }




    //returns the total images(items) in the recycler view
    @Override
    public int getItemCount() {
        return mItemList.size();
    }


    //holder for image details
    public class ImageDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imageView,downloadIcon;


        public ImageDetailsHolder(View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageview_data);
            downloadIcon=itemView.findViewById(R.id.imageview_download);

            downloadIcon.setOnClickListener(this);
            imageView.setOnClickListener(this);
        }

        //onclick for clicking an item
        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null)
            {
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }

        }

    }

    }

