package com.example.emp355.customview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import static com.example.emp355.Constant.ODD_NOT_ALLOWED;

public class CustomViewAssign3 extends AppCompatActivity {


    private int mNumber;
    private EditText mNumberText;
    private Button mClick;
    private CountDownCustomView mCountDownCustomView;
    private MyCountDownTimer mCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view_assign3);


        mNumberText = (EditText) findViewById(R.id.edittext_enternumber);
        mClick = (Button) findViewById(R.id.button_clickme);
        mCountDownCustomView = (CountDownCustomView) findViewById(R.id.customview_countdown);


        mNumberText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                int mTemp = 0;

                try {
                    mNumber = Integer.parseInt(mNumberText.getText().toString());
                } catch (NumberFormatException e) {
                    mTemp = 1;
                }

                if (mNumber > 0 && mNumber % 2 != 0 && mTemp == 0) {
                    Toast.makeText(getApplicationContext(), ODD_NOT_ALLOWED, Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        mClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                long totaltime = Long.parseLong(mNumberText.getText().toString());

                mCountDownTimer = new MyCountDownTimer(totaltime * 1000, (long) 1000, mCountDownCustomView, CustomViewAssign3.this);

                mCountDownTimer.start();


            }
        });

    }
}
