package com.example.emp355.kotlin


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.emp355.Constant
import com.example.emp355.R
import kotlinx.android.synthetic.main.fragment_assign_third_party.*

class AssignThirdPartyFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_assign_third_party, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var imageUrlList:ArrayList<String> =ArrayList()

        val bundle=arguments

        if(bundle!=null)
        {
            imageUrlList=bundle.getStringArrayList(Constant.IMAGES_URL_DATA)
        }


        val gridLayoutManager= GridLayoutManager(activity,2,LinearLayoutManager.VERTICAL,false)
        recyclerview_thirdparty.layoutManager=gridLayoutManager

        val mImageDataRecyclerAdapter= RecyclerAdapterImageData(activity!!, imageUrlList)
        recyclerview_thirdparty.adapter=mImageDataRecyclerAdapter

    }
}
