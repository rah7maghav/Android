package com.example.emp355.drawabletoolbox;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.example.emp355.R;

import top.defaults.drawabletoolbox.DrawableBuilder;

public class DrawableToolboxActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawable_toolbox);

        Button button=findViewById(R.id.button_update);

        DrawableBuilder drawableBuilder=new DrawableBuilder();

        Drawable drawable= drawableBuilder
                .rectangle()
                .hairlineBordered()
                .longDashed()
                .rounded()
                .strokeColor(R.color.Yellow)
                .strokeColorPressed(R.color.Red)
                .ripple()
                .build();

        button.setBackground(drawable);

    }
}
