package com.example.emp355.filestorage;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.File;
import java.io.FileOutputStream;

public class FileStorageAssign extends AppCompatActivity implements View.OnClickListener {

    private Button mCreateDir,mDeleteDir,mCreateFile,mDeleteFile;
    private EditText mInput;
    private int i=2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_storage_assign);


        mCreateDir=findViewById(R.id.button_createdir);
        mDeleteDir=findViewById(R.id.button_deletedir);
        mCreateFile=findViewById(R.id.button_createfile);
        mDeleteFile=findViewById(R.id.button_deletefile);
        mInput=findViewById(R.id.edittext_input);

        mCreateDir.setOnClickListener(this);
        mDeleteDir.setOnClickListener(this);
        mCreateFile.setOnClickListener(this);
        mDeleteFile.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.button_createdir:

                File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), mInput.getText().toString());
                if(!dir.exists())
                {
                     dir.mkdir();

                    Toast.makeText(FileStorageAssign.this,"Directory created",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(FileStorageAssign.this,"Directory already created",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.button_deletedir:

                File dir1=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),mInput.getText().toString());

                if(dir1.exists() && dir1.isDirectory())
                {
                   boolean temp=dir1.delete();//deletes only empty directory
                    if(temp)
                    {
                        Toast.makeText(FileStorageAssign.this,"Directory Deleted",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(FileStorageAssign.this,"Cannot Delete Directory as Directory Not Empty",Toast.LENGTH_SHORT).show();
                    }


                }
                else{
                    Toast.makeText(FileStorageAssign.this,"Nothing to delete",Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.button_createfile:

                File dir2=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),"Training");
                if(!dir2.exists())
                {
                    dir2.mkdir();
                }

                File file=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+"/Training","Sample_1.txt");

                while (file.exists() && file.isFile())
                {

                     file=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+"/Training",getFileName());
                }

                try
                {
                    file.createNewFile();
                    FileOutputStream fileOutputStream=new FileOutputStream(file);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    Toast.makeText(FileStorageAssign.this,"File Created",Toast.LENGTH_SHORT).show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;


            case R.id.button_deletefile:

                File delfile=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)+"/Training",mInput.getText().toString());

                if(delfile.exists() && delfile.isFile())
                {
                   delfile.delete();//also deletes files doesn't matter empty or not

                    Toast.makeText(FileStorageAssign.this,"File Deleted",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(FileStorageAssign.this,"Nothing to delete",Toast.LENGTH_SHORT).show();
                }
                break;




        }


    }

    private String getFileName() {
        return "Sample_" + i++ + ".txt";
    }


}
