package com.example.emp355.networking;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.emp355.Constant.DOWNLOAD_ALREADY_MESSAGE;
import static com.example.emp355.Constant.DOWNLOAD_FAILURE_MESSAGE;
import static com.example.emp355.Constant.DOWNLOAD_MESSAGE;
import static com.example.emp355.Constant.DOWNLOAD_SUCCESS_MESSAGE;
import static com.example.emp355.Constant.IMAGE_FILENAME;
import static com.example.emp355.Constant.IMAGE_POSITION;
import static com.example.emp355.Constant.IMAGE_URL;
import static com.example.emp355.Constant.SEARCHED_TEXT;


//service class for image downloading and saving process
public class DownloadIntentService extends IntentService {


    public DownloadIntentService() {
        //name of the service
        super("downloadimage");
    }

    //performs the downloading operation of image and
    // saves it in internal storage of device
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        String imageURL=intent.getStringExtra(IMAGE_URL);
        String searchedText=intent.getStringExtra(SEARCHED_TEXT);
        int imagePosition=intent.getIntExtra(IMAGE_POSITION,-1);
        String fileName="";
        String message=DOWNLOAD_FAILURE_MESSAGE;
        Bitmap myBitmap=null;
        InputStream input;

        //waits for 10 secs
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {

            //establishing the url connection to download the image

            URL url = new URL(imageURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();

            connection.setRequestMethod("GET");

            int statusCode=connection.getResponseCode();


            /* 200 represents HTTP OK */
            if(statusCode==200)
            {
                 input = connection.getInputStream();
                 myBitmap = BitmapFactory.decodeStream(input);

            }

            if(myBitmap!=null)
            {

                File dir=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),"search_images");

                //checks whether this directory exists or not
                if(!dir.exists())
                {
                    dir.mkdir();
                }

                //gets the filename of image to be saved on the device
                fileName=getFileName(searchedText,imagePosition);

                //Creates a new File in the specified dir directory
                //with a specific filename
                File file=new File(dir,fileName);

                //checks whether file already exists with a given filename
                //in the specified directory
                if (file.exists() && file.isFile())
                {
                    message=DOWNLOAD_ALREADY_MESSAGE;
                }

                //creates a new file and saves it on the device
                else {

                    try
                    {
                        file.createNewFile();
                        FileOutputStream fileOutputStream=new FileOutputStream(file);
                        boolean flag=myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                        fileOutputStream.flush();
                        fileOutputStream.close();

                        //checks whether image is saved on the device or not
                        if(flag)
                        {
                            message=DOWNLOAD_SUCCESS_MESSAGE;
                        }

                    }

                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }


            }

        } catch (Exception e) {
            e.printStackTrace();

        }



        //uses broadcast receiver to send the download message status to
        //NetworkingAssign2Activity
        Intent broadcastIntent=new Intent("downloadimagemessage");
        broadcastIntent.putExtra(IMAGE_POSITION,imagePosition);
        broadcastIntent.putExtra(DOWNLOAD_MESSAGE,message);
        broadcastIntent.putExtra(IMAGE_FILENAME,fileName);
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(broadcastIntent);





    }

    //returns the filename of image to be saved
    private String getFileName(String seachedText,int imagePosition) {

        return seachedText+"_"+imagePosition+".jpg";

    }
}
