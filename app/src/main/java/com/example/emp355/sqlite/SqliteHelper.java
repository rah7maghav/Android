package com.example.emp355.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.emp355.contentprovider.UserEntry;

import java.util.ArrayList;
import java.util.HashSet;

public class SqliteHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="User_Details.db";

    private static SqliteHelper sInstance;
    private SQLiteDatabase mDatabase;

    private SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    public static SqliteHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SqliteHelper(context);
        }
        return sInstance;
    }

    //creates tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(UserTable.CREATE_TABLE_QUERY);
            db.execSQL(LikesTable.CREATE_TABLE_QUERY);
            db.execSQL(UserEntry.CREATE_TABLE_QUERY);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    //upgrades database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + UserTable.TABLE_NAME);
        db.execSQL("drop table if exists " + LikesTable.TABLE_NAME);
        db.execSQL("drop table if exists " + UserEntry.TABLE_NAME);
        onCreate(db);
    }


    //inserts userdetails to user table
    public long insert(String firstName,String lastName,String phone,String email,String password) {

        mDatabase = getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(UserTable.FIRST_NAME,firstName);
        contentValues.put(UserTable.LAST_NAME,lastName);
        contentValues.put(UserTable.PHONE,phone);
        contentValues.put(UserTable.EMAIL,email);
        contentValues.put(UserTable.PASSWORD,password);

        long id=mDatabase.insert(UserTable.TABLE_NAME,null,contentValues);
        mDatabase.close();
        return id;
    }

    //checks whether email entered exists in db or not
    public boolean checkExistingEmail(String email)
    {
        mDatabase=getReadableDatabase();
        String selection=UserTable.EMAIL+"=?";
        String selectionArgs[]={email};
        Cursor cursor=mDatabase.query(UserTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);
        if(cursor.getCount()==1)
        {
            mDatabase.close();
            return true;
        }
        else  {
            mDatabase.close();
            return false;
        }
    }

    //validates user based on email and password entered while log in
    public long validateUser(String email,String password)
    {
        mDatabase=getReadableDatabase();
        String selection=UserTable.EMAIL+"=? AND "+UserTable.PASSWORD+"=?";
        String selectionArgs[]={email,password};
        Cursor cursor=mDatabase.query(UserTable.TABLE_NAME,null,selection,selectionArgs,null,null,null);

        if(cursor.getCount()==1 && cursor.moveToFirst())
        {
            int id=cursor.getInt(cursor.getColumnIndex(UserTable.ID));
            mDatabase.close();
            return id;
        }
        else {
            mDatabase.close();
            return -1;
        }
    }

    /**
     * return user-Data corresponding to id
     * @param id
     * @return
     */
    public MyUser getUser(long id) {

        mDatabase=getReadableDatabase();

        String selection=UserTable.ID+"=?";
        String selectionArgs[]={id+""};

        Cursor cursor = mDatabase.query(
                 UserTable.TABLE_NAME,
                null,
                 selection,
                 selectionArgs,
                null,
                null,
                null);

        MyUser myUser=new MyUser();

        if(cursor.getCount()==1 && cursor.moveToFirst())
        {
            myUser.setFirstname(cursor.getString(cursor.getColumnIndex(UserTable.FIRST_NAME)));
            myUser.setLastname(cursor.getString(cursor.getColumnIndex(UserTable.LAST_NAME)));
            myUser.setEmail(cursor.getString(cursor.getColumnIndex(UserTable.EMAIL)));
            myUser.setPhonenumber(cursor.getString(cursor.getColumnIndex(UserTable.PHONE)));
            myUser.setDob(cursor.getString(cursor.getColumnIndex(UserTable.DOB)));
            mDatabase.close();
            return myUser;
        }
        else {
            mDatabase.close();
            return null;
        }
    }

    //updates userdetails based on id
    public int updateUser(long id,String firstName,String lastName,String phone,String dob)
    {
        mDatabase=getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(UserTable.FIRST_NAME,firstName);
        contentValues.put(UserTable.LAST_NAME,lastName);
        contentValues.put(UserTable.PHONE,phone);
        contentValues.put(UserTable.DOB,dob);
        String whereClause=UserTable.ID+"=?";
        String whereArgs[]={id+""};

        int numofrowsupdated=mDatabase.update(UserTable.TABLE_NAME,contentValues,whereClause,whereArgs);
        mDatabase.close();
        return numofrowsupdated;

    }

    //gets all userdetails except current logged in user
    public ArrayList<MyUser> getAllUsersExceptCurrent(long id)
    {
        mDatabase=getReadableDatabase();

        String selection=UserTable.ID+" <> ?";
        String selectionArgs[]={id+""};
        ArrayList<MyUser> myList=new ArrayList<MyUser>();
        Cursor cursor=mDatabase.query(UserTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);

        while (cursor!=null && cursor.getCount()>0 && cursor.moveToNext())
        {
            MyUser myUser=new MyUser();

            myUser.setId(cursor.getInt(cursor.getColumnIndex(UserTable.ID)));
            myUser.setFirstname(cursor.getString(cursor.getColumnIndex(UserTable.FIRST_NAME)));
            myUser.setLastname(cursor.getString(cursor.getColumnIndex(UserTable.LAST_NAME)));
            myUser.setEmail(cursor.getString(cursor.getColumnIndex(UserTable.EMAIL)));
            myUser.setPhonenumber(cursor.getString(cursor.getColumnIndex(UserTable.PHONE)));
            myUser.setPassword(cursor.getString(cursor.getColumnIndex(UserTable.PASSWORD)));
            myUser.setDob(cursor.getString(cursor.getColumnIndex(UserTable.DOB)));
            myList.add(myUser);
        }
        mDatabase.close();
        return myList;
    }


    //inserts record on like by any user into likes table
    public void likesTableInsert(int likedById,int likedToId)
    {
        mDatabase=getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(LikesTable.LIKED_BY,likedById);
        contentValues.put(LikesTable.LIKED_TO,likedToId);

        mDatabase.insert(LikesTable.TABLE_NAME,null,contentValues);
        mDatabase.close();

    }

    //deletes record on unlike by any user from likes table
    public void deleteRecordOnUnlike(int likedById,int likedToId)
    {
        mDatabase=getWritableDatabase();

        String where=LikesTable.LIKED_BY+"=? AND "+LikesTable.LIKED_TO+"=?";
        String whereArgs[]={likedById+"",likedToId+""};

        int i=mDatabase.delete(LikesTable.TABLE_NAME,where,whereArgs);
        Log.d("i", "deleteRecordOnUnlike: "+i);
        mDatabase.close();
    }

    //gets all records of liked_to column from likes table
    public ArrayList<Integer> getAllLikedToData() {

        mDatabase=getReadableDatabase();
        String columns[]={LikesTable.LIKED_TO};

        Cursor cursor = mDatabase.query(
                LikesTable.TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null);

        ArrayList<Integer> myList=new ArrayList<Integer>();
        while (cursor!=null && cursor.getCount()>0 && cursor.moveToNext())
        {
            myList.add(cursor.getInt(cursor.getColumnIndex(LikesTable.LIKED_TO)));

        }
        mDatabase.close();
        return myList;
    }

    //fetches all users who have liked current logged in user
    public ArrayList<MyUser> getLikedByUsers(long id)
    {
        mDatabase=getReadableDatabase();

        String selection=LikesTable.LIKED_TO+"= ?";
        String selectionArgs[]={id+""};
        ArrayList<MyUser> myList=new ArrayList<MyUser>();
        String columns[]={LikesTable.LIKED_BY};

        Cursor cursor=mDatabase.query(LikesTable.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        while (cursor!=null && cursor.getCount()>0 && cursor.moveToNext())
        {
            String selection1=UserTable.ID+"= ?";
            String selectionArgs1[]={cursor.getInt(cursor.getColumnIndex(LikesTable.LIKED_BY))+""};

            Cursor cursor1=mDatabase.query(UserTable.TABLE_NAME,
                    null,
                    selection1,
                    selectionArgs1,
                    null,
                    null,
                    null);
            if(cursor1!=null && cursor1.getCount()==1 && cursor1.moveToFirst())
            {
                MyUser myUser=new MyUser();

                myUser.setId(cursor1.getInt(cursor1.getColumnIndex(UserTable.ID)));
                myUser.setFirstname(cursor1.getString(cursor1.getColumnIndex(UserTable.FIRST_NAME)));
                myUser.setLastname(cursor1.getString(cursor1.getColumnIndex(UserTable.LAST_NAME)));
                myUser.setEmail(cursor1.getString(cursor1.getColumnIndex(UserTable.EMAIL)));
                myUser.setPhonenumber(cursor1.getString(cursor1.getColumnIndex(UserTable.PHONE)));
                myList.add(myUser);
            }

        }
        mDatabase.close();
        return myList;
    }

    //checks whether current logged in user likes other users or not
    public HashSet<Integer> checkCurrentUserLikesOrNot(int likedById) {

        mDatabase=getReadableDatabase();

        String selection=LikesTable.LIKED_BY+"=?";
        String selectionArgs[]={likedById+""};
        String columns[]={LikesTable.LIKED_TO};

        Cursor cursor = mDatabase.query(
                LikesTable.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        HashSet<Integer> hashSet=new HashSet<Integer>();

        while (cursor!=null && cursor.getCount()>0 && cursor.moveToNext())
        {
              hashSet.add(cursor.getInt(cursor.getColumnIndex(LikesTable.LIKED_TO)));

        }
        mDatabase.close();
        return hashSet;
    }

}
