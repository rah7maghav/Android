package com.example.emp355.recyclerview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.emp355.R;
import com.example.emp355.adapter.RecyclerViewAdapter;
import com.example.emp355.adapter.RecyclerViewAdapter2;


public class RecyclerViewAssign2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_assign2);

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recyclerview2);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerViewAdapter2 adapter=new RecyclerViewAdapter2(RecyclerViewAssign2.this);
        recyclerView.setAdapter(adapter);

    }

    public void Next(View view)
    {
        Intent i=new Intent(this, RecyclerViewAssign3.class);
        startActivity(i);
    }


}
