package com.example.emp355.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.emp355.R;

public class RectangleView extends View{



    private int mRectangleColor;
    private Paint mRectanglePaint;

    private int mRectangleTop = 20;
    private int mRectangleLeft = 20;

    private int mRectangleWidth ;
    private int mRectangleHeight ;

    public RectangleView(Context context) {
        super(context);
        init(context, null);
    }

    public RectangleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RectangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);


    }

    public RectangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);

    }

    private void init(Context c,AttributeSet attrs) {



        TypedArray mArray=c.getTheme().obtainStyledAttributes(attrs, R.styleable.RectangleView,0,0);
        try {
            mRectangleColor = mArray.getColor(R.styleable.RectangleView_rectanglecolor, Color.parseColor("#000000"));

        }
        finally {
            mArray.recycle();
        }

        mRectanglePaint=new Paint();
        mRectanglePaint.setStyle(Paint.Style.FILL);
        mRectanglePaint.setColor(mRectangleColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mRectangleHeight=getHeight()-20;
        mRectangleWidth=getWidth()-20;

        canvas.drawRect(mRectangleLeft,mRectangleTop,mRectangleWidth,
                mRectangleHeight,mRectanglePaint);
    }
}
