package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ObjectData implements Parcelable {

    private String status;
    private String error_message;
    private List<Prediction> predictions;



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.error_message);
        dest.writeList(this.predictions);
    }

    public ObjectData() {
    }

    protected ObjectData(Parcel in) {
        this.status = in.readString();
        this.error_message = in.readString();
        this.predictions = new ArrayList<Prediction>();
        in.readList(this.predictions, Prediction.class.getClassLoader());
    }

    public static final Parcelable.Creator<ObjectData> CREATOR = new Parcelable.Creator<ObjectData>() {
        @Override
        public ObjectData createFromParcel(Parcel source) {
            return new ObjectData(source);
        }

        @Override
        public ObjectData[] newArray(int size) {
            return new ObjectData[size];
        }
    };
}
