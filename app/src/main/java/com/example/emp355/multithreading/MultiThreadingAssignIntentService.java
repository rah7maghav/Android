package com.example.emp355.multithreading;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.emp355.R;
import com.example.emp355.adapter.AllDetailsRecyclerAdapter;
import com.example.emp355.model.MyObject;

public class MultiThreadingAssignIntentService extends AppCompatActivity {


    private ProgressDialog mProgressDialog;
    private AllDetailsRecyclerAdapter allDetailsRecyclerAdapter;
    private RecyclerView allDetailsRecyclerView;
    private MyBroadCastReceiver myBroadCastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_threading_assign_intent_service);

        IntentFilter intentFilter=new IntentFilter("parcelabledata");
        myBroadCastReceiver=new MyBroadCastReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(myBroadCastReceiver,intentFilter);

        allDetailsRecyclerView=findViewById(R.id.recyclerview_alldetails);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        allDetailsRecyclerView.setLayoutManager(linearLayoutManager);


        Intent intent=new Intent(this,MyIntentService.class);

        startService(intent);
        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setMessage("Uploading");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadCastReceiver);
    }

    public class MyBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            MyObject myObject=intent.getParcelableExtra("data");
            allDetailsRecyclerAdapter=new AllDetailsRecyclerAdapter(MultiThreadingAssignIntentService.this,myObject);
            allDetailsRecyclerView.setAdapter(allDetailsRecyclerAdapter);
            mProgressDialog.dismiss();
        }
    }

}
