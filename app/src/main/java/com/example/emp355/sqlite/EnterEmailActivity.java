package com.example.emp355.sqlite;

import android.content.Intent;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.emp355.R;

import static com.example.emp355.Constant.EMAIL_ENTERED;
//Enter Email Activity
public class EnterEmailActivity extends AppCompatActivity {



    private EditText mEmail;
    private SqliteHelper mSqliteHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_email);

        Button mNext;

        mEmail=findViewById(R.id.edittext_enteremail);
        mNext=findViewById(R.id.button_next);

        mSqliteHelper=SqliteHelper.getInstance(EnterEmailActivity.this);


        mNext.setOnClickListener(new View.OnClickListener() {

           @Override
            public void onClick(View v) {

               boolean temp=mSqliteHelper.checkExistingEmail(mEmail.getText().toString());
                startActivity(new Intent(EnterEmailActivity.this,
                        temp ? EnterPasswordActivity.class : RegisterActivity.class)
                        .putExtra(EMAIL_ENTERED,mEmail.getText().toString()));

              //  finish();
            }
        });

    }

    //handles backPress for Email Activity
    @Override
    public void onBackPressed() {
        /*Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);*/
        finishAffinity();


    }
}
