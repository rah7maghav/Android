package com.example.emp355.contentprovider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

public class InsertData extends AppCompatActivity {

    private EditText mFullName,mEmail,mAge,mContactNo,mGender;
    private Uri mUri;
    private long mId;
    private ProviderObserver observer;
    private ContentValues[] mContentValues=new ContentValues[10];
    private int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data);


        mFullName=findViewById(R.id.edittext_fullname);
        mEmail=findViewById(R.id.edittext_idemail);
        mAge=findViewById(R.id.edittext_age);
        mContactNo=findViewById(R.id.edittext_contactno);
        mGender=findViewById(R.id.edittext_gender);
        observer = new ProviderObserver(new Handler());
        getContentResolver().registerContentObserver(UserEntry.CONTENT_URI, true, observer);



    }

    //Method for adding data to contentvalues array for bulk insertion
    public void addUser(View view)
    {
        mContentValues[i]=new ContentValues();
        mContentValues[i].put(UserEntry.COLUMN_FULLNAME,mFullName.getText().toString());
        mContentValues[i].put(UserEntry.COLUMN_EMAIL,mEmail.getText().toString());
        mContentValues[i].put(UserEntry.COLUMN_AGE,mAge.getText().toString());
        mContentValues[i].put(UserEntry.COLUMN_CONTACTNO,mContactNo.getText().toString());
        mContentValues[i].put(UserEntry.COLUMN_GENDER,mGender.getText().toString());
        i++;
        Toast.makeText(this,"Added row",Toast.LENGTH_SHORT).show();
    }

    //Method for inserting data
    public void InsertData(View view)
    {
        ContentValues values=new ContentValues();
        values.put(UserEntry.COLUMN_FULLNAME,mFullName.getText().toString());
        values.put(UserEntry.COLUMN_EMAIL,mEmail.getText().toString());
        values.put(UserEntry.COLUMN_AGE,mAge.getText().toString());
        values.put(UserEntry.COLUMN_CONTACTNO,mContactNo.getText().toString());
        values.put(UserEntry.COLUMN_GENDER,mGender.getText().toString());


        //inserts data using specific uri
        mUri=getContentResolver().insert(UserEntry.CONTENT_URI,values);
        mId =ContentUris.parseId(mUri);
        Log.d("uri", "Uri: "+mUri+"  id: "+mId);
    }

    //Method for fetching data
    //Moves to FetchData Activity
    public void FetchData(View view)
    {
        startActivity(new Intent(this,FetchData.class));
    }

    //Method for bulk insertion of data
    public void bulkInsert(View view)
    {
        //returns number of rows inserted
       int numberofrowsinserted= getContentResolver().bulkInsert(UserEntry.CONTENT_URI,mContentValues);
        Toast.makeText(this,"Inserted "+numberofrowsinserted+" rows",Toast.LENGTH_SHORT).show();

    }
}
