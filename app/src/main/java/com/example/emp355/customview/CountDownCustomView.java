package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class CountDownCustomView extends View{


    private Paint mRectanglePaint;

    private int mRectangleRight;
    private int mRectangleBottom;
    private int mRectangleHeight=100;
    private int mRectangleTop;
    private int mRectangleLeft = 80;

    public CountDownCustomView(Context context) {
        super(context);

        init();
    }

    public CountDownCustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CountDownCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    private void init()
    {
        notDraw();
        mRectanglePaint=new Paint();

        mRectanglePaint.setStyle(Paint.Style.FILL);
        mRectanglePaint.setColor(Color.BLUE);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mRectangleRight=getWidth()-80;
        mRectangleBottom=getHeight();

        canvas.drawRect(mRectangleLeft,mRectangleTop,mRectangleRight,mRectangleBottom,mRectanglePaint);
    }


    //Updates the Custom View
    public void UpdateView(boolean mTemp,boolean mTemp1) {

        if (mTemp1)
        {
            mRectangleTop=getHeight()-mRectangleHeight;
        }

        else {

            if (mTemp) {
                mRectangleTop = mRectangleTop - mRectangleHeight;
            } else {
                mRectangleTop = mRectangleTop + mRectangleHeight;
            }
        }

        this.setWillNotDraw(false);
        invalidate();

    }

    public void notDraw()
    {
        this.setWillNotDraw(true);//onDraw will not be called

    }

}
