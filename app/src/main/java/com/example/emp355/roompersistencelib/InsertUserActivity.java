package com.example.emp355.roompersistencelib;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

public class InsertUserActivity extends AppCompatActivity {

    private EditText mName,mEmail,mPassword,mContactNo;

    private UserDao mUserDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_user);

        MyDatabase myDatabase;

        //gets the instance of MyDatabase class
        myDatabase=MyDatabase.getInstance(getApplicationContext());
        mUserDao=myDatabase.userDao();

        mName=findViewById(R.id.edittext_name);
        mEmail=findViewById(R.id.edittext_email);
        mPassword=findViewById(R.id.edittext_password);
        mContactNo=findViewById(R.id.edittext_contactno);

    }


    //Method for inserting data
    public void insertData(View view)
    {

        NewUser newUser=new NewUser();

        newUser.setName(mName.getText().toString());
        newUser.setEmail(mEmail.getText().toString());
        newUser.setPassword(mPassword.getText().toString());
        newUser.setContactno(mContactNo.getText().toString());
        new MyAsyncTask().execute(newUser);
    }

    //Method for fetching data
    //moves to FetchUser activity
    public void fetchData(View view)
    {
        startActivity(new Intent(this,FetchUser.class));
    }



    public class MyAsyncTask extends AsyncTask<NewUser,Void,Long>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(NewUser... newUsers) {

            //inserts data
            //returns id of inserted row
            long value=mUserDao.insert(newUsers[0]);

            return value;
        }

        @Override
        protected void onPostExecute(Long value) {

            if(value>0)
            {
                Toast.makeText(InsertUserActivity.this,"Successfully Inserted",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }
}
