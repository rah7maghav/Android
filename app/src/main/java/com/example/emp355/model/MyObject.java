package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyObject implements Parcelable {

    private List<String> html_attributions;
    private String next_page_token;
    private List<Result> results;
    private String status;

    private String error_message;
    private Result resultobj;


    public Result getResultobj() {
        return resultobj;
    }

    public void setResultobj(Result resultobj) {
        this.resultobj = resultobj;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public List getHtml_attributions() {
        return html_attributions;
    }

    public void setHtml_attributions(List html_attributions) {
        this.html_attributions = html_attributions;
    }

    public String getNext_page_token() {
        return next_page_token;
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    public List getResults() {
        return results;
    }

    public void setResults(List results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.html_attributions);
        dest.writeString(this.next_page_token);
        dest.writeTypedList(this.results);
        dest.writeString(this.status);
        dest.writeString(this.error_message);
        dest.writeParcelable(this.resultobj, flags);
    }

    public MyObject() {
    }

    protected MyObject(Parcel in) {
        this.html_attributions = in.createStringArrayList();
        this.next_page_token = in.readString();
        this.results = in.createTypedArrayList(Result.CREATOR);
        this.status = in.readString();
        this.error_message = in.readString();
        this.resultobj = in.readParcelable(Result.class.getClassLoader());
    }

    public static final Parcelable.Creator<MyObject> CREATOR = new Parcelable.Creator<MyObject>() {
        @Override
        public MyObject createFromParcel(Parcel source) {
            return new MyObject(source);
        }

        @Override
        public MyObject[] newArray(int size) {
            return new MyObject[size];
        }
    };
}
