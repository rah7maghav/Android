package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.emp355.R;

public class CircleViewAssign4 extends View {

    private float mRadius = 0;
    private Paint mCirclePaint;

    private int mYCoordinate;
    private int mXCoordinate;

    public CircleViewAssign4(Context context) {
        this(context, null, 0, 0);

    }

    public CircleViewAssign4(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);


    }

    public CircleViewAssign4(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);

    }

    public CircleViewAssign4(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        notDraw();
        mCirclePaint = new Paint();
        mCirclePaint.setStyle(Paint.Style.FILL);
        mCirclePaint.setColor(getResources().getColor(R.color.customviewcirclecolor));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        System.out.println("width :: " + getMeasuredWidth() + "height :: " + getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mYCoordinate = this.getHeight() / 2;//(X,Y)to mark center
        mXCoordinate = this.getWidth() / 2;

        canvas.drawCircle(mXCoordinate, mYCoordinate, mRadius, mCirclePaint);
    }

    public void UpdateView(int mProgress) {

        float width=getWidth();
        float height=getHeight();

        mRadius = mProgress * (Math.min(width,height) / 200);

        this.setWillNotDraw(false);
        invalidate();
    }

    public void notDraw() {
        this.setWillNotDraw(true);
    }
}
