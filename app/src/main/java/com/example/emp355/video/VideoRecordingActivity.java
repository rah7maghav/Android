package com.example.emp355.video;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.File;
import java.io.IOException;

public class VideoRecordingActivity extends AppCompatActivity {
    private static final int VIDEO_CAPTURE = 101;
    Uri videoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recording);

    }


    public void startRecordingVideo(View view) {

        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // Ensure that there's a video activity to handle the intent
        if (videoIntent.resolveActivity(getPackageManager()) != null) {

            // Create the File where the video should go
            File mediaFile = new File(
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/myvideo.mp4");

            //gets the uri from mediaFile
            videoUri = FileProvider.getUriForFile(this,
                    "com.example.emp355.assignment",
                    mediaFile);

            Log.d("uri",videoUri.toString());
            videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
            startActivityForResult(videoIntent, VIDEO_CAPTURE);
        }

        else
        {
            Toast.makeText(this, "No camera on device", Toast.LENGTH_LONG).show();
        }

    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Video has been saved to:\n", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Video recording cancelled.",  Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Failed to record video",  Toast.LENGTH_LONG).show();
            }
        }
    }
}
