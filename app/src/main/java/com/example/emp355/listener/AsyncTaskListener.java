package com.example.emp355.listener;

public interface AsyncTaskListener {

    void onTaskStarted();
    void taskProgress(Integer... integers);
    void onTaskCompleted(String result);

}
