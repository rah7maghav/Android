package com.example.emp355.uiassignment;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.toolbar.ToolbarAssign1;

public class Flight extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);


        Toolbar toolbar=(Toolbar)findViewById(R.id.flighttoolbarid);
        setSupportActionBar(toolbar);

        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
    }

    public void Next(View view)
    {
        Intent i=new Intent(this, Vessels.class);
        startActivity(i);
    }
}
