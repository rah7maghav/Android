package com.example.emp355.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.emp355.R;

public class RectangleDiagonalView extends View {

    private int mRectangleStrokeColor;
    private int mRectangleStrokeWidth;
    private Paint mRectanglePaint;



    private int mLineColor;
    private int mLineWidth;
    private Paint mLinePaint;

    private int mRectangleTop = 20;
    private int mRectangleLeft = 20;

    private int mRectangleRight ;
    private int mRectangleBottom ;



    public RectangleDiagonalView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }


    private void init(Context context,AttributeSet attributeSet)
    {
        mRectanglePaint=new Paint();

        TypedArray mArray=context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.RectangleDiagonalView,0,0);

        try {
          mRectangleStrokeColor=mArray.getColor(R.styleable.RectangleDiagonalView_strokecolor, Color.parseColor("#000000"));
          mRectangleStrokeWidth=mArray.getInteger(R.styleable.RectangleDiagonalView_strokewidth,0);
            mLineColor=mArray.getColor(R.styleable.RectangleDiagonalView_linecolor, Color.parseColor("#000000"));
            mLineWidth=mArray.getInteger(R.styleable.RectangleDiagonalView_linewidth,0);

        }
        finally {
            mArray.recycle();
        }

        mRectanglePaint=new Paint();
        mRectanglePaint.setStyle(Paint.Style.STROKE);
        mRectanglePaint.setColor(mRectangleStrokeColor);
        mRectanglePaint.setStrokeWidth(mRectangleStrokeWidth);

        mLinePaint=new Paint();
        mLinePaint.setStyle(Paint.Style.FILL);
        mLinePaint.setColor(mLineColor);
        mLinePaint.setStrokeWidth(mLineWidth);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        mRectangleBottom=getMeasuredHeight()-20;
        mRectangleRight=getMeasuredWidth()-20;

        canvas.drawRect(mRectangleLeft,mRectangleTop,mRectangleRight,mRectangleBottom,mRectanglePaint);

        canvas.drawLine(mRectangleLeft,mRectangleTop,mRectangleRight,mRectangleBottom,mLinePaint);
        canvas.drawLine(mRectangleLeft,mRectangleBottom,mRectangleRight,mRectangleTop,mLinePaint);


    }
}
