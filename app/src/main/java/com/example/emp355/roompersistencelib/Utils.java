package com.example.emp355.roompersistencelib;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class Utils {

    private void printAll(final Context context) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                List<NewUser> users = MyDatabase.getInstance(context).userDao().getAllUsers();
                Log.d("Database Users: ", users.toString());
            }
        });
    }
}
