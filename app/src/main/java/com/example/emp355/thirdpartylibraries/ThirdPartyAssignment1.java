package com.example.emp355.thirdpartylibraries;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.emp355.JSONUtils;
import com.example.emp355.R;
import com.example.emp355.adapter.ViewPagerThirdPartyAdapter;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Image;
import com.example.emp355.model.Result;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThirdPartyAssignment1 extends AppCompatActivity {

    private ViewPager mViewPager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_party_assignment1);

        String jsonFilename="json_imagesdata.json";
        new ParseJsonData().execute(jsonFilename);

        mViewPager=findViewById(R.id.viewpager);

        TabLayout tabLayout=findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(mViewPager);

    }


    //Async Task for parsing json data
    public class ParseJsonData extends AsyncTask<String,Void,DataObject>
    {

        //Shows Progress Dialog during parsing
        @Override
        protected void onPreExecute() {
            mProgressDialog=new ProgressDialog(ThirdPartyAssignment1.this);
            mProgressDialog.setMessage("Uploading");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        //parses json data into java object
        @Override
        protected DataObject doInBackground(String... values) {

            String jsonstringdata=loadJsonFromAssets(values[0]);


            JSONObject jsonObject= JSONUtils.getJSONObject(jsonstringdata);

            DataObject dataObject=new DataObject();

            JSONArray jsonImagesArray=JSONUtils.getJSONArray(jsonObject,"images");

            List<Image> imageList=new ArrayList<Image>();

            for(int i=0;i<JSONUtils.getLengthofJSONArray(jsonImagesArray);i++)
            {

                JSONObject jsonImagesObject=JSONUtils.getJSONObject(jsonImagesArray,i);

                Image image=new Image();

                image.setName(JSONUtils.getStringfromJSON(jsonImagesObject,"name"));


                JSONArray jsonValuesArray=JSONUtils.getJSONArray(jsonImagesObject,"values");

                List<String> valuesList=new ArrayList<String>();

                for(int j=0;j<JSONUtils.getLengthofJSONArray(jsonValuesArray);j++)
                {
                    valuesList.add(JSONUtils.getStringObject(jsonValuesArray,j));
                }

                image.setValues(valuesList);

                imageList.add(image);

                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            dataObject.setImages(imageList);



            return dataObject;
        }


        //loads json file to String
        private String loadJsonFromAssets(String fileName)
        {
            String json;

            try{
                InputStream inputStream=ThirdPartyAssignment1.this.getAssets().open(fileName);

                int size=inputStream.available();
                byte[] buffer=new byte[size];

                inputStream.read(buffer);
                inputStream.close();

                json=new String(buffer,"UTF-8");


            }
            catch (IOException e)
            {
                e.printStackTrace();
                return null;
            }

            return json;

        }

        //dismisses the dialog after parsing data and sets the data in the adapter
        @Override
        protected void onPostExecute(DataObject dataObject) {


            mProgressDialog.dismiss();

            //sets the adapter with required data
            ViewPagerThirdPartyAdapter ThirdPartyAdapter=new ViewPagerThirdPartyAdapter(getSupportFragmentManager(),dataObject);
            mViewPager.setAdapter(ThirdPartyAdapter);

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


}
