package com.example.emp355.gridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;


import com.example.emp355.R;

public class GridViewSimple extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_simple);


        final GridView gridView=(GridView)findViewById(R.id.gridview1);

        String values[]={"Data at pos 0","Data at pos 1",
                "Data at pos 2","Data at pos 3","Data at pos 4",
                "Data at pos 5","Data at pos 6","Data at pos 7","Data at pos 8",
                "Data at pos 9","Data at pos 10","Data at pos 11","Data at pos 12","Data at pos 13",
                "Data at pos 14","Data at pos 15","Data at pos 16",
                "Data at pos 17","Data at pos 18","Data at pos 19","Data at pos 20",
                "Data at pos 21","Data at pos 22","Data at pos 23","Data at pos 24","Data at pos 25",
                "Data at pos 26","Data at pos 27","Data at pos 28",
                "Data at pos 29","Data at pos 30","Data at pos 31","Data at pos 32",
                "Data at pos 33","Data at pos 34","Data at pos 35","Data at pos 36","Data at pos 37",
                "Data at pos 38","Data at pos 39","Data at pos 40",
                "Data at pos 41","Data at pos 42","Data at pos 43","Data at pos 44",
                "Data at pos 45","Data at pos 46","Data at pos 47","Data at pos 48","Data at pos 49"};



        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.item1,R.id.text_id1,values);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int itemPosition=position;

                String itemValue=(String) gridView.getItemAtPosition(position);

                Toast.makeText(getApplicationContext(),"Position:"+itemPosition+"  ListItem:"+itemValue,Toast.LENGTH_LONG).show();

            }
        });

    }

    public void Next(View view)
    {
        Intent i=new Intent(this, GridViewMultipleViewExample.class);
        startActivity(i);
    }



}
