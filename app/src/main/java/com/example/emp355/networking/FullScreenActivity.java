package com.example.emp355.networking;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static com.example.emp355.Constant.IMAGE_FILENAME;


//Activity to show the downloaded image on full screen
public class FullScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);

        String fileName;
        ImageView imageView;

        imageView=findViewById(R.id.imageview_fullscreen);

        if(getIntent()!=null)
        {
            fileName=getIntent().getStringExtra(IMAGE_FILENAME);


            File dir=new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),"search_images");
            if(dir.exists())
            {
                try {
                    File f=new File(dir, fileName);

                    //reading the bitmap of required image from the given directory in device
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));

                    if(b!=null)
                    {
                        //setting the bitmap to imageview
                        imageView.setImageBitmap(b);
                    }
                }
                catch (FileNotFoundException e)
                {
                    Toast.makeText(this,"Cannot Open Image",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


        }
        else {

        }


    }


    //destroying the activity on back pressed
    @Override
    public void onBackPressed() {

        finish();
    }
}
