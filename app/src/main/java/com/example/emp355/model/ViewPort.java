package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ViewPort implements Parcelable {

    private Location northeast;
    private Location southwest;

    public Location getNortheast() {
        return northeast;
    }

    public void setNortheast(Location northeast) {
        this.northeast = northeast;
    }

    public Location getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Location southwest) {
        this.southwest = southwest;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.northeast, flags);
        dest.writeParcelable(this.southwest, flags);
    }

    public ViewPort() {
    }

    protected ViewPort(Parcel in) {
        this.northeast = in.readParcelable(Location.class.getClassLoader());
        this.southwest = in.readParcelable(Location.class.getClassLoader());
    }

    public static final Parcelable.Creator<ViewPort> CREATOR = new Parcelable.Creator<ViewPort>() {
        @Override
        public ViewPort createFromParcel(Parcel source) {
            return new ViewPort(source);
        }

        @Override
        public ViewPort[] newArray(int size) {
            return new ViewPort[size];
        }
    };
}
