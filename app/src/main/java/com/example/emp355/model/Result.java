package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Result implements Parcelable {

    private Geometry geometry;
    private String icon;
    private String id;
    private String name;

    private OpeningHour opening_hours;
    private List<Photo> photos;
    private String place_id;
    private int price_level;
    private double rating;
    private String reference;
    private String scope;
    private List<String> types;
    private String vicinity;

    private List<Address_Component> address_components;
    private String adr_address;
    private String formatted_address;
    private Plus_Code plus_code;
    private List<Review> reviews;
    private String url;
    private int utc_offset;
    private String website;
    private String formatted_phone_number;
    private String international_phone_number;


    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OpeningHour getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(OpeningHour opening_hours) {
        this.opening_hours = opening_hours;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public int getPrice_level() {
        return price_level;
    }

    public void setPrice_level(int price_level) {
        this.price_level = price_level;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public List<Address_Component> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<Address_Component> address_components) {
        this.address_components = address_components;
    }

    public String getAdr_address() {
        return adr_address;
    }

    public void setAdr_address(String adr_address) {
        this.adr_address = adr_address;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public Plus_Code getPlus_code() {
        return plus_code;
    }

    public void setPlus_code(Plus_Code plus_code) {
        this.plus_code = plus_code;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUtc_offset() {
        return utc_offset;
    }

    public void setUtc_offset(int utc_offset) {
        this.utc_offset = utc_offset;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFormatted_phone_number() {
        return formatted_phone_number;
    }

    public void setFormatted_phone_number(String formatted_phone_number) {
        this.formatted_phone_number = formatted_phone_number;
    }

    public String getInternational_phone_number() {
        return international_phone_number;
    }

    public void setInternational_phone_number(String international_phone_number) {
        this.international_phone_number = international_phone_number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.geometry, flags);
        dest.writeString(this.icon);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.opening_hours, flags);
        dest.writeTypedList(this.photos);
        dest.writeString(this.place_id);
        dest.writeInt(this.price_level);
        dest.writeDouble(this.rating);
        dest.writeString(this.reference);
        dest.writeString(this.scope);
        dest.writeStringList(this.types);
        dest.writeString(this.vicinity);
        dest.writeTypedList(this.address_components);
        dest.writeString(this.adr_address);
        dest.writeString(this.formatted_address);
        dest.writeParcelable(this.plus_code, flags);
        dest.writeTypedList(this.reviews);
        dest.writeString(this.url);
        dest.writeInt(this.utc_offset);
        dest.writeString(this.website);
        dest.writeString(this.formatted_phone_number);
        dest.writeString(this.international_phone_number);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.geometry = in.readParcelable(Geometry.class.getClassLoader());
        this.icon = in.readString();
        this.id = in.readString();
        this.name = in.readString();
        this.opening_hours = in.readParcelable(OpeningHour.class.getClassLoader());
        this.photos = in.createTypedArrayList(Photo.CREATOR);
        this.place_id = in.readString();
        this.price_level = in.readInt();
        this.rating = in.readDouble();
        this.reference = in.readString();
        this.scope = in.readString();
        this.types = in.createStringArrayList();
        this.vicinity = in.readString();
        this.address_components = in.createTypedArrayList(Address_Component.CREATOR);
        this.adr_address = in.readString();
        this.formatted_address = in.readString();
        this.plus_code = in.readParcelable(Plus_Code.class.getClassLoader());
        this.reviews = in.createTypedArrayList(Review.CREATOR);
        this.url = in.readString();
        this.utc_offset = in.readInt();
        this.website = in.readString();
        this.formatted_phone_number = in.readString();
        this.international_phone_number = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}