package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import static com.example.emp355.Constant.TIMER_OVER;

public class MyCountDownTimer extends CountDownTimer{

    private boolean mTemp;
    private boolean mTemp1;
    private long mFutureMillis;
    private Context mContext;
    private CountDownCustomView mCountDownCustomView;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public MyCountDownTimer(long millisInFuture, long countDownInterval,CountDownCustomView mCountDownCustomView,Context context) {
        super(millisInFuture, countDownInterval);
        mFutureMillis=millisInFuture;
        mContext=context;
       this.mCountDownCustomView=mCountDownCustomView;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        Log.d("millis", "millis= "+millisUntilFinished+ "   futuremillis= "+mFutureMillis);

        if(millisUntilFinished>=(mFutureMillis-1000))
        {
            mTemp1=true;
        }
        else {
            mTemp1=false;
        }

        if(millisUntilFinished>mFutureMillis/2)
        {
             mTemp=true;
        }
        else
        {
             mTemp=false;
        }

        if(mCountDownCustomView!=null)
        {
            mCountDownCustomView.UpdateView(mTemp,mTemp1);
        }


    }

    @Override
    public void onFinish() {


        if(mCountDownCustomView!=null) {
            mCountDownCustomView.notDraw();

            Toast.makeText(mContext, TIMER_OVER, Toast.LENGTH_SHORT).show();
        }
    }


}
