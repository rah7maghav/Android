package com.example.emp355.navigationdrawer;


import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.fragments.AppleFragment;

import com.example.emp355.fragments.MangoFragment;
import com.example.emp355.fragments.OrangeFragment;

public class NavigationDrawerPractice extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer_practice);


        final DrawerLayout drawerLayout=(DrawerLayout)findViewById(R.id.drawerlayout);

        NavigationView navigationView=(NavigationView)findViewById(R.id.navigationview);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                Fragment fragment=null;

                int itemId=item.getItemId();

                switch (itemId)
                {
                    case R.id.firstitem:
                       fragment=new AppleFragment();
                       break;

                    case R.id.seconditem:
                        fragment= new OrangeFragment();
                        break;
                    case R.id.thirditem:
                        fragment= new MangoFragment();
                        break;
                }

                Toast.makeText(getApplicationContext(),item.getTitle(),Toast.LENGTH_LONG).show();

                if(fragment!=null)
                {
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_navdrawer,fragment).commit();
                    drawerLayout.closeDrawers();
                    return true;

                }
                return false;
            }
        });

    }
}
