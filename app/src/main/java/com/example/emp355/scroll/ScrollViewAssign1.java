package com.example.emp355.scroll;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.assignment.TextViewAssign1;

public class ScrollViewAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view_assign1);
    }

    public void sendMessage(View view)
    {
        Intent i=new Intent(this,TextViewAssign1.class);

        startActivity(i);

    }
}
