package com.example.emp355.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.emp355.R;


public class GridViewMultipleAdapter extends BaseAdapter
{

    private Context mContext;

    public GridViewMultipleAdapter(Context context)
    {
        mContext=context;
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {

        if(position==0)
        {
            return 0;
        }

        if (position % 5 == 0) {
            return 1;

        }
        return 0;
    }

    @Override
    public int getCount() {
        return 50;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int type=getItemViewType(position);

        if(convertView==null)
        {

            holder =new ViewHolder();

            if(type==0)
            {
                convertView= LayoutInflater.from(mContext).inflate(R.layout.item1,parent,false);
                holder.nameText=(TextView)convertView.findViewById(R.id.text_id1);
                holder.nameText.setText("Data at pos "+ position);
            }
            else
            {
                convertView= LayoutInflater.from(mContext).inflate(R.layout.circle,parent,false);

            }
            convertView.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)convertView.getTag();
            if(holder.nameText!=null)
            {
                holder.nameText.setText("Data at pos "+ position);
            }
        }


        return convertView;
    }

    public class ViewHolder
    {
        public TextView nameText;
    }
}