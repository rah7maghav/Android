package com.example.emp355.thirdpartylibraries;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.widget.LinearLayout;

import com.example.emp355.JSONUtils;
import com.example.emp355.R;
import com.example.emp355.adapter.MyStickyHeaderRecyclerAdapter;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Image;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThirdPartyAssignment2Activity extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private MyStickyHeaderRecyclerAdapter myStickyHeaderRecyclerAdapter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_party_assignment2);

        mRecyclerView = findViewById(R.id.recyclerview_images);

        String jsonFilename="json_imagesdata.json";
        new ParseImageJsonData().execute(jsonFilename);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(linearLayoutManager);



    }

    //Async Task for parsing json data
    public class ParseImageJsonData extends AsyncTask<String,Void,DataObject>
    {

        //Shows Progress Dialog during parsing
        @Override
        protected void onPreExecute() {
            mProgressDialog=new ProgressDialog(ThirdPartyAssignment2Activity.this);
            mProgressDialog.setMessage("Uploading");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        //loads json file to String
        private String loadJsonFromAssets(String fileName)
        {
            String json;

            try{
                InputStream inputStream=ThirdPartyAssignment2Activity.this.getAssets().open(fileName);

                int size=inputStream.available();
                byte[] buffer=new byte[size];

                inputStream.read(buffer);
                inputStream.close();

                json=new String(buffer,"UTF-8");


            }
            catch (IOException e)
            {
                e.printStackTrace();
                return null;
            }

            return json;

        }


        //parses json data into java object
        @Override
        protected DataObject doInBackground(String... values) {


            String jsonstringdata=loadJsonFromAssets(values[0]);


            JSONObject jsonObject= JSONUtils.getJSONObject(jsonstringdata);

            DataObject dataObject=new DataObject();

            JSONArray jsonImagesArray=JSONUtils.getJSONArray(jsonObject,"images");

            List<Image> imageList=new ArrayList<Image>();

            for(int i=0;i<JSONUtils.getLengthofJSONArray(jsonImagesArray);i++)
            {

                JSONObject jsonImagesObject=JSONUtils.getJSONObject(jsonImagesArray,i);

                Image image=new Image();

                image.setName(JSONUtils.getStringfromJSON(jsonImagesObject,"name"));


                JSONArray jsonValuesArray=JSONUtils.getJSONArray(jsonImagesObject,"values");

                List<String> valuesList=new ArrayList<String>();

                for(int j=0;j<JSONUtils.getLengthofJSONArray(jsonValuesArray);j++)
                {
                    valuesList.add(JSONUtils.getStringObject(jsonValuesArray,j));
                }

                image.setValues(valuesList);

                imageList.add(image);

                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            dataObject.setImages(imageList);



            return dataObject;

        }


        //dismisses the dialog after parsing data and sets the data in the adapter
        @Override
        protected void onPostExecute(DataObject dataObject) {
            mProgressDialog.dismiss();

            List<Image> ImageList=dataObject.getImages();
            List<String> imageUrlList;
            List<String> AllUrlList=new ArrayList<String>();
            List<String> TitleList=new ArrayList<String>();


            for(int i=0;i<ImageList.size();i++)
            {
                imageUrlList=ImageList.get(i).getValues();
                for(int j=0;j<imageUrlList.size();j++)
                {
                    AllUrlList.add(imageUrlList.get(j));
                    TitleList.add(ImageList.get(i).getName());
                }

            }

            //sets the adapter with required data
            myStickyHeaderRecyclerAdapter=new MyStickyHeaderRecyclerAdapter(ThirdPartyAssignment2Activity.this,AllUrlList,TitleList);
            mRecyclerView.setAdapter(myStickyHeaderRecyclerAdapter);
            mRecyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(myStickyHeaderRecyclerAdapter));

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
