package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emp355.R;



public class RecyclerViewAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    public RecyclerViewAdapter2(Context context)
    {

        mContext=context;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(mContext).inflate(R.layout.item3,parent,false);

        MyViewHolder myViewHolder=new MyViewHolder(v);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MyViewHolder holder1=(MyViewHolder) holder;

        holder1.name.setText("Data at pos "+ position);

    }

    @Override
    public int getItemCount() {
        return 50;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView name;

        public MyViewHolder(View itemView){
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.text_id3);


        }
    }
}