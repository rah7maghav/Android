package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.emp355.R;

public class RangeSeekbarView2 extends View {





    private Paint mCirclePaint;
    private Paint mGreenLinePaint;
    private Paint mGreyLinePaint;
    private Paint mTextLeftPaint;
    private Paint mTextRightPaint;

    private float mGreenLineStartX;
    private float mGreenLineStopX;

    private float mLeftBoundLeftSeekbar;
    private float mRightBoundLeftSeekbar;
    private float mTopBoundLeftSeekbar;
    private float mBottomBoundLeftSeekbar;


    private float mLeftBoundRightSeekbar;
    private float mRightBoundRightSeekbar;
    private float mTopBoundRightSeekbar;
    private float mBottomBoundRightSeekbar;

    private int mLeftSeekbarProgress;
    private int mRightSeekbarProgress;
    private int mLeftTempProgress;
    private int mRightTempProgress;

    private float mTouchPointX;
    private float mTouchPointY;


    private int i=0;
    private float temp1;
    private float temp2;



    public RangeSeekbarView2(Context context) {
        this(context,null,0,0);
    }

    public RangeSeekbarView2(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0,0);
    }

    public RangeSeekbarView2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public RangeSeekbarView2(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init()
    {
        mCirclePaint=new Paint();
        mCirclePaint.setStyle(Paint.Style.FILL);
        mCirclePaint.setColor(getResources().getColor(R.color.customviewcircle));



        mGreenLinePaint=new Paint();
        mGreenLinePaint.setStyle(Paint.Style.FILL);
        mGreenLinePaint.setColor(getResources().getColor(R.color.customviewgreenline));
        mGreenLinePaint.setStrokeWidth(25);


        mGreyLinePaint=new Paint();
        mGreyLinePaint.setStyle(Paint.Style.FILL);
        mGreyLinePaint.setColor(getResources().getColor(R.color.customviewsemicirclelarge));
        mGreyLinePaint.setStrokeWidth(20);

        mTextLeftPaint=new Paint();
        mTextLeftPaint.setStyle(Paint.Style.FILL);
        mTextLeftPaint.setColor(Color.BLACK);
        mTextLeftPaint.setTextSize(80);
        mTextLeftPaint.setTextAlign(Paint.Align.CENTER);

        mTextRightPaint=new Paint();
        mTextRightPaint.setStyle(Paint.Style.FILL);
        mTextRightPaint.setColor(Color.BLACK);
        mTextRightPaint.setTextSize(80);
        mTextRightPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);



        if(i==0)
        {
            mGreenLineStartX=50;

            mLeftSeekbarProgress=0;
            mRightSeekbarProgress=40;

            mGreenLineStopX=((mRightSeekbarProgress*(getWidth()-100))/100)+50;

            i++;

        }
        Log.d("I", "onDraw: "+i);



        mLeftBoundLeftSeekbar=mGreenLineStartX-30;
        mRightBoundLeftSeekbar=mGreenLineStartX+30;
        mTopBoundLeftSeekbar=50-30;
        mBottomBoundLeftSeekbar=50+30;


        mLeftBoundRightSeekbar=mGreenLineStopX-30;
        mRightBoundRightSeekbar=mGreenLineStopX+30;
        mTopBoundRightSeekbar=50-30;
        mBottomBoundRightSeekbar=50+30;


        canvas.drawLine(50,50,getWidth()-50,50,mGreyLinePaint);
        canvas.drawLine(mGreenLineStartX,50,mGreenLineStopX,50,mGreenLinePaint);

        canvas.drawCircle(mGreenLineStartX,50,30,mCirclePaint);
        canvas.drawCircle(mGreenLineStopX,50,30,mCirclePaint);

        canvas.drawText(String.valueOf(mLeftSeekbarProgress),200,200,mTextLeftPaint);
        canvas.drawText(String.valueOf(mRightSeekbarProgress),getWidth()-200,200,mTextRightPaint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        int eventAction=motionEvent.getAction();

        //float temp;
        int i=0;
        Log.d("I", "onTouchEvent: " +i);
        float mStopPointX;
        boolean flag=true;
        switch (eventAction)
        {

            case MotionEvent.ACTION_DOWN:
                mTouchPointX=motionEvent.getX();
                mTouchPointY=motionEvent.getY();

                Log.d("Touch", mTouchPointX+"  "+mTouchPointY+"  "+getHeight());
                break;

            case MotionEvent.ACTION_MOVE:

                mStopPointX=motionEvent.getX();

                Log.d("X", "onTouchEvent: "+mStopPointX);

                Log.d("getX", "onTouchEvent: "+motionEvent.getX()+" "+motionEvent.getY()+"  "+motionEvent.getRawX()+" "+motionEvent.getRawY());



                Log.d("L1", "onTouchEvent: "+mLeftBoundLeftSeekbar);
                Log.d("R1", "onTouchEvent: "+mRightBoundLeftSeekbar);
                Log.d("T1", "onTouchEvent: "+mTopBoundLeftSeekbar);
                Log.d("B1", "onTouchEvent: "+mBottomBoundLeftSeekbar);

                Log.d("L2", "onTouchEvent: "+mLeftBoundRightSeekbar);
                Log.d("R2", "onTouchEvent: "+mRightBoundRightSeekbar);
                Log.d("T2", "onTouchEvent: "+mTopBoundRightSeekbar);
                Log.d("B2", "onTouchEvent: "+mBottomBoundRightSeekbar);

               // temp=mStopPointX;

                if(((mLeftBoundLeftSeekbar-100)<mTouchPointX) && (mTouchPointX<(mRightBoundLeftSeekbar+100)))
                {
                    if(((mTopBoundLeftSeekbar-100)<mTouchPointY) && (mTouchPointY<(mBottomBoundLeftSeekbar+100)))
                    {
                        mGreenLineStartX=mStopPointX;
                        flag=true;
                        i=1;

                    }


                }
                else if(((mLeftBoundRightSeekbar-100)<mTouchPointX) && (mTouchPointX<(mRightBoundRightSeekbar+100)))
                {
                    if(((mTopBoundRightSeekbar-100)<mTouchPointY) && (mTouchPointY<(mBottomBoundRightSeekbar+100)))
                    {
                        mGreenLineStopX=mStopPointX;
                        flag=false;
                        i=1;
                    }


                }

                mLeftSeekbarProgress= (int)(((mGreenLineStartX-50)*100)/(getWidth()-100));

                mRightSeekbarProgress= (int)(((mGreenLineStopX-50)*100)/(getWidth()-100));

                Log.d("I1", "onTouchEvent: " +i);

                if((mRightSeekbarProgress-mLeftSeekbarProgress)==20)
                {

                    temp1=mGreenLineStartX;
                    temp2=mGreenLineStopX;
                    mLeftTempProgress=mLeftSeekbarProgress;
                    mRightTempProgress=mRightSeekbarProgress;
                }

                else if((mRightSeekbarProgress-mLeftSeekbarProgress)<20)
                {
                    mGreenLineStartX=temp1;
                    mGreenLineStopX=temp2;

                    mLeftSeekbarProgress=mLeftTempProgress;
                    mRightSeekbarProgress=mRightTempProgress;
                }

                else if(((mRightSeekbarProgress-mLeftSeekbarProgress)>40) && i==1)
                 {

                     if(flag)//if left seekbar moves beyond range 40
                     {
                         if(mLeftSeekbarProgress<0)
                         {
                             mGreenLineStartX=50;
                             mLeftSeekbarProgress=0;
                         }

                         mRightSeekbarProgress=mLeftSeekbarProgress+40;
                         mGreenLineStopX=((mRightSeekbarProgress*(getWidth()-100))/100)+50;

                     }
                     else//if right seekbar moves beyond range 40
                     {
                         if(mRightSeekbarProgress>100)
                         {
                             mGreenLineStopX=getWidth()-50;
                             mRightSeekbarProgress=100;
                         }

                         mLeftSeekbarProgress=mRightSeekbarProgress-40;
                         mGreenLineStartX=((mLeftSeekbarProgress*(getWidth()-100))/100)+50;
                     }

                 }


                if(mLeftSeekbarProgress<0)
                {
                    mGreenLineStartX=50;
                    mLeftSeekbarProgress=0;

                }
                if(mRightSeekbarProgress>100)
                {

                    mGreenLineStopX=getWidth()-50;
                    mRightSeekbarProgress=100;
                }


                break;

            case MotionEvent.ACTION_UP:

                break;
        }


        invalidate();


        return true;
    }


}
