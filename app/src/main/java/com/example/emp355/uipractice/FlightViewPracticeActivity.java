package com.example.emp355.uipractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.emp355.R;

public class FlightViewPracticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_view_practice);

        final Toolbar toolbar;
        final ImageView imageView,flightIcon;
        final LinearLayout layoutContent;


        toolbar = findViewById(R.id.toolbar_flightview);
        imageView = findViewById(R.id.imageview_flight_bg);
        flightIcon=findViewById(R.id.image_flighticon);
        layoutContent=findViewById(R.id.linearlayout_content);

        toolbar.setNavigationIcon(R.drawable.childviewmenu);
        toolbar.inflateMenu(R.menu.menu_toolbar_flightview);

       /*
        //to get height of device(screen)
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int deviceHeight = displayMetrics.heightPixels;

        toolbar.post(new Runnable() {
            @Override
            public void run() {

                int toolbarHeight = toolbar.getHeight();

                Log.d("height", "device: " + deviceHeight + "toolbar: " + toolbarHeight);
                int height = ((deviceHeight - toolbarHeight) * 33 / 100);
                Log.d("height", "final: " + height);

                imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));

            }
        });*/

        /**
         * gets height of flightIcon and setting margin top and padding top of layoutContent to
         * half of flightIcon's height
         */
       flightIcon.post(new Runnable() {
           @Override
           public void run() {

               int iconHeight=flightIcon.getHeight();

               FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutContent.getLayoutParams();
               params.setMargins(0, iconHeight/2, 0, 0);
               layoutContent.setLayoutParams(params);

               layoutContent.setPadding(0,iconHeight/2,0,0);

           }
       });

    }


}
