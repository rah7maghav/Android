package com.example.emp355.audio;

import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.File;
import java.io.IOException;

public class AudioPlaybackActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private MediaRecorder mediaRecorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_playback);


    }

    @Override
    protected void onResume() {
        super.onResume();

        mediaPlayer=new MediaPlayer();

        //Uri mp3Uri=Uri.parse("android.resource://com.example.emp355/" + R.raw.sampleaudio_1);
        Uri mp3Uri= Uri.parse("android.resource://com.example.emp355/raw/sampleaudio_1");
        Log.d("uri", String.valueOf(mp3Uri));
        try {
            mediaPlayer.setDataSource(this,mp3Uri);
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(this,String.valueOf((mediaPlayer.getDuration())/1000),Toast.LENGTH_SHORT).show();

    }

    public void start(View view) {

        mediaPlayer.start();
        Toast.makeText(this,String.valueOf((mediaPlayer.getCurrentPosition())/1000),Toast.LENGTH_SHORT).show();
    }

    public void stop(View view) {

        mediaPlayer.stop();
    }

    public void pause(View view) {

        mediaPlayer.pause();
    }


    @Override
    protected void onPause() {
        super.onPause();

        mediaPlayer.release();
        mediaPlayer=null;
    }



    public void audioPlaybackStreaming(View view) {

        String url = "https://dl.dropboxusercontent.com/u/10281242/sample_audio.mp3";
        final MediaPlayer mediaPlayerStream = new MediaPlayer();

        // Listen for if the audio file can't be prepared
        mediaPlayerStream.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });

        // Attach to when audio file is prepared for playing
        mediaPlayerStream.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayerStream.start();
            }
        });

        // Set the data source to the remote URL
        try {
            mediaPlayerStream.setDataSource(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Trigger an async preparation which will file listener when completed
        mediaPlayerStream.prepareAsync();


    }


    /**
     * method to record audio
     * @param view
     */
    public void recordAudio(View view) {

        // File path of recorded audio
        String mFileName;
        String LOG_TAG = "AudioRecordTest";

        // Verify that the device has a mic first
        PackageManager pmanager = this.getPackageManager();
        if (pmanager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {

            // Set the file location for the audio
            //mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();

            mFileName = getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
            mFileName += "/audiorecordtest1.3gp";

            Log.d("filename",mFileName);
            // Create the recorder
            mediaRecorder = new MediaRecorder();

            // Set the audio format and encoder
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            // Setup the output location
            mediaRecorder.setOutputFile(mFileName);

            // Start the recording
            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() failed");
            }

        }

        else
        { // no mic on device
            Toast.makeText(this, "This device doesn't have a mic!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * method to stop audio recording
     * @param view
     */
    public void stopRecording(View view) {

        mediaRecorder.stop();
        mediaRecorder.reset();
        mediaRecorder.release();

    }
}
