package com.example.emp355.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.emp355.R;
import com.example.emp355.fragments.ViewPagerFragment;

import java.util.ArrayList;
import java.util.List;

import static com.example.emp355.Constant.BOOKMARK;
import static com.example.emp355.Constant.DRAWABLE_ID;
import static com.example.emp355.Constant.FINISH;
import static com.example.emp355.Constant.GOA_TOUR;
import static com.example.emp355.Constant.ITEM_LIST;
import static com.example.emp355.Constant.LIKES_145;
import static com.example.emp355.Constant.LIKES_154;
import static com.example.emp355.Constant.LIKES_170;
import static com.example.emp355.Constant.REMOVE;
import static com.example.emp355.Constant.SOUTH_WOLD;
import static com.example.emp355.Constant.WASHINGTON_JOURNEY;

public class ViewPagerAssignAdapter extends FragmentPagerAdapter {

    private List<String> mTitleList;


    public ViewPagerAssignAdapter(FragmentManager fragmentManager, List<String> list)
    {
        super(fragmentManager);
        mTitleList=list;
    }

    @Override
    public Fragment getItem(int position) {

        ArrayList<String> arrayList = new ArrayList<String>();
        int icon = 0;

        switch (position) {

            case 0:
                icon = R.drawable.viewpagerfinishicon;

                arrayList= getList(WASHINGTON_JOURNEY,LIKES_145,FINISH);
                break;
            case 1:
                icon = R.drawable.viewpagerremoveicon;

                arrayList= getList(SOUTH_WOLD,LIKES_154,REMOVE);
                break;
            case 2:

                icon = R.drawable.viewpagerbookmarkicon;

                arrayList= getList(GOA_TOUR,LIKES_170,BOOKMARK);

                break;
        }
        Bundle bundle = new Bundle();

        bundle.putInt(DRAWABLE_ID, icon);
        bundle.putStringArrayList(ITEM_LIST, arrayList);

        ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
        viewPagerFragment.setArguments(bundle);
        return viewPagerFragment;


    }


    public ArrayList<String> getList(String name, String status, String key) {
        ArrayList<String> list = new ArrayList<>();
        list.add(name);
        list.add(status);
        list.add(key);
        return list;
    }

    @Override
    public int getCount() {
        return 3;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }
}
