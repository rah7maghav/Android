package com.example.emp355.viewpager;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.emp355.R;
import com.example.emp355.adapter.ViewPagerAssignAdapter;

import java.util.Arrays;
import java.util.List;

import static com.example.emp355.Constant.BOOKMARKS;
import static com.example.emp355.Constant.MY_COURSES;
import static com.example.emp355.Constant.NEARBY;

public class ViewPagerActivityAssign extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_activity_assign);

        Toolbar toolbar=(Toolbar)findViewById(R.id.viewpagertoolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);


        ViewPager viewPager=(ViewPager)findViewById(R.id.view_pager);


        TabLayout tabLayout=(TabLayout)findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        List<String> list= Arrays.asList(MY_COURSES, BOOKMARKS, NEARBY);

        ViewPagerAssignAdapter viewPagerAssignAdapter=new ViewPagerAssignAdapter(getSupportFragmentManager(),list);

        viewPager.setAdapter(viewPagerAssignAdapter);

    }
}
