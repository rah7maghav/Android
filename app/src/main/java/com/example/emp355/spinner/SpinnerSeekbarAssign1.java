package com.example.emp355.spinner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.drawables.DrawableAssign1;

public class SpinnerSeekbarAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_seekbar_assign1);


        final Spinner s=(Spinner)findViewById(R.id.spinner);

     s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            String selecteditem= s.getSelectedItem().toString();
             TextView t=(TextView)findViewById(R.id.spinner_sel);
             t.setText(selecteditem);

         }

         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
     });


        final SeekBar sk=(SeekBar)findViewById(R.id.seekbar);

        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                String seekbarprog= String.valueOf(sk.getProgress());
                TextView t=(TextView)findViewById(R.id.seekbar_prog);
                t.setText(seekbarprog);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    public void Next(View view)
    {
        Intent i=new Intent(this, DrawableAssign1.class);
        startActivity(i);
    }
}
