package com.example.emp355.contentprovider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.emp355.sqlite.SqliteHelper;

import java.util.ArrayList;

public class MyContentProvider extends ContentProvider{

    // Use an int for each URI we will run, this represents the different queries
    private static final int USER = 100;
    private static final int USER_ID = 101;

    private static final UriMatcher uriMatcher=buildUriMatcher();
    private SqliteHelper mSqliteHelper;

    /**
     * Builds a UriMatcher that is used to determine witch database request is being made.
     */
    public static UriMatcher buildUriMatcher()
    {
        String content=MyContractClass.CONTENT_AUTHORITY;

        // All paths to the UriMatcher have a corresponding code to return
        // when a match is found (the ints above).
        UriMatcher uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(content,MyContractClass.PATH_USER,USER);
        uriMatcher.addURI(content,MyContractClass.PATH_USER + "/#",USER_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        mSqliteHelper=SqliteHelper.getInstance(getContext());
        return true;
    }


    //retrieves data according to specific uri
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase sqLiteDatabase=mSqliteHelper.getReadableDatabase();

        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case USER:

                cursor=sqLiteDatabase.query(UserEntry.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                break;

            case USER_ID:
                long _id= ContentUris.parseId(uri);
                cursor=sqLiteDatabase.query(
                        UserEntry.TABLE_NAME,
                        projection,
                        UserEntry._ID+"=?",
                        new String[]{String.valueOf(_id)},
                        null,
                        null,
                        sortOrder);
                break;

                default:
                    throw new UnsupportedOperationException("Unknown uri: "+ uri);


        }
        return cursor;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        switch (uriMatcher.match(uri))
        {
            case USER:
                 return UserEntry.CONTENT_TYPE;

            case USER_ID:
                 return UserEntry.CONTENT_ITEM_TYPE;

                 default:
                     throw new UnsupportedOperationException("Unknown uri: "+ uri);
        }

    }


    //inserts record according to specific uri
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        final SQLiteDatabase sqLiteDatabase=mSqliteHelper.getWritableDatabase();
        long _id;
        Uri returnUri;

        switch (uriMatcher.match(uri))
        {
            case USER:
                _id=sqLiteDatabase.insert(UserEntry.TABLE_NAME,null,values);
                if(_id>0)
                {
                    returnUri=UserEntry.buildUserUri(_id);
                }
                else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);

        }

        // Use this on the URI passed into the function to notify any observers that the uri has
        // changed.
       getContext().getContentResolver().notifyChange(uri,null);
        return returnUri;

    }


    //delete records according to specific uri
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        final SQLiteDatabase sqLiteDatabase=mSqliteHelper.getWritableDatabase();
        int rows;

        switch (uriMatcher.match(uri))
        {
            case USER:
                //returns number of rows deleted
                rows=sqLiteDatabase.delete(UserEntry.TABLE_NAME,selection,selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);

        }
        if(selection == null || rows != 0){
            // Use this on the URI passed into the function to notify any observers that the uri has
            // changed.
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;
    }


    //updates records according to specific uri
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        final SQLiteDatabase sqLiteDatabase=mSqliteHelper.getWritableDatabase();
        int rows;

        switch (uriMatcher.match(uri))
        {
            case USER:
                //returns number of rows updated
                rows=sqLiteDatabase.update(UserEntry.TABLE_NAME,values,selection,selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if(rows != 0){
            // Use this on the URI passed into the function to notify any observers that the uri has
            // changed.
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;
    }


    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(operations);
    }


    //bulk inserts data
    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {

        SQLiteDatabase sqLiteDatabase=mSqliteHelper.getWritableDatabase();
        int rowsInserted=0;
        long _id;
        switch (uriMatcher.match(uri))
        {
            case USER:

                for (ContentValues contentValues:values)
                {
                    _id=sqLiteDatabase.insert(UserEntry.TABLE_NAME,null,contentValues);

                    if(_id>0)
                    {
                        rowsInserted++;
                    }

                }
                break;

                default:
                    throw new UnsupportedOperationException("Unknown uri: "+ uri);


        }

        // Use this on the URI passed into the function to notify any observers that the uri has
        // changed.
        getContext().getContentResolver().notifyChange(uri,null);
        return rowsInserted;
    }
}
