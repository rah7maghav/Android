package com.example.emp355.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.emp355.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

//Adapter for image data
public class ImageDataRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;

    //Image url list with required data for each tab
    private ArrayList<String> mImageUrllist ;


    public ImageDataRecyclerAdapter(Context mContext, ArrayList<String> mImageUrllist) {
        this.mContext = mContext;
        this.mImageUrllist = mImageUrllist;
    }

    //inflates the layout for images
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(mContext).inflate(R.layout.imagelayout,parent,false);


        return new ImageHolder(view);
    }

    //binds the url for images based on position
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(mImageUrllist!=null)
        {
            //using Universal Image Loader Library for loading and displaying image
            /*ImageLoader.getInstance()
                    .displayImage(mImageUrllist.get(position)//string url
                            ,((ImageHolder) holder).imageView);//imageview for showing image
*/
                //used Glide to load images
              /*  Glide.with(mContext)
                     .load(mImageUrllist.get(position))
                     .into(((ImageHolder) holder).imageView);*/

            //used Fresco to load images
           ((ImageHolder) holder).imageView.setImageURI(mImageUrllist.get(position));



          /* ImageLoader.getInstance().displayImage(mImageUrllist.get(position), ((ImageHolder) holder).imageView, null, new ImageLoadingListener() {
               @Override
               public void onLoadingStarted(String imageUri, View view) {

                   //Toast.makeText(mContext,"loading Started",Toast.LENGTH_SHORT).show();
               }

               @Override
               public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                   //Toast.makeText(mContext,"loading Failed",Toast.LENGTH_SHORT).show();
               }

               @Override
               public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                   //Toast.makeText(mContext,"loading Complete",Toast.LENGTH_SHORT).show();
               }

               @Override
               public void onLoadingCancelled(String imageUri, View view) {

                   //Toast.makeText(mContext,"loading Cancelled",Toast.LENGTH_SHORT).show();
               }
           });
*/
        }

    }


    //returns the total items in the recycler view
    @Override
    public int getItemCount() {
        return mImageUrllist.size();
    }


    //holder for images
    public class ImageHolder extends RecyclerView.ViewHolder{

        private SimpleDraweeView imageView;
        //private ImageView imageView;


        public ImageHolder(View itemView) {
            super(itemView);

           /* imageView=itemView.findViewById(R.id.imageview_data);*/
            imageView=itemView.findViewById(R.id.imageview_data);
        }


    }
}
