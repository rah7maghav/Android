package com.example.emp355.uipractice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.example.emp355.R;


/*Custom Text view class in which size of the Text view is according to font...*/
public class FontFitTextView extends TextView {

    // Attributes
    private Paint mTestPaint;
    /**
     * 'Initial' text size
     */
    private float mDefaultTextSize;

    public FontFitTextView(final Context context) {
        super(context);
        initialize();
    }

    public FontFitTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize() {
        mTestPaint = new Paint();
        mTestPaint.set(this.getPaint());
        Paint.FontMetrics defaultMetrics = getPaint().getFontMetrics();
        mDefaultTextSize = -1 * (defaultMetrics.descent + defaultMetrics.ascent);
    }

    /*
     * Re size the font so the specified text fits in the text box
     * assuming the text box is the specified width.
     */
    private void refitText(final String text, final int textHeight) {

        if (textHeight <= 0 || text.isEmpty()) {
            return;
        }

        int targetHeight = textHeight - this.getPaddingTop() - this.getPaddingBottom();

        // this is most likely a non-relevant call
        if (targetHeight <= 2) {
            return;
        }

        // text already fits with the xml-defined font size?
        mTestPaint.set(this.getPaint());
        Log.d("dimen", "default: "+mDefaultTextSize);
        mTestPaint.setTextSize(mDefaultTextSize);


        // adjust text size using binary search for efficiency
        float hi = Math.max(mDefaultTextSize, targetHeight);
        float lo = 2;
        final float threshold = 0.3f; // How close we have to be

        while (hi - lo > threshold) {
            float size = (hi + lo) / 2;
            Log.d("dimen", "size: "+size);
            mTestPaint.setTextSize(size);

            float newHeight = -1 * (mTestPaint.getFontMetrics().descent + mTestPaint.getFontMetrics().ascent);

            if (newHeight >= targetHeight) {
                hi = size; // too big
            } else {
                lo = size; // too small
            }
        }
        // Use lo so that we undershoot rather than overshoot
        int bottomSpace = (int) getResources().getDimension(R.dimen.dimen_2dp);
        Log.d("dimen", "final: "+lo + bottomSpace);
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, lo + bottomSpace);

    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int parentHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        refitText(this.getText().toString(), parentHeight);

        this.setMeasuredDimension(getMeasuredWidth(), parentHeight);
    }

    @Override
    protected void onTextChanged(final CharSequence text, final int start,
                                 final int before, final int after) {
        refitText(text.toString(), this.getHeight());
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
        if (w != oldw || h != oldh) {
            refitText(this.getText().toString(), h);
        }
    }




    //    @Override
//    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
//        super.onLayout(changed, left, top, right, bottom);
//        if (getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
//            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) getLayoutParams();
//            int topMargin = lp.topMargin - 15;
//            lp.setMargins(lp.leftMargin, topMargin, lp.rightMargin, lp.bottomMargin);
//            setLayoutParams(lp);
//
//        }
//    }


}