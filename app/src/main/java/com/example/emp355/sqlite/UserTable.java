package com.example.emp355.sqlite;

//table for user
public class UserTable {
    public static final String TABLE_NAME = "user";
    public static final String ID = "_id";
    public static final String FIRST_NAME = "first_name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String LAST_NAME = "last_name";
    public static final String DOB = "dob";


    public static final String CREATE_TABLE_QUERY=
            "CREATE TABLE "+TABLE_NAME+
                    "("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    +FIRST_NAME+" VARCHAR(255) ,"
                    +LAST_NAME+" VARCHAR(255) ,"
                    +EMAIL+" VARCHAR(255) ,"
                    +PHONE+" VARCHAR(255) ,"
                    +PASSWORD+" VARCHAR(255),"
                    + DOB+" VARCHAR(255));";

}
