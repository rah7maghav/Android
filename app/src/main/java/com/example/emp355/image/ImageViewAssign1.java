package com.example.emp355.image;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.linearlayout.LinearLayoutAssign2;
import com.example.emp355.listview.ListViewSimpleExample;

public class ImageViewAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view_assign1);
    }

    public void sendMessage(View view)
    {
        Intent i=new Intent(this,ListViewSimpleExample.class);

        startActivity(i);

    }
}
