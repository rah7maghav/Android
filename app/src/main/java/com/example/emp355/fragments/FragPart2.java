package com.example.emp355.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.adapter.Fragpart1RecyclerAdapter;
import com.example.emp355.adapter.MessageAdapter;

import java.util.ArrayList;
import java.util.List;


public class FragPart2 extends Fragment {

    TextView textView;
    Button button;
    RecyclerView recyclerView;
    List<String> list=new ArrayList<String>();
    MessageAdapter messageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frag_part2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=(RecyclerView)view.findViewById(R.id.messagesrecyclerview);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);


        recyclerView.setLayoutManager(linearLayoutManager);
        messageAdapter=new MessageAdapter(getActivity(),list);
        recyclerView.setAdapter(messageAdapter);


        textView = (TextView) view.findViewById(R.id.frag2name);
        button =(Button)view.findViewById(R.id.sendbutton);
       final EditText editText=(EditText)view.findViewById(R.id.typemessageid);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.add(editText.getText().toString());
                messageAdapter.notifyItemInserted(list.size()-1);

            }
        });


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
            String s = b.getString("uname");
            textView.setText(s);
        }
    }


}
