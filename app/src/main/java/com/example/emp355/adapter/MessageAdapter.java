package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emp355.R;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<String> list;


    public MessageAdapter(Context context,List<String> list)
    {
        mContext=context;
        this.list=list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        view= LayoutInflater.from(mContext).inflate(R.layout.messagelayout,parent,false);

        return new MessageHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(list!=null)
        {
            ((MessageHolder) holder).message.setText(list.get(position));
        }



    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class MessageHolder extends RecyclerView.ViewHolder
    {
        TextView message;

        public MessageHolder(View itemView)
        {
            super(itemView);
            message=(TextView)itemView.findViewById(R.id.message_textid);
        }
    }
}
