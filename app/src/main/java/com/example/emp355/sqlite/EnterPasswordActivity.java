package com.example.emp355.sqlite;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.emp355.R;

import com.example.emp355.sharedpref.MySharedPreferences;

import static com.example.emp355.Constant.EMAIL_ENTERED;
//Enter Password Activity
public class EnterPasswordActivity extends AppCompatActivity {

    private EditText mPassword;
    private SqliteHelper mSqliteHelper;
    private MySharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_password);

        Button logIn;
        String email="";

        mPassword = findViewById(R.id.edittext_enterpass);
        logIn=findViewById(R.id.button_login);


        if(getIntent()!=null)
        {
            email=getIntent().getStringExtra(EMAIL_ENTERED);
        }
        final String var=email;
        mSqliteHelper=SqliteHelper.getInstance(EnterPasswordActivity.this);


        mSharedPreferences=MySharedPreferences.getInstance(EnterPasswordActivity.this);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long userid=mSqliteHelper.validateUser(var,mPassword.getText().toString());
                if(userid==-1)
                {
                    Toast.makeText(EnterPasswordActivity.this,"Incorrect Password",Toast.LENGTH_LONG).show();

                }
                else {
                    mSharedPreferences.saveId(userid);
                    startActivity(new Intent(EnterPasswordActivity.this,AfterLoginActivity.class));
                    finishAffinity();
                }
            }
        });

    }


}
