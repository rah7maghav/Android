package com.example.emp355.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.edittext.EditTextAssign1;

public class TextViewAssign2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view_assign2);

        TextView t1=(TextView)findViewById(R.id.text_view1);
        String text=t1.getText().toString();

       int len=text.length();
        String text2=String.valueOf(len);
       text2="The Length is "+text2;

        TextView t2=(TextView)findViewById(R.id.text_view2);
        t2.setText(text2);
    }


    public void Next(View view)
    {
        Intent i=new Intent(this, EditTextAssign1.class);
        startActivity(i);
    }
}
