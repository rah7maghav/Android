package com.example.emp355.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<String> mTitleList=new ArrayList<String>();
    private List<Fragment> mList=new ArrayList<Fragment>();

    public ViewPagerAdapter(FragmentManager fragmentManager)
    {
        super(fragmentManager);

    }


    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }


    public void addFragment(Fragment fragment,String title)
    {

        mList.add(fragment);
        mTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position)
    {

        return mTitleList.get(position);
    }
}
