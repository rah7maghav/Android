package com.example.emp355.networking;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.adapter.ImageDetailsRecyclerAdapter;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.AllDataObject;
import com.example.emp355.model.Item;
import com.example.emp355.retrofit.GetDataService;
import com.example.emp355.retrofit.RetrofitClientInstance;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.emp355.Constant.DOWNLOAD_MESSAGE;
import static com.example.emp355.Constant.DOWNLOAD_SUCCESS_MESSAGE;
import static com.example.emp355.Constant.IMAGE_FILENAME;
import static com.example.emp355.Constant.IMAGE_POSITION;
import static com.example.emp355.Constant.IMAGE_URL;
import static com.example.emp355.Constant.SEARCHED_TEXT;

public class NetworkingAssign2Activity extends AppCompatActivity implements OnItemClickListener {


    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;

    //API_KEY used as a parameter in the BASE_URL of API
    private static final String API_KEY="AIzaSyAPiG2fwDSl9qn5EhkY48EMFU67nKzOciI";
    private LinearLayout mLinearLayout;
    private  EditText mEditText;

    //list containing all the urls of all images
    private List<Item> mItemList;
    private List<String> mFileList=new ArrayList<String>();

    //the text that is being searched
    private String mSearchedText;

    //Download message status of image
    private String mDownloadMsgStatus;

    //list containing the all the filenames of given directory in the device
    private ArrayList<String> mDeviceFileList = new ArrayList<>();

    //RecyclerView Adapter object
    private ImageDetailsRecyclerAdapter mImageDetailsAdapter;

    //BASE_URL of API to fetch data(images)
    private static final String BASE_URL="https://www.googleapis.com/customsearch/";
    private DownloadImageReceiver mDownloadImageReceiver;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_assign2);

        final ImageView closeIcon;

        IntentFilter intentFilter=new IntentFilter("downloadimagemessage");
        mDownloadImageReceiver=new DownloadImageReceiver();


        //registers the broadcast receiver(should be registered in onResume)
        //https://developer.android.com/guide/components/broadcasts#manifest-declared-receivers
        //read Context-registered receivers point no.3
        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloadImageReceiver,intentFilter);


        mRecyclerView = findViewById(R.id.recyclerview_imagedetails);
        mEditText = findViewById(R.id.edittext_searchimage);
        mLinearLayout = findViewById(R.id.linearlayout_empty);
        closeIcon = findViewById(R.id.imageview_closeicon);


        //clears the text written in edittext
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText("");
            }
        });



        File dir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "search_images");

        //mDeviceList containing list of all file names in  search_images directory
        mDeviceFileList.addAll(Arrays.asList(dir.list()));



        //sets layout manager for adapter
        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,3, LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(gridLayoutManager);


        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!mEditText.getText().toString().equals(""))
                {
                    closeIcon.setVisibility(View.VISIBLE);
                }
                else {
                    closeIcon.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Keyboard search  action
        mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {



                if (id == EditorInfo.IME_ACTION_DONE) {

                    mProgressDialog = new ProgressDialog(NetworkingAssign2Activity.this);
                    mProgressDialog.setMessage("Loading....");
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();

                    if(checkInternetConnectivity())
                    {
                        //calling the NetworkingOperation() method on search action
                        NetworkingOperation(mEditText.getText().toString());

                    }
                    return true;
                }
                return false;
            }
        });


    }




    //unregisters the broadcast receiver
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDownloadImageReceiver);
    }




    //Receiver to check whether image is downloaded or not
    public class DownloadImageReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent!=null)
            {
                mDownloadMsgStatus=intent.getStringExtra(DOWNLOAD_MESSAGE);
                String fileName=intent.getStringExtra(IMAGE_FILENAME);
                int imagePosition=intent.getIntExtra(IMAGE_POSITION,-1);

                //checks the download message status of image obtained from DownloadIntentService
                if(mDownloadMsgStatus.equals(DOWNLOAD_SUCCESS_MESSAGE))
                {
                    mFileList.remove(fileName);
                    mDeviceFileList.add(fileName);
                    mImageDetailsAdapter.notifyItemChanged(imagePosition);
                }

                Toast.makeText(NetworkingAssign2Activity.this,mDownloadMsgStatus,Toast.LENGTH_LONG).show();
                mDownloadMsgStatus=null;

            }



        }
    }


    //performs the networking operation to fetch and parse the data provided by api
    private void NetworkingOperation(final String searchedText)
    {
        //parameters used in BASE_URL Of API request
        final String cx="008733068000644023346:eqlwl3brxma";
        final String enableImageSearch="true";
        final String searchType="image";
        final String disableWebSearch="true";

        GetDataService service= RetrofitClientInstance.getRetrofitInstance(BASE_URL).create(GetDataService.class);

        Call<AllDataObject> call=service.getAllImages(API_KEY,cx,enableImageSearch,disableWebSearch,searchType,searchedText);

        call.enqueue(new Callback<AllDataObject>() {
            @Override
            public void onResponse(Call<AllDataObject> call, Response<AllDataObject> response) {
                mProgressDialog.dismiss();
                Toast.makeText(NetworkingAssign2Activity.this,"Cannot fetch data, try Again",Toast.LENGTH_SHORT).show();
                if(response.isSuccessful())
                {
                    generateDataList(response.body(),searchedText);
                }

            }

            @Override
            public void onFailure(Call<AllDataObject> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(NetworkingAssign2Activity.this,"Failure, try Again",Toast.LENGTH_SHORT).show();
            }
        });
    }



    //sends the required data to show in adapter
    private void generateDataList(AllDataObject allDataObject,String searchedText) {

        if(allDataObject!=null)
        {
            mItemList=allDataObject.getItems();
            mSearchedText=searchedText;


            if(mItemList!=null)
            {
                mLinearLayout.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

                //sets the adapter with required data
                mImageDetailsAdapter= new ImageDetailsRecyclerAdapter(this,mItemList,mSearchedText,mDeviceFileList,this);
                mRecyclerView.setAdapter(mImageDetailsAdapter);
            }
            else {
                Toast.makeText(this,"Try Again",Toast.LENGTH_SHORT).show();
            }

        }
        else {
            Toast.makeText(this,"Please try Again",Toast.LENGTH_SHORT).show();
        }

    }





    // Checks for the internet connection.
    private boolean checkInternetConnectivity()
    {
        // Get Connectivity Manager
        ConnectivityManager connectivityManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Details about the currently active default data network
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        if(networkInfo==null)
        {
            Toast.makeText(this, "No default network is currently active", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!networkInfo.isAvailable()) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!networkInfo.isConnected())
        {
            Toast.makeText(this, "Network is not connected", Toast.LENGTH_SHORT).show();
            return false;

        }

        Toast.makeText(this, "Network Connected", Toast.LENGTH_SHORT).show();
        return true;

    }



    //performs the on click operation
    @Override
    public void onItemClick(View view, int position) {

        String fileName=getFileName(mSearchedText,position);

            String imageUrl=mItemList.get(position).getImage().getThumbnailLink();
            switch (view.getId())
            {
                //performs the downloading operation of image using intent service
                case R.id.imageview_download:

                    if(mFileList.contains(fileName) && mDownloadMsgStatus==null)
                    {
                        Toast.makeText(this,"Downloading already in progress",Toast.LENGTH_SHORT).show();
                    }

                    else {


                        if(checkInternetConnectivity())
                        {
                            Intent intent=new Intent(this,DownloadIntentService.class);

                            intent.putExtra(IMAGE_URL,imageUrl);
                            intent.putExtra(SEARCHED_TEXT,mSearchedText);
                            intent.putExtra(IMAGE_POSITION,position);

                            //starting the download image service
                            startService(intent);

                            mFileList.add(fileName);

                        }

                    }

                    break;


                //opens the image in full screen by checking
                // whether image is downloaded or not
                case R.id.imageview_data:

                    if(mDeviceFileList.contains(fileName))
                    {
                        Intent intent=new Intent(this,FullScreenActivity.class);
                        intent.putExtra(IMAGE_FILENAME,fileName);
                        startActivity(intent);

                    }
                    else {
                        Toast.makeText(this,"First Download Image",Toast.LENGTH_SHORT).show();
                    }

                    break;


            }





    }


    //returns the filename to be saved for the image
    private String getFileName(String seachedText,int imagePosition) {

        return seachedText+"_"+imagePosition+".jpg";

    }


    @Override
    public void onItemClick(View view, Cursor cursor) {

    }
}
