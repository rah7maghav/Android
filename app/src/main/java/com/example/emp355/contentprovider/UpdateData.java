package com.example.emp355.contentprovider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

public class UpdateData extends AppCompatActivity {

    private EditText mFullName,mEmail,mAge,mContactNo,mGender;
    private Uri mUri;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);


        mFullName=findViewById(R.id.edit_fullname);
        mEmail=findViewById(R.id.edit_idemail);
        mAge=findViewById(R.id.edit_age);
        mContactNo=findViewById(R.id.edit_contactno);
        mGender=findViewById(R.id.edit_gender);

        if(getIntent()!=null)
        {
          id=getIntent().getIntExtra("id",-1);
        }

        Log.d("u", "onCreate: "+id);

        //returns an uri specific to a particular record
        mUri=UserEntry.buildUserUri(id);

        //fetches data according to current userid
        Cursor cursor=getContentResolver().query(mUri,null,null,null,null);

        if(cursor!=null && cursor.getCount()==1 && cursor.moveToFirst())
        {
            mFullName.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_FULLNAME)));
            mAge.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_AGE)));
            mContactNo.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_CONTACTNO)));
            mEmail.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_EMAIL)));
            mGender.setText(cursor.getString(cursor.getColumnIndex(UserEntry.COLUMN_GENDER)));
        }


    }

    //Method for updating data
    public void UpdateData(View view)
    {
        ContentValues values=new ContentValues();
        values.put(UserEntry.COLUMN_FULLNAME,mFullName.getText().toString());
        values.put(UserEntry.COLUMN_EMAIL,mEmail.getText().toString());
        values.put(UserEntry.COLUMN_AGE,mAge.getText().toString());
        values.put(UserEntry.COLUMN_CONTACTNO,mContactNo.getText().toString());
        values.put(UserEntry.COLUMN_GENDER,mGender.getText().toString());
        String where=UserEntry._ID+"=?";
        String selectionArgs[]={id+""};

        //updates the record based on current userid
        //returns the number of rows updated
        int rowsUpdated=getContentResolver().update(UserEntry.CONTENT_URI,values,where,selectionArgs);

        if(rowsUpdated>0 && rowsUpdated==1)
        {
            Toast.makeText(this,"Updated Record",Toast.LENGTH_LONG).show();
            finish();
        }
        else {
            Toast.makeText(this,"Could Not Be Updated",Toast.LENGTH_LONG).show();
        }


    }
}
