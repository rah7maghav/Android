package com.example.emp355.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.Prediction;
import com.example.emp355.model.Structured_Formatting;

import java.util.List;

public class SearchDetailsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    //NetworkingShowDetailsActivity Context
    private Context mContext;

    //Predictions List
    private List<Prediction> mPredictionList;

    //Click Listener for an item
    private OnItemClickListener onItemClickListener;


    //initializes the Prediction List and onItemClickListener and Context
    public SearchDetailsRecyclerAdapter(Context mContext, List<Prediction> mPredictionList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.mPredictionList = mPredictionList;
        this.onItemClickListener=onItemClickListener;
    }

    //inflates the layout for searched details
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.searchdetailslayout,parent,false);

        return new SearchDetailsHolder(view);
    }

    //binds the textviews to obtained data based on position
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(mPredictionList!=null)
        {
            Structured_Formatting structured_formatting=mPredictionList.get(position).getStructured_formatting();

            if(structured_formatting!=null)
            {
                ((SearchDetailsHolder) holder).mainText.setText(structured_formatting.getMain_text());
                if(!structured_formatting.getSecondary_text().equals("secondary_text"))
                {
                    ((SearchDetailsHolder) holder).secondaryText.setText(structured_formatting.getSecondary_text());
                }
                else {
                    ((SearchDetailsHolder) holder).linearLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.reddrawable));
                }

            }

        }

    }

    //returns the total items in the recycler view
    @Override
    public int getItemCount() {
        return mPredictionList.size();
    }


    //holder for searched details
    public class SearchDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView mainText,secondaryText;
        LinearLayout linearLayout;

        public SearchDetailsHolder(View itemView) {
            super(itemView);

            mainText=itemView.findViewById(R.id.textview_maintext);
            secondaryText=itemView.findViewById(R.id.textview_secondarytext);
            linearLayout=itemView.findViewById(R.id.linearlayout_searchdetails);

            linearLayout.setOnClickListener(this);
        }

        //onclick for clicking an item
        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null)
            {
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }

        }
    }
}
