package com.example.emp355.relativelayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;

public class RelativeLayoutAssign3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout_assign3);
    }

    public void sendMessage(View view)
    {
        Intent i=new Intent(this,RelativeLayoutAssign4.class);

        startActivity(i);

    }
}
