package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Term implements Parcelable {

    private int offset;
    private String value;



    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.offset);
        dest.writeString(this.value);
    }

    public Term() {
    }

    protected Term(Parcel in) {
        this.offset = in.readInt();
        this.value = in.readString();
    }

    public static final Parcelable.Creator<Term> CREATOR = new Parcelable.Creator<Term>() {
        @Override
        public Term createFromParcel(Parcel source) {
            return new Term(source);
        }

        @Override
        public Term[] newArray(int size) {
            return new Term[size];
        }
    };
}
