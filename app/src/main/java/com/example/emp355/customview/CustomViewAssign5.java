package com.example.emp355.customview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;

import com.example.emp355.R;

public class CustomViewAssign5 extends AppCompatActivity {

    private SeekBar mSeekbar;
    private int mCurrentProgress;
    private SemiCircleViewAssign5 mSemiCircleViewAssign5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view_assign5);

        mSemiCircleViewAssign5=(SemiCircleViewAssign5)findViewById(R.id.customview_speedometer);
        mSeekbar=(SeekBar)findViewById(R.id.seekbar_progress1);



        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                mCurrentProgress=mSeekbar.getProgress();

                mSemiCircleViewAssign5.UpdateSpeedometerView(mCurrentProgress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
