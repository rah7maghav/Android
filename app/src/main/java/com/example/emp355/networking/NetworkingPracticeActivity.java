package com.example.emp355.networking;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emp355.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NetworkingPracticeActivity extends AppCompatActivity {

    private TextView mDownloadedData;
    private ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_practice);


        mDownloadedData=findViewById(R.id.textview_networkdata);

    }


    // When user click on the "Download Data".
    public void downloadData(View view)
    {
        boolean flag=checkInternetConnection();
        if(flag)
        {
            String url="https://o7planning.org/download/static/default/demo-data/company.json";
            new NetworkAsyncTask().execute(url);
        }


    }


    // Checks for the internet connection.
    private boolean checkInternetConnection() {

        // Get Connectivity Manager
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        // Details about the currently active default data network
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            Toast.makeText(this, "No default network is currently active", Toast.LENGTH_LONG).show();
            return false;
        }

        if(!networkInfo.isConnected())
        {
            Toast.makeText(this, "Network is not connected", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!networkInfo.isAvailable()) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
            return false;
        }
        Toast.makeText(this, "Network OK", Toast.LENGTH_LONG).show();
        return true;

    }



        //async task to fetch data from url
        public class NetworkAsyncTask extends AsyncTask<String,Void,String>
    {


        //shows the progress dialog while fetching data from url
        @Override
        protected void onPreExecute() {
            mProgressDialog=new ProgressDialog(NetworkingPracticeActivity.this);
            mProgressDialog.setMessage("Downloading");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }


        //fetches the data from url
        @Override
        protected String doInBackground(String... values) {

            InputStream inputStream=null;
            HttpURLConnection urlConnection=null;
            Integer result=0;
            String response=null;
            try
            {
                /* forming the java.net.URL object */
                URL url=new URL(values[0]);
                urlConnection=(HttpURLConnection) url.openConnection();

                /* for Get request */
                urlConnection.setRequestMethod("GET");

                int statusCode=urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if(statusCode==200)
                {
                    inputStream=new BufferedInputStream(urlConnection.getInputStream());
                   response=convertInputStreamToString(inputStream);
                    //parseResult(response);
                    result=1;

                }
                else {
                    result=0;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            try {
                Thread.sleep(3000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //returns the required string data
            return response;
        }



        //converts the inputstream from url to string
        private String convertInputStreamToString(InputStream inputStream)
        {
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            String line="";
            String result="";
            try
            {
                while ((line=bufferedReader.readLine())!=null)
                {
                    result=result+line;
                }
                if(inputStream!=null)
                {
                    inputStream.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return result;

        }


        //dismisses the dialog and updates the textview
        @Override
        protected void onPostExecute(String value) {

            mProgressDialog.dismiss();
            if(value!=null)
            {
                //sets the obtained data from url to a textview
                mDownloadedData.setText(value);
            }
            else {
                Toast.makeText(NetworkingPracticeActivity.this,"Failed to Fetch data",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
