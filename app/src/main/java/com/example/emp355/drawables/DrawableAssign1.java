package com.example.emp355.drawables;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.actionbar.ActionBarAssign1;
import com.example.emp355.R;

public class DrawableAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawable_assign1);

    }

    public void Next(View view)
    {
        Intent i=new Intent(this, ActionBarAssign1.class);
        startActivity(i);
    }
}
