package com.example.emp355.recyclerview;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.emp355.R;
import com.example.emp355.adapter.RecyclerViewAdapter;

public class RecyclerViewAssign3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_assign3);


        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recyclerview3);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,2, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);

        RecyclerViewAdapter adapter=new RecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);

    }
}
