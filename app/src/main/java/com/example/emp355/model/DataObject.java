package com.example.emp355.model;

import java.util.ArrayList;
import java.util.List;

public class DataObject {


    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
