package com.example.emp355.retrofit;

import com.example.emp355.model.AllDataObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("v1")
    Call<AllDataObject> getAllImages(@Query("key") String key
                                    ,@Query("cx") String cx
                                    ,@Query("enableImageSearch") String enableImageSearch
                                    ,@Query("disableWebSearch") String disableWebSearch
                                    ,@Query("searchType") String searchType
                                    ,@Query("q") String input);

}
