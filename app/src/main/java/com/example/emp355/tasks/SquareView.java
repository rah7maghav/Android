package com.example.emp355.tasks;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class SquareView extends View{

    private Paint mSquarePaint;

    public SquareView(Context context) {
        this(context,null,0,0);

    }

    public SquareView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0,0);

    }

    public SquareView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
       this(context, attrs, defStyleAttr,0);

    }

    public SquareView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init()
    {
        mSquarePaint=new Paint();
        mSquarePaint.setStyle(Paint.Style.FILL);
        mSquarePaint.setColor(Color.YELLOW);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float side=getWidth()/3;
        float height=getHeight();

        float top=(height-side)/2;

        canvas.drawRect(side,top,side*2,top+side,mSquarePaint);
    }
}
