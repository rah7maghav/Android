package com.example.emp355.thirdpartylibraries;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emp355.Constant;
import com.example.emp355.R;
import com.example.emp355.adapter.ImageDataRecyclerAdapter;

import java.util.ArrayList;

public class ThirdPartyAssignFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ImageDataRecyclerAdapter mImageDataRecyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third_party_assign, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //String url list for each tab
        ArrayList<String> imageUrllist = new ArrayList<String>();
        Bundle bundle = getArguments();
        if (bundle != null) {

            //fetching the required String url list for each tab
            imageUrllist = bundle.getStringArrayList(Constant.IMAGES_URL_DATA);

        }


        mRecyclerView = view.findViewById(R.id.recyclerview_thirdparty);

        //sets layout manager for adapter
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(gridLayoutManager);


        //sets the adapter with required data
        mImageDataRecyclerAdapter = new ImageDataRecyclerAdapter(getActivity(), imageUrllist);
        mRecyclerView.setAdapter(mImageDataRecyclerAdapter);
    }
}
