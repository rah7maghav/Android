package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class OpeningHour implements Parcelable {

    private boolean open_now;
    private List<String> weekday_text;
    private List<Period> periods;


    public boolean isOpen_now() {
        return open_now;
    }

    public void setOpen_now(boolean open_now) {
        this.open_now = open_now;
    }

    public List<String> getWeekday_text() {
        return weekday_text;
    }

    public void setWeekday_text(List<String> weekday_text) {
        this.weekday_text = weekday_text;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.open_now ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.weekday_text);
        dest.writeTypedList(this.periods);
    }

    public OpeningHour() {
    }

    protected OpeningHour(Parcel in) {
        this.open_now = in.readByte() != 0;
        this.weekday_text = in.createStringArrayList();
        this.periods = in.createTypedArrayList(Period.CREATOR);
    }

    public static final Parcelable.Creator<OpeningHour> CREATOR = new Parcelable.Creator<OpeningHour>() {
        @Override
        public OpeningHour createFromParcel(Parcel source) {
            return new OpeningHour(source);
        }

        @Override
        public OpeningHour[] newArray(int size) {
            return new OpeningHour[size];
        }
    };
}
