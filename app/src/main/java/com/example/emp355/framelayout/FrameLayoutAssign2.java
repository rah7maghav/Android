package com.example.emp355.framelayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;
import com.example.emp355.assignment.TextViewAssign1;
import com.example.emp355.scroll.ScrollViewAssign1;

public class FrameLayoutAssign2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_layout_assign2);
    }


    public void sendMessage(View view)
    {
        Intent i=new Intent(this,ScrollViewAssign1.class);

        startActivity(i);

    }
}
