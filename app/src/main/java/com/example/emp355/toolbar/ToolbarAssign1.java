package com.example.emp355.toolbar;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.image.ImageViewAssign1;

public class ToolbarAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_assign1);


        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
       // toolbar.setTitle(R.string.mytool_bar);


        setSupportActionBar(toolbar);


        ActionBar actionBar=getSupportActionBar();

       // actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

    }


    public void Next(View view)
    {
        Intent i=new Intent(this, ImageViewAssign1.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
            getMenuInflater().inflate(R.menu.menu_main,menu);
            return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
          case  R.id.gps:
            Toast.makeText(this, "You clicked GPS", Toast.LENGTH_SHORT).show();
            return true;

            case R.id.refresh:
                Toast.makeText(this,"You clicked Refresh",Toast.LENGTH_LONG).show();
                return true;

            case R.id.help:
                LayoutInflater li=getLayoutInflater();
                View layout=li.inflate(R.layout.customtoast,(ViewGroup) findViewById(R.id.custom_toast));

                Toast toast=new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
                toast.setView(layout);
                toast.show();
                return true;

            case R.id.chk_upd:
                Toast.makeText(this,"You clicked Check for updates",Toast.LENGTH_LONG).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }



}
