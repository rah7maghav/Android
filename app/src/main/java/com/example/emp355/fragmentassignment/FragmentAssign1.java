package com.example.emp355.fragmentassignment;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.adapter.Fragpart1RecyclerAdapter;

import com.example.emp355.fragments.FragPart2;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.User;

import java.util.ArrayList;

public class FragmentAssign1 extends AppCompatActivity implements OnItemClickListener {
    ArrayList<User> list=new ArrayList<User>();
    Fragpart1RecyclerAdapter fragpart1RecyclerAdapter;
    RecyclerView recyclerView;
    private static final int USER_REQUEST = 1;
  //  private static final int EDIT_USER_REQUEST = 2;
  TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_assign1);

        textView=(TextView)findViewById(R.id.norecordtext);

        recyclerView=(RecyclerView) findViewById(R.id.frag1recyclerview);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        fragpart1RecyclerAdapter=new Fragpart1RecyclerAdapter(this,list,this);
        recyclerView.setAdapter(fragpart1RecyclerAdapter);


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USER_REQUEST && resultCode == RESULT_OK && data != null) {
         //   Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag1);
           //FragPart1 fragment=new FragPart1();
          //  if (fragment != null && fragment instanceof FragPart1) {



          //  int  viewposition=Integer.parseInt(data.getStringExtra("viewpos"));


        int viewposition=    data.getIntExtra("viewpos",-2);
                String username=data.getStringExtra("uname");
                String useremail=data.getStringExtra("uemail");
                String usergender=data.getStringExtra("ugender");
                String usertype=data.getStringExtra("utype");


                //FragPart1 fragPart1=new FragPart1();
        /*        Bundle bundle = new Bundle();
                bundle.putString("username",username);
                bundle.putString("useremail",useremail);
                bundle.putString("usergender",usergender);
                bundle.putString("usertype",usertype);

                fragment.setArguments(bundle);*/

                if(viewposition!=-1)
                {
                    User u=list.get(viewposition);

                    u.setName(username);
                    u.setType(usertype);
                    u.setEmail(useremail);
                    u.setGender(usergender);

                    fragpart1RecyclerAdapter.notifyItemChanged(viewposition);
                }

                else {

                    User user = new User();

                    user.setEmail(useremail);
                    user.setGender(usergender);
                    user.setName(username);
                    user.setType(usertype);

                    list.add(user);

                    fragpart1RecyclerAdapter.notifyItemInserted(list.size() - 1);
                }


            recyclerView.setVisibility(View.VISIBLE);
          //  TextView textView=(TextView)findViewById(R.id.norecordtext);
            textView.setVisibility(View.GONE);


             //    fragment.addUser(user);



/*
                FragmentManager fragmentManager=getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.part1,fragment);
                fragmentTransaction.commit();*/


          //  }
        }

    }

 /*  public  void NextFrag(String empname)
    {

                        FragPart2 fragPart2 = new FragPart2();

                        Bundle bundle = new Bundle();
                        bundle.putString("empname",empname);

                        fragPart2.setArguments(bundle);

                        FragmentManager fragmentManager=getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.part2,fragPart2);
                        fragmentTransaction.commit();

    }*/

    public void Next(View view)
    {
        Intent intent=new Intent(FragmentAssign1.this,UserDetailForm.class);
        intent.putExtra("viewposition",-1);
        startActivityForResult(intent,USER_REQUEST);
    }

/*
    @Override
    public void onItemClick(int position) {
        MyUser user = list.get(position);


        FragPart2 fragPart2 = new FragPart2();

        Bundle bundle = new Bundle();
        bundle.putString("uname",user.getName());

        fragPart2.setArguments(bundle);

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.part2,fragPart2);
        fragmentTransaction.commit();

    }*/

    @Override
    public void onItemClick(View view, int position) {

        User user = list.get(position);


       int viewid=view.getId();
       if(viewid==R.id.employee_layout || viewid==R.id.student_layout )
       {



           FragPart2 fragPart2 = new FragPart2();

           Bundle bundle = new Bundle();
           bundle.putString("uname",user.getName());

           fragPart2.setArguments(bundle);

           FragmentManager fragmentManager=getSupportFragmentManager();
           FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
           fragmentTransaction.replace(R.id.part2,fragPart2);
           fragmentTransaction.commit();
       }



       else if (viewid==R.id.deleteemployee || viewid==R.id.deletestudent)
       {
           list.remove(position);
           fragpart1RecyclerAdapter.notifyItemRemoved(position);
           if(list.size()==0)
           {
               textView.setVisibility(View.VISIBLE);
           }
       }

       else if (viewid==R.id.editemployee || viewid==R.id.editstudent)
       {

           list.get(position);

           Intent intent=new Intent(FragmentAssign1.this,UserDetailForm.class);
           intent.putExtra("user_name",user.getName());
           intent.putExtra("user_type",user.getType());
           intent.putExtra("user_email",user.getEmail());
           intent.putExtra("user_gender",user.getGender());
           intent.putExtra("viewposition",position);
           startActivityForResult(intent,USER_REQUEST);
       }



    }

    @Override
    public void onItemClick(View view, Cursor cursor) {

    }
}
