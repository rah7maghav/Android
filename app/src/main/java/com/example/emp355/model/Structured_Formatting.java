package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Structured_Formatting implements Parcelable {

    private String main_text;
    private List<Matched_Substring> main_text_matched_substrings;
    private String secondary_text;


    public String getMain_text() {
        return main_text;
    }

    public void setMain_text(String main_text) {
        this.main_text = main_text;
    }

    public List<Matched_Substring> getMain_text_matched_substrings() {
        return main_text_matched_substrings;
    }

    public void setMain_text_matched_substrings(List<Matched_Substring> main_text_matched_substrings) {
        this.main_text_matched_substrings = main_text_matched_substrings;
    }

    public String getSecondary_text() {
        return secondary_text;
    }

    public void setSecondary_text(String secondary_text) {
        this.secondary_text = secondary_text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.main_text);
        dest.writeList(this.main_text_matched_substrings);
        dest.writeString(this.secondary_text);
    }

    public Structured_Formatting() {
    }

    protected Structured_Formatting(Parcel in) {
        this.main_text = in.readString();
        this.main_text_matched_substrings = new ArrayList<Matched_Substring>();
        in.readList(this.main_text_matched_substrings, Matched_Substring.class.getClassLoader());
        this.secondary_text = in.readString();
    }

    public static final Parcelable.Creator<Structured_Formatting> CREATOR = new Parcelable.Creator<Structured_Formatting>() {
        @Override
        public Structured_Formatting createFromParcel(Parcel source) {
            return new Structured_Formatting(source);
        }

        @Override
        public Structured_Formatting[] newArray(int size) {
            return new Structured_Formatting[size];
        }
    };
}
