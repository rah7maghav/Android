package com.example.emp355.multithreading;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.emp355.R;

import java.lang.ref.WeakReference;

public class AsyncExampleActivity extends AppCompatActivity {

    private EditText mNumber;
    private TextView mShowProgress;
    private ProgressDialog mProgressDialog;
    private CustomAsyncTask mCustomAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_example);

        Log.d("tag", "onCreate: ");
        mNumber=findViewById(R.id.edittext_number);
        mShowProgress=findViewById(R.id.text_showprogress);
        if (savedInstanceState != null) {

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("tag", "onSaveInstanceState: ");

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.d("tag", "onRestoreInstanceState: ");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d("config", "ORIENTATION_LANDSCAPE");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Log.d("config", "ORIENTATION_PORTRAIT");
        }
        
    }


    public void clickMe(View view)
    {
        mCustomAsyncTask =new CustomAsyncTask(this);
        mCustomAsyncTask.execute(mNumber.getText().toString());
    }

    @Override
    protected void onDestroy() {

        //should be called before super.onDestroy()(Default functionality)
        if(mCustomAsyncTask !=null && !mCustomAsyncTask.isCancelled())
        {
            mCustomAsyncTask.cancel(true);
            mProgressDialog.dismiss();
            Log.d("cancel", "cancelled: ");
        }

        super.onDestroy();



    }

    public static class CustomAsyncTask extends AsyncTask<String,Integer,Void>
    {

        private WeakReference<AsyncExampleActivity> activityWeakReference;

        private CustomAsyncTask(AsyncExampleActivity activity) {

            activityWeakReference=new WeakReference<>(activity);
        }

        @Override
        protected void onPreExecute() {

            if(activityWeakReference.get()!=null)
            {
                activityWeakReference.get().mProgressDialog=new ProgressDialog(activityWeakReference.get());
                activityWeakReference.get().mProgressDialog.setMessage("Uploading");
                activityWeakReference.get().mProgressDialog.setCancelable(true);
                activityWeakReference.get().mProgressDialog.show();
            }

        }

        @Override
        protected Void doInBackground(String... values) {

            for (int i=1;i<=Integer.parseInt(values[0]);i++)
            {
                try {

                    Thread.sleep(1000);
                    publishProgress(i);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            if(activityWeakReference.get()!=null)
            {
                Log.d("activity status", "onPostExecute: " + activityWeakReference.get());
                activityWeakReference.get().mProgressDialog.dismiss();
                activityWeakReference.get().mNumber.setText("2");
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            if(activityWeakReference.get()!=null)
            {
                Log.d("pro", "onProgressUpdate: "+values[0]);
                activityWeakReference.get().mShowProgress.setText(String.valueOf(values[0]));
            }

        }

    }

}

