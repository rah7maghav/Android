package com.example.emp355.kotlin

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.example.emp355.R
import kotlinx.android.synthetic.main.activity_example_async.*
import java.lang.ref.WeakReference


class ExampleAsyncActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example_async)

        button_enter.setOnClickListener {

            DataLoader(this).execute(edittext_enternum.text.toString())

        }
    }



    class DataLoader(activity: ExampleAsyncActivity) :AsyncTask<String, Int,String>(){

        var weakReference:WeakReference<ExampleAsyncActivity>?=null

        init {

            weakReference= WeakReference(activity)
        }

        override fun onPreExecute() {

        }


        override fun doInBackground(vararg params: String): String {

            for (i in 0..params[0].toInt())
            {
                try {
                    Thread.sleep(1000)
                   publishProgress(i)
                }
                catch (e:InterruptedException){
                    e.printStackTrace()
                }
            }

            return params[0]

        }

        override fun onProgressUpdate(vararg values: Int?) {

                weakReference!!.get()!!.textview_result.text = values[0].toString()
        }

        override fun onPostExecute(result: String) {

                 weakReference!!.get()!!.textview_result.text = result
        }

    }


}

