package com.example.emp355.uipractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.emp355.R;

public class ChildViewPracticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_view_practice);

        Toolbar toolbar;


        toolbar=findViewById(R.id.toolbar_childview);

        toolbar.setNavigationIcon(R.drawable.childviewmenu);
        toolbar.inflateMenu(R.menu.menu_toolbar_childview);

    }
}
