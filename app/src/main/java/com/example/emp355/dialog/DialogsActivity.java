package com.example.emp355.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.emp355.R;

import java.util.Calendar;

import static com.example.emp355.Constant.CANCEL;
import static com.example.emp355.Constant.CLICK_CANCEL;
import static com.example.emp355.Constant.CLICK_NO;
import static com.example.emp355.Constant.NO;
import static com.example.emp355.Constant.YES;

import static android.content.DialogInterface.*;

public class DialogsActivity extends AppCompatActivity implements DialogInterface.OnClickListener,View.OnClickListener{

    Button dateselector;
    Button timeselector;
    Button customdialogbutton;
    Button popupbutton;

    Dialog dialog;


    LinearLayout dialogslayout;

    PopupWindow popupWindow;

    EditText showdate;
    EditText showtime;

    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogs);


        // finding views
        dialogslayout = (LinearLayout) findViewById(R.id.dialogs_layout);

        dateselector=(Button)findViewById(R.id.selectdate);
        timeselector=(Button)findViewById(R.id.selecttime);
        customdialogbutton=(Button)findViewById(R.id.customdialog_btn);
        popupbutton=(Button)findViewById(R.id.popup_btn);


        showdate=(EditText)findViewById(R.id.showdate);
        showtime=(EditText)findViewById(R.id.showtime);


        // on click listener
        dateselector.setOnClickListener(this);
        timeselector.setOnClickListener(this);
        customdialogbutton.setOnClickListener(this);
        popupbutton.setOnClickListener(this);




    }


    public void exit(View view)
    {

        AlertDialog.Builder alertdialogbuilder=new AlertDialog.Builder(this);
        alertdialogbuilder.setTitle(R.string.confirm_exit);//set title

        alertdialogbuilder.setIcon(R.drawable.navigationhelpicon);//set icon

        alertdialogbuilder.setMessage(R.string.msg_exit);//set message

        alertdialogbuilder.setCancelable(false);//dialog does not close on clicking outside



        alertdialogbuilder.setPositiveButton(YES, this);//positive button on click

        alertdialogbuilder.setNegativeButton(NO, this);//negative button onclick

        alertdialogbuilder.setNeutralButton(CANCEL, this);//neutral button onclick



        AlertDialog alertDialog=alertdialogbuilder.create();//creates a dialog
        alertDialog.show();//shows the dialog
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which)
        {
            case BUTTON_POSITIVE://on clicking positive button
                finish();
                break;


            case BUTTON_NEGATIVE://on clicking negative button
                Toast.makeText(DialogsActivity.this,CLICK_NO,Toast.LENGTH_SHORT).show();
                break;

            case BUTTON_NEUTRAL://on clicking neutral button
                Toast.makeText(DialogsActivity.this,CLICK_CANCEL,Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onClick(View v) {


        Calendar calendar=Calendar.getInstance();//to get current time and date
       mYear= calendar.get(Calendar.YEAR);
        mMonth=calendar.get(Calendar.MONTH);
        mDay=calendar.get(Calendar.DATE);
        mHour=calendar.get(Calendar.HOUR_OF_DAY);
        mMinute=calendar.get(Calendar.MINUTE);




        switch (v.getId())
        {

            case R.id.selectdate:
                DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                        showdate.setText(dayOfMonth+"-"+(month+1)+"-"+year);



                    }
                },mYear,mMonth,mDay);
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());//maxdate for the calendar
                datePickerDialog.setCancelable(false);//dialog does not close on clicking outside
                datePickerDialog.show();// to show the dialog
                break;



            case R.id.selecttime:

                TimePickerDialog timePickerDialog=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                      showtime.setText(hourOfDay+":"+minute);

                    }
                },mHour,mMinute,false);
                timePickerDialog.setCancelable(false);//dialog does not close on clicking outside
                timePickerDialog.show();// to show the dialog
                break;



            case R.id.customdialog_btn:

                dialog=new Dialog(this);


                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);// used to remove the title of dialog
                dialog.setContentView(R.layout.customdialoglayout);

                Button dismissbtn=(Button)dialog.findViewById(R.id.dismiss_btn);
                dialog.setCancelable(false);//dialog does not close on clicking outside
                dismissbtn.setOnClickListener(this);

                dialog.show();// shows the dialog
                break;


            case R.id.dismiss_btn:

                dialog.dismiss();//closes the custom dialog
                break;




            case R.id.popup_btn:

                LayoutInflater layoutInflater=(LayoutInflater)DialogsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popuplayout=layoutInflater.inflate(R.layout.popuplayout,null);//inflating popup layout

                Button popupdismissbtn=(Button)popuplayout.findViewById(R.id.popup_close);

                popupWindow=new PopupWindow(popuplayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());// popup closes on clicked outside
                popupWindow.setOutsideTouchable(true);// popup closes on clicked outside
                popupWindow.showAtLocation(dialogslayout, Gravity.CENTER,0,0);//setting the position of popup

                popupdismissbtn.setOnClickListener(this);//onclicking dismiss button in popup

                break;



            case R.id.popup_close:

                popupWindow.dismiss();// closes th popup

                break;
        }

    }
}
