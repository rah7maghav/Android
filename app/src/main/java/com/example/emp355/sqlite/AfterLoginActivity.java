package com.example.emp355.sqlite;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.sharedpref.MySharedPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;
import static com.example.emp355.Constant.BACK_AGAIN;
import static com.example.emp355.Constant.LIKES_NUMBER;
import static com.example.emp355.Constant.NO;
import static com.example.emp355.Constant.YES;

//Activity after User Log in
public class AfterLoginActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,DialogInterface.OnClickListener, Toolbar.OnMenuItemClickListener{

    private DrawerLayout mDrawer;
    private MySharedPreferences mSharedPreferences;
    private Menu menu;
    private SqliteHelper mSqliteHelper;
    private Toolbar mToolbar;
    private ArrayList<MyUser> list=new ArrayList<MyUser>();
    private long id;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);

        NavigationView mNavView;


        mDrawer=findViewById(R.id.drawer_afterlogin);
        mNavView=findViewById(R.id.navigation_afterlogin);
        mToolbar=findViewById(R.id.toolbar_dashboard);

        mToolbar.setNavigationIcon(R.drawable.databasemenuicon);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);

            }
        });
        mToolbar.inflateMenu(R.menu.toolbarmenu);
        mToolbar.setOnMenuItemClickListener(this);

        mSharedPreferences=MySharedPreferences.getInstance(this);
        id= mSharedPreferences.fetchId();
        mSqliteHelper=SqliteHelper.getInstance(this);

        new NewAsyncTask().execute();



        mNavView.setNavigationItemSelectedListener(this);
        Log.d("Acrea", "ActCreate: called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Ade", "ActPause: called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Ast", "ActStop: called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Ades", "ActDestroy: called");
    }

    //Navigation View Item Click
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment=null;

        switch (item.getItemId())
        {

            case R.id.item_memberdetail:
                fragment=new AllMembersFragment();

                break;

            case R.id.item_likes:
                fragment=new LikesFragment();
                ((LikesFragment) fragment).getAllLikedByData(list);

                break;

            case R.id.item_logout:
                AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(AfterLoginActivity.this);
                alertDialogBuilder.setTitle(R.string.log_out);
                alertDialogBuilder.setMessage(R.string.confirm_logout);
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton(YES, this);
                alertDialogBuilder.setNegativeButton(NO,this);

                AlertDialog alertDialog=alertDialogBuilder.create();
                alertDialog.show();


                return true;

        }

        if(fragment!=null)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_replace,fragment).commit();
            mDrawer.closeDrawers();
            return true;
        }


        return false;
    }

    //AlertDialog OnClick
    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which)
        {
            case BUTTON_POSITIVE:
                mSharedPreferences.clearAllData();
                startActivity(new Intent(AfterLoginActivity.this,EnterEmailActivity.class));
                finishAffinity();
                break;

            case BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }

    }


    //Opens Dashboard Fragment
    public void OpenDashboardFragment()
    {

        DashboardFragment dashboardFragment=new DashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(LIKES_NUMBER,list.size());

        dashboardFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_replace,dashboardFragment).commit();
    }

    //Changes toolbar icons and title according to current fragment
    public void changeToolbarIcons()
    {
        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.framelayout_replace);

        if(fragment!=null && fragment instanceof DashboardFragment)
        {
            mToolbar.setTitle(R.string.my_profile);
            mToolbar.getMenu().findItem(R.id.item_calendar).setVisible(false);
            mToolbar.getMenu().findItem(R.id.item_edit).setVisible(true);
        }
        else if(fragment!=null && fragment instanceof EditProfileFragment)
        {
            mToolbar.setTitle(R.string.edit_profile);
            mToolbar.getMenu().findItem(R.id.item_edit).setVisible(false);
            mToolbar.getMenu().findItem(R.id.item_calendar).setVisible(true);
        }
        else if(fragment!=null && fragment instanceof AllMembersFragment)
        {
            mToolbar.setTitle(R.string.all_members);
            mToolbar.getMenu().findItem(R.id.item_calendar).setVisible(false);
            mToolbar.getMenu().findItem(R.id.item_edit).setVisible(false);
        }
        else if(fragment!=null && fragment instanceof LikesFragment)
        {
            mToolbar.setTitle(R.string.likes);
            mToolbar.getMenu().findItem(R.id.item_calendar).setVisible(false);
            mToolbar.getMenu().findItem(R.id.item_edit).setVisible(false);

        }
    }

    //Opens Likes Fragment
    public void OpenLikesFragment()
    {
        Fragment fragment;
        fragment=new LikesFragment();
        ((LikesFragment) fragment).getAllLikedByData(list);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.framelayout_replace,fragment).commit();

    }

    //Toolbar Item Click
    @Override
    public boolean onMenuItemClick(MenuItem item) {

        Calendar calendar=Calendar.getInstance();//to get current time and date

        int currentYear= calendar.get(Calendar.YEAR);
        int currentMonth=calendar.get(Calendar.MONTH);
        int currentDay=calendar.get(Calendar.DATE);


        switch (item.getItemId())
        {

            case R.id.item_edit:


                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_replace,new EditProfileFragment()).commit();

                return true;

            case R.id.item_calendar:


                DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.framelayout_replace);

                        if(fragment!=null && fragment instanceof EditProfileFragment)
                        {
                            ((EditProfileFragment) fragment).SetDobData(year,month,dayOfMonth);
                        }
                    }
                },currentYear,currentMonth,currentDay);

                calendar.set(currentYear-3,currentMonth,currentDay);

                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.setCancelable(false);
                datePickerDialog.show();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    //AsyncTask class for fetching number of likes of current logged in user from database
    public class NewAsyncTask extends AsyncTask<Void,Void,Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AfterLoginActivity.this.OpenDashboardFragment();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            ArrayList<MyUser> list = mSqliteHelper.getLikedByUsers(id);
            AfterLoginActivity.this.list.addAll(list);
            return null;
        }
    }

    //Handling BackPress after login
    @Override
    public void onBackPressed() {


        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.framelayout_replace);


        if(mDrawer.isDrawerOpen(GravityCompat.START))
        {
            mDrawer.closeDrawers();
        }

        else if(fragment!=null && fragment instanceof DashboardFragment)
        {

            if(doubleBackToExitPressedOnce)
            {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce=true;
            Toast.makeText(this, BACK_AGAIN, Toast.LENGTH_SHORT).show();


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            },3000);

        }

        else
        {

            this.OpenDashboardFragment();
        }
    }



}
