package com.example.emp355.fragmentassignment;


import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.emp355.Constant;
import com.example.emp355.R;


public class UserDetailForm extends AppCompatActivity implements Constant {

     String usertype="" ;
    String usergender="" ;
    String username="";
    String useremail="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail_form);


        Intent intent = getIntent();
        if (intent != null) {
         //   final int viewposition = Integer.parseInt(intent.getStringExtra("viewposition"));

            final int viewposition=intent.getIntExtra("viewposition",-2);

            RadioGroup usertypeid = (RadioGroup) findViewById(R.id.usertypeid);
            final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.studentportion);

            final RadioGroup usergenderid = (RadioGroup) findViewById(R.id.usergenderid);

            final EditText editText1 = (EditText) findViewById(R.id.username);
            final EditText editText2 = (EditText) findViewById(R.id.useremail);




            if(viewposition!=-1)
            {
               username= intent.getStringExtra("user_name");
                usertype=intent.getStringExtra("user_type");
                useremail=intent.getStringExtra("user_email");
                usergender=intent.getStringExtra("user_gender");


                editText1.setText(username);

                if(usertype.equals("Student"))
                {

                    usertypeid.check(R.id.student);
                    editText2.setText(useremail);
                    linearLayout.setVisibility(View.VISIBLE);

                    if(usergender.equals("Male"))
                    {
                        usergenderid.check(R.id.male);
                    }
                    else if(usergender.equals("Female"))
                    {
                        usergenderid.check(R.id.female);
                    }
                }
                else if(usertype.equals("Employee"))
                {
                    usertypeid.check(R.id.employee);
                }






            }



                usertypeid.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {

                        if (checkedId == R.id.student) {
                            linearLayout.setVisibility(View.VISIBLE);
                            RadioButton radioButton = (RadioButton) findViewById(R.id.student);
                            usertype = radioButton.getText().toString();
                        } else if (checkedId == R.id.employee) {
                            linearLayout.setVisibility(View.INVISIBLE);
                            RadioButton radioButton = (RadioButton) findViewById(R.id.employee);
                            usertype = radioButton.getText().toString();
                        }
                    }
                });





            usergenderid.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (checkedId == R.id.male) {

                        RadioButton radioButton = (RadioButton) findViewById(R.id.male);
                        usergender = radioButton.getText().toString();
                    } else if (checkedId == R.id.female) {

                        RadioButton radioButton = (RadioButton) findViewById(R.id.female);
                        usergender = radioButton.getText().toString();
                    }
                }
            });






            Button button = (Button) findViewById(R.id.savebtn);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    username = editText1.getText().toString();
                    useremail = editText2.getText().toString();


                    Intent intent = new Intent(UserDetailForm.this, FragmentAssign1.class);

                    intent.putExtra("uname", username);
                    intent.putExtra("uemail", useremail);
                    intent.putExtra("utype", usertype);
                    intent.putExtra("ugender", usergender);
                    intent.putExtra("viewpos", viewposition);
                    setResult(RESULT_OK, intent);
                    finish();


                }
            });

        }

    }
}
