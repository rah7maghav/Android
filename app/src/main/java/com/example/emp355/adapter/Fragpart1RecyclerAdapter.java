package com.example.emp355.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.example.emp355.R;
import com.example.emp355.fragmentassignment.FragmentAssign1;
import com.example.emp355.fragments.FragPart2;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.User;

import java.util.ArrayList;

public class Fragpart1RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private ArrayList<User> list;
    private OnItemClickListener onItemClickListener;

    public Fragpart1RecyclerAdapter(Context context, ArrayList<User> arrayList,
                                    OnItemClickListener clickListener)
    {
        mContext=context;
        list=arrayList;
        this.onItemClickListener = clickListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;


        if(viewType==1)
        {
            view= LayoutInflater.from(mContext).inflate(R.layout.studentlayout,parent,false);

            return new StudentHolder(view);

        }
        else if (viewType==2)
        {
            view=LayoutInflater.from(mContext).inflate(R.layout.employeelayout,parent,false);
            return new EmployeeHolder(view);

        }


        return null;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        User user=list.get(position);
        if(user!=null)
        {
            if(user.getType().equals("Student"))
            {

                ((StudentHolder) holder).sname.setText(user.getName());
                ((StudentHolder) holder).semail.setText(user.getEmail());
                ((StudentHolder) holder).sgender.setText(user.getGender());


            }
            else if(user.getType().equals("Employee"))
            {
                final String empname=user.getName();

                ((EmployeeHolder) holder).ename.setText(user.getName());

            }
        }





    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public int getItemViewType(int position) {

        String TYPE=list.get(position).getType();

        if(TYPE.equals("Student"))
        {
            return 1;//1 for student

        }
        else
        {
            return 2;// 2 for employee
        }


    }



    public class StudentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView sname;
        TextView semail;
        TextView sgender;
        LinearLayout studframelayout;
        ImageButton studdelete;
        ImageButton studedit;


        public StudentHolder(View itemView)
        {
            super(itemView);
            sname=(TextView)itemView.findViewById(R.id.studentname);
            semail=(TextView)itemView.findViewById(R.id.studentemail);
            sgender=(TextView)itemView.findViewById(R.id.studentgender);

            studframelayout=(LinearLayout) itemView.findViewById(R.id.student_layout);
            studframelayout.setOnClickListener(this);

            studdelete=(ImageButton)itemView.findViewById(R.id.deletestudent);
            studdelete.setOnClickListener(this);

            studedit=(ImageButton)itemView.findViewById(R.id.editstudent);
            studedit.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }
        }
    }





    public class EmployeeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView ename;
        FrameLayout empframelayout;
        ImageButton empdelete;
        ImageButton empedit;


        public EmployeeHolder(View itemView)
        {
            super(itemView);
            ename=(TextView)itemView.findViewById(R.id.employeename);

            empframelayout=(FrameLayout)itemView.findViewById(R.id.employee_layout);
            empframelayout.setOnClickListener(this);

            empdelete=(ImageButton)itemView.findViewById(R.id.deleteemployee);
            empdelete.setOnClickListener(this);




           /* empedit=(ImageButton)itemView.findViewById(R.id.editemployee);*/
            itemView.findViewById(R.id.editemployee).setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null)
            { onItemClickListener.onItemClick(v,getAdapterPosition());
            }
        }
    }
}
