package com.example.emp355.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import android.view.View;

import com.example.emp355.R;

public class SemiCircleViewAssign5 extends View{


    private Paint mLinePaint;
    private Paint mArcPaint;
    private Paint mArcPaint2;
    private Paint mArcPaint3;
    private Paint mTextPaint;

    private float mAngle=0;

    private String mShowProgress="0";
    private float mLineStartX;
    private float mLineStartY;
    private float mLineStopX=0;
    private float mLineStopY;

    public SemiCircleViewAssign5(Context context) {
        this(context,null,0,0);
    }

    public SemiCircleViewAssign5(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0,0);
    }

    public SemiCircleViewAssign5(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public SemiCircleViewAssign5(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();

    }



    private void init()
    {

        mLinePaint=new Paint();

        mLinePaint.setStyle(Paint.Style.FILL);
        mLinePaint.setStrokeWidth(8);
        mLinePaint.setColor(getResources().getColor(R.color.customviewlinecolor));


        mArcPaint=new Paint();
        mArcPaint.setStyle(Paint.Style.FILL);
        mArcPaint.setColor(getResources().getColor(R.color.customviewsemicirclelarge));


        mArcPaint2=new Paint();
        mArcPaint2.setStyle(Paint.Style.STROKE);
        mArcPaint2.setStrokeWidth(8);
        mArcPaint2.setColor(Color.BLACK);

        mArcPaint3=new Paint();
        mArcPaint3.setStyle(Paint.Style.FILL);
        mArcPaint3.setColor(getResources().getColor(R.color.customviewsemicirclesmall));


        mTextPaint=new Paint();
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(50);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setColor(Color.WHITE);


    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mLineStartX=getWidth()/2;
        mLineStartY=getWidth()/2;

        if(mAngle==0)
        {
            mLineStopY=getWidth()/2;
        }

        canvas.drawArc(0,0,getWidth(),getWidth(),180,180,true,mArcPaint);

        canvas.drawArc(0,0,getWidth(),getWidth(),180,180,true,mArcPaint2);
        canvas.drawLine(mLineStartX,mLineStartY,mLineStopX,mLineStopY,mLinePaint);
        canvas.drawArc((getWidth()/2)-50,(getWidth()/2)-50,(getWidth()/2)+50,(getWidth()/2)+50,180,180,true,mArcPaint3);

        canvas.drawText(mShowProgress,(getWidth()/2),(getWidth()/2)-100,mTextPaint);

    }


    public void UpdateSpeedometerView(int mProgress)
    {
        mShowProgress=String.valueOf(mProgress);

        float radius=getWidth()/2;

        float angleradians=(float)Math.toRadians(180);

        mAngle=mProgress*(angleradians/220);

        mLineStopX=(float)(radius-(radius*Math.cos(mAngle)));
        mLineStopY=(float)(radius-(radius*Math.sin(mAngle)));

        if(mProgress>=176)
        {
            mLinePaint.setColor(Color.RED);
        }
        else {
            mLinePaint.setColor(getResources().getColor(R.color.customviewlinecolor));
        }

        invalidate();
    }






}
