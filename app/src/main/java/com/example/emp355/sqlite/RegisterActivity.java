package com.example.emp355.sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import com.example.emp355.sharedpref.MySharedPreferences;

import static com.example.emp355.Constant.EMAIL_ENTERED;

public class RegisterActivity extends AppCompatActivity {


    private EditText mFirstName, mLastName, mPhoneNumber, mEmail, mPassword, mConfirmPassword;
    private SqliteHelper mSqliteHelper;
    private  MySharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button signUp;
        String email;

        signUp = findViewById(R.id.button_sign_up);

        mFirstName = findViewById(R.id.edittext_fname);
        mLastName = findViewById(R.id.edittext_lname);
        mPhoneNumber = findViewById(R.id.edittext_phonenumber);
        mEmail = findViewById(R.id.edittext_emailid);
        mPassword = findViewById(R.id.edittext_pass);
        mConfirmPassword = findViewById(R.id.edittext_cpass);


        if(getIntent()!=null)
        {
            email=getIntent().getStringExtra(EMAIL_ENTERED);
            mEmail.setText(email);
        }
        mSqliteHelper=SqliteHelper.getInstance(this);
        mSharedPreferences=MySharedPreferences.getInstance(this);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean temp=validateData(mFirstName.getText().toString(),mLastName.getText().toString(),
                        mPhoneNumber.getText().toString(),
                        mPassword.getText().toString(),mConfirmPassword.getText().toString());
                if (temp)
                {

                   /* MyUser user=new MyUser();
                    user.setFirstname(mFirstName.getText().toString());
                    user.setLastname(mLastName.getText().toString());
                    user.setPhonenumber(mPhoneNumber.getText().toString());
                    user.setEmail(mEmail.getText().toString());
                    user.setPassword(mPassword.getText().toString());*/

                    long id=mSqliteHelper.insert(mFirstName.getText().toString(),mLastName.getText().toString(),mPhoneNumber.getText().toString(),mEmail.getText().toString(),mPassword.getText().toString());
                    if(id==-1)
                    {
                        Toast.makeText(RegisterActivity.this,"Not able to Register",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(RegisterActivity.this,"Successfully Registered",Toast.LENGTH_LONG).show();
                        mSharedPreferences.saveId(id);

                        startActivity(new Intent(RegisterActivity.this,AfterLoginActivity.class));
                        finish();

                    }
                    /*int id= mSqliteHelper.validateUser(mEmail.getText().toString(),mPassword.getText().toString());*/

                }


            }
        });
    }

    //validation for data entered by user while register
    private boolean validateData(String firstName, String lastName,String phoneNumber,String password,String confirmPassword) {

        if (TextUtils.isEmpty(firstName)) {
            mFirstName.setError("FirstName Cannot be Empty");
            return false;
        }
        if (TextUtils.isEmpty(lastName)) {
            mLastName.setError("LastName Cannot be Empty");
            return false;
        }

        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumber.setError("PhoneNumber Cannot be Empty");
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Password Cannot be Empty");
            return false;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;
    }


}
