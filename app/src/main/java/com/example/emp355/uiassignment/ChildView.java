package com.example.emp355.uiassignment;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.emp355.R;
import com.example.emp355.toolbar.ToolbarAssign1;

public class ChildView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_view);


        android.support.v7.widget.Toolbar toolbar=(android.support.v7.widget.Toolbar) findViewById(R.id.childviewtoolbar);

        setSupportActionBar(toolbar);


        ActionBar actionBar=getSupportActionBar();

        // actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        SpannableString newchildtext=new SpannableString(getString(R.string.childviewnewchild));

         newchildtext.setSpan(new AbsoluteSizeSpan(getResources().getDimensionPixelSize(R.dimen.textsize_22sp)),0,9, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        TextView textView=(TextView)findViewById(R.id.childviewtext1);
        textView.setText(newchildtext);



        SpannableString allowancetext=new SpannableString(getString(R.string.childviewallowance));
        allowancetext.setSpan(new AbsoluteSizeSpan(getResources().getDimensionPixelSize(R.dimen.textsize_20sp)),0,2,Spanned.SPAN_INCLUSIVE_INCLUSIVE);



        TextView textView1=(TextView)findViewById(R.id.childviewtext3);
        textView1.setText(allowancetext);


        SpannableString timelefttext=new SpannableString(getString(R.string.childviewtime));
        timelefttext.setSpan(new AbsoluteSizeSpan(getResources().getDimensionPixelSize(R.dimen.textsize_20sp)),0,2,Spanned.SPAN_INCLUSIVE_INCLUSIVE);



        TextView textView2=(TextView)findViewById(R.id.childviewtext4);
        textView2.setText(timelefttext);




    }

    public void Next(View view)
    {
        Intent i=new Intent(this,Flight.class);
        startActivity(i);
    }
}
