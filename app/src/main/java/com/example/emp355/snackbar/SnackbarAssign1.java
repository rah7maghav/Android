package com.example.emp355.snackbar;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.toolbar.ToolbarAssign1;

public class SnackbarAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbar_assign1);


     final   LinearLayout linearLayout = (LinearLayout) findViewById(R.id.Linear_Layout);

        Button btn1=findViewById(R.id.btn_1);
        Button btn2=findViewById(R.id.btn_2);
        Button btn3=findViewById(R.id.btn_3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          Snackbar snackbar=Snackbar.make(linearLayout,"Simple snackbar",Snackbar.LENGTH_LONG);
            snackbar.show();
            }
        });





        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar=Snackbar.make(linearLayout,"Action Snackbar",Snackbar.LENGTH_LONG).setAction("New", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Snackbar snackbar1=Snackbar.make(linearLayout,"YES",Snackbar.LENGTH_LONG);
                        snackbar1.show();
                    }
                });
                snackbar.show();
            }
        });








        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar=Snackbar.make(linearLayout,"Customized Snackbar",Snackbar.LENGTH_LONG).setAction("NO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Snackbar snackbar1=Snackbar.make(linearLayout,"YES",Snackbar.LENGTH_LONG).setAction("ACTION", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });

                        snackbar1.setActionTextColor(Color.BLUE);
                        View v1=snackbar1.getView();
                        TextView textView = (TextView)   v1.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.GREEN);
                        snackbar1.show();
                    }

                });
                snackbar.setActionTextColor(Color.GREEN);
                View v1=snackbar.getView();
                TextView textView = (TextView)   v1.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
    }


    public void Next(View view)
    {
        Intent i=new Intent(this, ToolbarAssign1.class);
        startActivity(i);
    }
}
