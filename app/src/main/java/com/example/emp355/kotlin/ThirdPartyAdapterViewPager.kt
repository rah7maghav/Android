package com.example.emp355.kotlin

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.example.emp355.Constant


class ThirdPartyAdapterViewPager(fm:FragmentManager,dataObjectKotlin: DataObjectKotlin) : FragmentPagerAdapter(fm) {

    private var mImageList:MutableList<ImageKotlin> ?= null

    private var mTitleList:MutableList<String> =ArrayList()

    init {

        mImageList=dataObjectKotlin.images

        for (i in 0 until mImageList!!.size)
        {
            mTitleList.add(mImageList!!.get(i).name!!)
        }
    }


    override fun getItem(position: Int): Fragment {

        var imageUrlList:ArrayList<String> = ArrayList()

        when(position)
        {

            0-> imageUrlList=getList(position)

            1-> imageUrlList=getList(position)

            2-> imageUrlList=getList(position)

            3-> imageUrlList=getList(position)

            4-> imageUrlList=getList(position)
        }

        val bundle= Bundle()

        bundle.putStringArrayList(Constant.IMAGES_URL_DATA,imageUrlList)

        val thirdPartyAssignFragment= AssignThirdPartyFragment()
        thirdPartyAssignFragment.arguments=bundle

        return thirdPartyAssignFragment
    }


    fun getList(pos: Int):ArrayList<String>
    {
        return mImageList?.get(pos)?.values as ArrayList<String>
    }

    override fun getCount(): Int {

        return mTitleList.size
    }

    override fun getPageTitle(position: Int): CharSequence {

        return mTitleList.get(position)
    }
}