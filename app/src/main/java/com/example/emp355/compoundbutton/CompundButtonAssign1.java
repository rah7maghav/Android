package com.example.emp355.compoundbutton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.emp355.R;

import com.example.emp355.spinner.SpinnerSeekbarAssign1;

public class CompundButtonAssign1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compound_button_assign1);
    }

    public void Next(View view)
    {
        Intent i=new Intent(this, SpinnerSeekbarAssign1.class);
        startActivity(i);
    }
}
