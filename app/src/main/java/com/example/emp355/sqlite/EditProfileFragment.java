package com.example.emp355.sqlite;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emp355.R;
import com.example.emp355.sharedpref.MySharedPreferences;
//USer Edit Profile Fragment
public class EditProfileFragment extends Fragment {
    private SqliteHelper mSqliteHelper;
    private EditText firstName,lastName,email,phoneNumber;
    private TextView dob;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AfterLoginActivity) getActivity()).changeToolbarIcons();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MySharedPreferences mSharedPreferences;


        Button update;

        mSharedPreferences=MySharedPreferences.getInstance(getActivity());
        mSqliteHelper=SqliteHelper.getInstance(getActivity());

        firstName=view.findViewById(R.id.edittext_ufirstname);
        lastName=view.findViewById(R.id.edittext_ulastname);
        email=view.findViewById(R.id.edittext_uemail);
        phoneNumber=view.findViewById(R.id.edittext_uphonenumber);
        update=view.findViewById(R.id.button_update);
        dob=view.findViewById(R.id.textview_udob);

        final long id= mSharedPreferences.fetchId();
        MyUser myUser=mSqliteHelper.getUser(id);

        if(myUser!=null)
        {
            firstName.setText(myUser.getFirstname());
            lastName.setText(myUser.getLastname());
            email.setText(myUser.getEmail());
            phoneNumber.setText(myUser.getPhonenumber());
            dob.setText(myUser.getDob());
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int rows=mSqliteHelper.updateUser(id,firstName.getText().toString(),lastName.getText().toString(),phoneNumber.getText().toString(),dob.getText().toString());
                if(rows==1)
                {

                    Toast.makeText(getActivity(),"Successfully Updated",Toast.LENGTH_LONG).show();
                    ((AfterLoginActivity) getActivity()).OpenDashboardFragment();
                }
                else {
                    Toast.makeText(getActivity(),"Not able to Update",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    //sets dob for user
    public void SetDobData(int year,int month,int dayOfMonth)
    {

        dob.setText(dayOfMonth+"/"+(month+1)+"/"+year);
    }
}
