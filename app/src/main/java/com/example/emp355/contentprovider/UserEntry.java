package com.example.emp355.contentprovider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 *  class for  table that handles all information regarding the table schema and
 * the URIs related to it.
 */
public class UserEntry implements BaseColumns{
    // Content URI represents the base location for the table
    public static final Uri CONTENT_URI =MyContractClass.BASE_CONTENT_URI.buildUpon().appendPath(MyContractClass.PATH_USER).build();

    // These are special type prefixes that specify if a URI returns a list or a specific item
    public static final String CONTENT_TYPE ="vnd.android.cursor.dir/"+CONTENT_URI+"/"+MyContractClass.PATH_USER;
    public static final String CONTENT_ITEM_TYPE ="vnd.android.cursor.item/"+CONTENT_URI+"/"+MyContractClass.PATH_USER;

    // Define the table schema
    public static final String TABLE_NAME = "userInfo";
    public static final String COLUMN_FULLNAME = "fullName";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_CONTACTNO = "contactno";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_EMAIL = "email";

    //Table create query
    public static final String CREATE_TABLE_QUERY="CREATE TABLE "
            +TABLE_NAME+" ( "+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COLUMN_FULLNAME+" VARCHAR(255),"
            + COLUMN_AGE+" VARCHAR(255),"
            + COLUMN_EMAIL+" VARCHAR(255),"
            + COLUMN_CONTACTNO+ " VARCHAR(255),"
            + COLUMN_GENDER+ " VARCHAR(255));";

    // Define a function to build a URI to find a specific movie by it's identifier
    public static Uri buildUserUri(long id)
    {
       return ContentUris.withAppendedId(CONTENT_URI,id);
    }

}
