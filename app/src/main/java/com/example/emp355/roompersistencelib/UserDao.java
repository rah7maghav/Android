package com.example.emp355.roompersistencelib;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.List;

//Marks the class as a Data Access Object.
//Data Access Objects are the main classes where you define your database interactions. They can
// * include a variety of query methods.
@Dao
public interface UserDao {

    //inserts user data
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(NewUser newUser);

    //updates particular user data based on primary key
    @Update
    public int update(NewUser newUser );

    //deletes particular user data based on primary key
    @Delete
    public int delete( NewUser newUser);

    //fetches all users data
    @Query("Select * from NewUser")
    public List<NewUser> getAllUsers();

    //fetches user data based on userid
    @Query("Select * from NewUser where id = :userid")
    public  NewUser getUserById(int userid);
}
