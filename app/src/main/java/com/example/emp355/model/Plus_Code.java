package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Plus_Code implements Parcelable {


    private String compound_code;
    private String global_code;


    public String getCompound_code() {
        return compound_code;
    }

    public void setCompound_code(String compound_code) {
        this.compound_code = compound_code;
    }

    public String getGlobal_code() {
        return global_code;
    }

    public void setGlobal_code(String global_code) {
        this.global_code = global_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.compound_code);
        dest.writeString(this.global_code);
    }

    public Plus_Code() {
    }

    protected Plus_Code(Parcel in) {
        this.compound_code = in.readString();
        this.global_code = in.readString();
    }

    public static final Parcelable.Creator<Plus_Code> CREATOR = new Parcelable.Creator<Plus_Code>() {
        @Override
        public Plus_Code createFromParcel(Parcel source) {
            return new Plus_Code(source);
        }

        @Override
        public Plus_Code[] newArray(int size) {
            return new Plus_Code[size];
        }
    };
}
