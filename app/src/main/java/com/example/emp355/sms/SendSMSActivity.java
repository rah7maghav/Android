package com.example.emp355.sms;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import java.util.ArrayList;

public class SendSMSActivity extends AppCompatActivity {

    private static final int REQUEST_TAKE_PHOTO_GALLERY = 1;
    private EditText mEditTextNumber,mEditTextMessage;
    private Uri mSelectedImage;
    private ArrayList<Uri> list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        mEditTextNumber=findViewById(R.id.edittext_number);
        mEditTextMessage=findViewById(R.id.edittext_message);
    }

    public void sendSMS(View view) {

        String number=mEditTextNumber.getText().toString();
        String message=mEditTextMessage.getText().toString();


       Intent intent=new Intent(this,SendSMSActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent,0);

        SmsManager smsManager=SmsManager.getDefault();
        smsManager.sendTextMessage(number,null,message,pendingIntent,null);

    }

    public void openSMSApp(View view)
    {
        String number=mEditTextNumber.getText().toString();
        String message=mEditTextMessage.getText().toString();

        //simple text msg
        Intent intent=new Intent(Intent.ACTION_VIEW);

        intent.setData(Uri.parse("smsto:"+number));
        intent.putExtra("sms_body",message);
       // intent.putExtra(Intent.EXTRA_STREAM,mSelectedImage);
        if(intent.resolveActivity(getPackageManager())!=null)
        {
            startActivity(intent);
        }


        //text msg with attachment
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/*");
        //sendIntent.setData(Uri.parse("mmsto:8899665522"));
        sendIntent.putExtra("sms_body", "some text");
        sendIntent.putExtra(Intent.EXTRA_STREAM, mSelectedImage);

        if(sendIntent.resolveActivity(getPackageManager())!=null)
        {
            startActivity(sendIntent);
        }
    }

    public void selectAttachment(View view) {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , REQUEST_TAKE_PHOTO_GALLERY);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode)
        {

            case REQUEST_TAKE_PHOTO_GALLERY:
                if(data!=null)
                {
                    //uri of image is obtained from intent
                    mSelectedImage = data.getData();
                    list.add(mSelectedImage);
                    Toast.makeText(this,mSelectedImage.toString(), Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(this,"error", Toast.LENGTH_SHORT).show();
                }
                break;
        }



    }
}
