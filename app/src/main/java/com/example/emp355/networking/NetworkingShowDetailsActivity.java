package com.example.emp355.networking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.emp355.JSONUtils;
import com.example.emp355.R;
import com.example.emp355.adapter.SearchDetailsRecyclerAdapter;
import com.example.emp355.listener.OnItemClickListener;
import com.example.emp355.model.Address_Component;
import com.example.emp355.model.DataObject;
import com.example.emp355.model.Geometry;
import com.example.emp355.model.Location;
import com.example.emp355.model.MyObject;
import com.example.emp355.model.ObjectData;
import com.example.emp355.model.Open;
import com.example.emp355.model.OpeningHour;
import com.example.emp355.model.Period;
import com.example.emp355.model.Photo;
import com.example.emp355.model.Plus_Code;
import com.example.emp355.model.Prediction;
import com.example.emp355.model.Result;
import com.example.emp355.model.Review;
import com.example.emp355.model.ViewPort;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.emp355.Constant.FULL_DETAILS_DATA;
import static com.example.emp355.Constant.SEARCH_DATA;

public class NetworkingShowDetailsActivity extends AppCompatActivity implements OnItemClickListener{

    private SearchDetailsRecyclerAdapter mSearchRecyclerAdapter;
    private List<Prediction> mPredictionList;
    private ProgressDialog mProgressDialog;
   /* private static final String API_KEY="AIzaSyAPiG2fwDSl9qn5EhkY48EMFU67nKzOciI";*/

    //API  KEY
    private static final String API_KEY="AIzaSyDQnCy64XWPpTf27kT4Y2kulQULjCDMMpw";

    //BASE URL for places full details
    private static final String PLACE_DETAILS_URL="https://maps.googleapis.com/maps/api/place/details/json?key="+API_KEY+"&placeid=";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_show_details);

        RecyclerView searchRecyclerView;
        ObjectData objectData=null;

        searchRecyclerView=findViewById(R.id.recyclerview_searchdetails);

        Intent intent=getIntent();
        if(intent!=null)
        {
            //fetching the data from NetworkingAssign1Activity
            objectData=intent.getParcelableExtra(SEARCH_DATA);
            mPredictionList=objectData.getPredictions();
        }

        //setting the layout manager to vertical
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        searchRecyclerView.setLayoutManager(linearLayoutManager);

        //setting the obtained data to the adapter
        mSearchRecyclerAdapter=new SearchDetailsRecyclerAdapter(this,mPredictionList,this);
        searchRecyclerView.setAdapter(mSearchRecyclerAdapter);



    }

    //builds the final url
    private String returnFinalUrl(String text)
    {
        return PLACE_DETAILS_URL+text;

    }


    //Item click on any searched item
    @Override
    public void onItemClick(View view, int position) {

        switch (view.getId())
        {
            case R.id.linearlayout_searchdetails:

                Prediction prediction=mPredictionList.get(position);
                if(prediction!=null)
                {
                    String place_id=prediction.getPlace_id();

                    //check whether the clicked item has a place_id or not
                    if(!place_id.equals("place_id"))
                    {
                        String finalUrl=returnFinalUrl(place_id);
                        Log.d("finalURL", finalUrl);
                        if(checkInternetConnectivity())
                        {
                            //networking operation is carried out on a background thread
                            // for extracting full details of clicked place
                            new MyNewAsync().execute(finalUrl);
                        }
                    }

                }

                break;
        }


    }

    // Checks for the internet connection.
    private boolean checkInternetConnectivity()
    {
        // Get Connectivity Manager
        ConnectivityManager connectivityManager=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Details about the currently active default data network
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        if(networkInfo==null)
        {
            Toast.makeText(this, "No default network is currently active", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!networkInfo.isAvailable()) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_LONG).show();
            return false;
        }

        if(!networkInfo.isConnected())
        {
            Toast.makeText(this, "Network is not connected", Toast.LENGTH_LONG).show();
            return false;

        }

        Toast.makeText(this, "Network Connected", Toast.LENGTH_LONG).show();
        return true;

    }

    @Override
    public void onItemClick(View view, Cursor cursor) {

    }




    //async task to fetch data from url
    public class MyNewAsync extends AsyncTask<String,Void,MyObject>
    {
        //shows the progress dialog while fetching data from url
        @Override
        protected void onPreExecute() {
            mProgressDialog=new ProgressDialog(NetworkingShowDetailsActivity.this);
            mProgressDialog.setMessage("Fetching");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        //fetches the data from url
        @Override
        protected MyObject doInBackground(String... values) {

            InputStream inputStream=null;
            HttpURLConnection urlConnection=null;
            String response=null;
            MyObject myObject=null;

            try
            {
                /* forming the java.net.URL object */
                URL url=new URL(values[0]);

                urlConnection=(HttpURLConnection) url.openConnection();

                /* for Get request */
                urlConnection.setRequestMethod("GET");

                int statusCode=urlConnection.getResponseCode();

                Log.d("status", "doInBackground: "+statusCode);
                /* 200 represents HTTP OK */
                if(statusCode==200)
                {
                    inputStream=new BufferedInputStream(urlConnection.getInputStream());
                    response=convertInputStreamToString(inputStream);
                   myObject=jsonParsing(response);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            Log.d("resp", response);
            return myObject;

        }

        //parsing of json to java object
        private MyObject jsonParsing(String jsonStringData)
        {
            MyObject myObject=new MyObject();

            JSONObject jsonObject=JSONUtils.getJSONObject(jsonStringData);

            JSONArray html_attributionsarray =JSONUtils.getJSONArray(jsonObject,"html_attributions");

            List<String> html_attributionsList=new ArrayList<String>();

            for (int i=0;i < JSONUtils.getLengthofJSONArray(html_attributionsarray);i++)
            {

                html_attributionsList.add(JSONUtils.getStringObject(html_attributionsarray,i));
            }

            myObject.setHtml_attributions(html_attributionsList);


           // myObject.setNext_page_token(JSONUtils.getStringfromJSON(jsonObject,"next_page_token"));


            myObject.setStatus(JSONUtils.getStringfromJSON(jsonObject,"status"));
            myObject.setError_message(JSONUtils.getStringfromJSON(jsonObject,"error_message"));




            //JSONArray jsonResultArray = JSONUtils.getJSONArray(jsonObject,"results");

            JSONObject jsonResultObject=JSONUtils.getJSONObject(jsonObject,"result");

            //List<Result> resultList=new ArrayList<Result>();

           // for (int i = 0; i < JSONUtils.getLengthofJSONArray(jsonResultArray); i++)
            //{


               // JSONObject jsonResultObject =JSONUtils.getJSONObject(jsonResultArray,i);

                Result result = new Result();

                JSONArray jsonAddressCompArray=JSONUtils.getJSONArray(jsonResultObject,"address_components");

                List<Address_Component> addressCompList=new ArrayList<Address_Component>();

                for(int i=0;i<JSONUtils.getLengthofJSONArray(jsonAddressCompArray);i++)
                {
                    JSONObject jsonAddressCompObject =JSONUtils.getJSONObject(jsonAddressCompArray,i);

                    Address_Component address_component=new Address_Component();

                    address_component.setLong_name(JSONUtils.getStringfromJSON(jsonAddressCompObject,"long_name"));
                    address_component.setShort_name(JSONUtils.getStringfromJSON(jsonAddressCompObject,"short_name"));


                    JSONArray jsonTypesArray = JSONUtils.getJSONArray(jsonAddressCompObject,"types");


                    List<String> typesList=new ArrayList<String>();


                    for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonTypesArray); k++)
                    {

                        typesList.add(JSONUtils.getStringObject(jsonTypesArray,k));
                    }

                    address_component.setTypes(typesList);



                    addressCompList.add(address_component);

                }

                result.setAddress_components(addressCompList);


                result.setAdr_address(JSONUtils.getStringfromJSON(jsonResultObject,"adr_address"));

                result.setFormatted_address(JSONUtils.getStringfromJSON(jsonResultObject,"formatted_address"));



                JSONObject jsonGeometryObject =JSONUtils.getJSONObject(jsonResultObject,"geometry");


                Geometry geometry = new Geometry();


                JSONObject jsonLocationObject =JSONUtils.getJSONObject(jsonGeometryObject,"location");


                Location location = new Location();


                location.setLat(JSONUtils.getDoublefromJSON(jsonLocationObject,"lat"));

                location.setLng(JSONUtils.getDoublefromJSON(jsonLocationObject,"lng"));

                geometry.setLocation(location);


                JSONObject jsonViewPortObject = JSONUtils.getJSONObject(jsonGeometryObject,"viewport");

                ViewPort viewPort = new ViewPort();


                JSONObject jsonNorthEastObject = JSONUtils.getJSONObject(jsonViewPortObject,"northeast");

                Location northEast = new Location();


                northEast.setLat(JSONUtils.getDoublefromJSON(jsonNorthEastObject,"lat"));


                northEast.setLng(JSONUtils.getDoublefromJSON(jsonNorthEastObject,"lng"));
                viewPort.setNortheast(northEast);


                JSONObject jsonSouthWestObject =JSONUtils.getJSONObject(jsonViewPortObject,"southwest");

                Location southWest = new Location();


                southWest.setLat(JSONUtils.getDoublefromJSON(jsonSouthWestObject,"lat"));

                southWest.setLng(JSONUtils.getDoublefromJSON(jsonSouthWestObject,"lng"));

                viewPort.setSouthwest(southWest);


                geometry.setViewPort(viewPort);

                result.setGeometry(geometry);




                result.setIcon(JSONUtils.getStringfromJSON(jsonResultObject,"icon"));

                result.setId(JSONUtils.getStringfromJSON(jsonResultObject,"id"));

                result.setName(JSONUtils.getStringfromJSON(jsonResultObject,"name"));


            result.setPlace_id(JSONUtils.getStringfromJSON(jsonResultObject,"place_id"));

            result.setReference(JSONUtils.getStringfromJSON(jsonResultObject,"reference"));

            result.setScope(JSONUtils.getStringfromJSON(jsonResultObject,"scope"));





                JSONObject jsonOpeningHoursObject =JSONUtils.getJSONObject(jsonResultObject,"opening_hours");

                OpeningHour openingHour=new OpeningHour();


                openingHour.setOpen_now(JSONUtils.getBooleanfromJSON(jsonOpeningHoursObject,"open_now"));


                JSONArray jsonWeekdayArray=JSONUtils.getJSONArray(jsonOpeningHoursObject,"weekday_text");


                List<String> weekdayList=new ArrayList<String>();


                for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonWeekdayArray); k++)
                {

                    weekdayList.add(JSONUtils.getStringObject(jsonWeekdayArray,k));
                }

                openingHour.setWeekday_text(weekdayList);




                JSONArray jsonPeriodsArray=JSONUtils.getJSONArray(jsonOpeningHoursObject,"periods");

                List<Period> periodList=new ArrayList<Period>();


                for(int k = 0; k < JSONUtils.getLengthofJSONArray(jsonPeriodsArray); k++)
                {
                    JSONObject jsonPeriodsObject=JSONUtils.getJSONObject(jsonPeriodsArray,k);

                    Period period=new Period();

                    JSONObject jsonOpenObject=JSONUtils.getJSONObject(jsonPeriodsObject,"open");

                    Open open=new Open();
                    open.setDay(JSONUtils.getIntegerfromJSON(jsonOpenObject,"day"));
                    open.setTime(JSONUtils.getStringfromJSON(jsonOpenObject,"time"));

                    period.setOpen(open);

                    periodList.add(period);
                }

                openingHour.setPeriods(periodList);



                result.setOpening_hours(openingHour);






            JSONArray jsonTypesArray = JSONUtils.getJSONArray(jsonResultObject,"types");


            List<String> typesList=new ArrayList<String>();


            for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonTypesArray); k++)
            {

                typesList.add(JSONUtils.getStringObject(jsonTypesArray,k));
            }

            result.setTypes(typesList);







            result.setUrl(JSONUtils.getStringfromJSON(jsonResultObject,"url"));

            result.setUtc_offset(JSONUtils.getIntegerfromJSON(jsonResultObject,"utc_offset"));

            result.setVicinity(JSONUtils.getStringfromJSON(jsonResultObject,"vicinity"));





                JSONArray jsonPhotosArray = JSONUtils.getJSONArray(jsonResultObject,"photos");


                List<Photo> photoList=new ArrayList<Photo>();


                for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonPhotosArray); j++)
                {

                    JSONObject jsonPhotosObject = JSONUtils.getJSONObject(jsonPhotosArray,j);


                    Photo photo = new Photo();


                    photo.setPhoto_reference(JSONUtils.getStringfromJSON(jsonPhotosObject,"photo_reference"));


                    photo.setHeight(JSONUtils.getIntegerfromJSON(jsonPhotosObject,"height"));


                    photo.setWidth(JSONUtils.getIntegerfromJSON(jsonPhotosObject,"width"));



                    JSONArray jsonhtml_attributionsArray = JSONUtils.getJSONArray(jsonPhotosObject,"html_attributions");


                    List<String> html_attrList=new ArrayList<String>();


                    for (int k = 0; k < JSONUtils.getLengthofJSONArray(jsonhtml_attributionsArray); k++)
                    {

                        html_attrList.add(JSONUtils.getStringObject(jsonhtml_attributionsArray,k));
                    }

                    photo.setHtml_attributions(html_attrList);

                    photoList.add(photo);



                }


                result.setPhotos(photoList);





                JSONObject jsonPlusCodeObject=JSONUtils.getJSONObject(jsonResultObject,"plus_code");

                Plus_Code plus_code=new Plus_Code();

                plus_code.setCompound_code(JSONUtils.getStringfromJSON(jsonPlusCodeObject,"compound_code"));
                plus_code.setGlobal_code(JSONUtils.getStringfromJSON(jsonPlusCodeObject,"global_code"));

                result.setPlus_code(plus_code);




                result.setRating(JSONUtils.getDoublefromJSON(jsonResultObject,"rating"));









            JSONArray jsonReviewsArray = JSONUtils.getJSONArray(jsonResultObject,"reviews");


            List<Review> reviewsList=new ArrayList<Review>();


            for (int j = 0; j < JSONUtils.getLengthofJSONArray(jsonReviewsArray); j++)
            {

                JSONObject jsonReviewsObject = JSONUtils.getJSONObject(jsonReviewsArray,j);


                Review review = new Review();


                review.setAuthor_name(JSONUtils.getStringfromJSON(jsonReviewsObject,"author_name"));


                review.setAuthor_url(JSONUtils.getStringfromJSON(jsonReviewsObject,"author_url"));


                review.setLanguage(JSONUtils.getStringfromJSON(jsonReviewsObject,"language"));


                review.setProfile_photo_url(JSONUtils.getStringfromJSON(jsonReviewsObject,"profile_photo_url"));




                review.setRating(JSONUtils.getIntegerfromJSON(jsonReviewsObject,"rating"));


                review.setRelative_time_description(JSONUtils.getStringfromJSON(jsonReviewsObject,"relative_time_description"));


                review.setText(JSONUtils.getStringfromJSON(jsonReviewsObject,"text"));


                review.setTime(JSONUtils.getIntegerfromJSON(jsonReviewsObject,"time"));



                reviewsList.add(review);



            }


            result.setReviews(reviewsList);






                result.setWebsite(JSONUtils.getStringfromJSON(jsonResultObject,"website"));

            result.setFormatted_phone_number(JSONUtils.getStringfromJSON(jsonResultObject,"formatted_phone_number"));

            result.setInternational_phone_number(JSONUtils.getStringfromJSON(jsonResultObject,"international_phone_number"));

            result.setPrice_level(JSONUtils.getIntegerfromJSON(jsonResultObject,"price_level"));

                //resultList.add(result);

            //}

            myObject.setResultobj(result);


            return myObject;

        }


        //converts the inputstream from url to string
        private String convertInputStreamToString(InputStream inputStream)
        {
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            String line="";
            String result="";

            try
            {
                while((line=bufferedReader.readLine())!=null)
                {
                    result=result+line;
                }
                if(inputStream!=null)
                {
                    inputStream.close();
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            return result;
        }

        //dismisses the dialog
        @Override
        protected void onPostExecute(MyObject myObject) {
            mProgressDialog.dismiss();

            if(myObject!=null)
            {

                //control moves to next NetworkingFullDetailsActivity for showing all details
                if(myObject.getError_message().equals("error_message"))
                {
                    Intent intent=new Intent(NetworkingShowDetailsActivity.this,NetworkingFullDetailsActivity.class);
                    intent.putExtra(FULL_DETAILS_DATA, myObject);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(NetworkingShowDetailsActivity.this,myObject.getError_message(),Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(NetworkingShowDetailsActivity.this,"Failed to Fetch data",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }


    }

}
