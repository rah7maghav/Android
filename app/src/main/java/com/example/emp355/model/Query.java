package com.example.emp355.model;

import java.util.List;

public class Query {

    private List<Request> request;
    private List<Request> nextPage;

    public List<Request> getRequest() {
        return request;
    }

    public void setRequest(List<Request> request) {
        this.request = request;
    }

    public List<Request> getNextPage() {
        return nextPage;
    }

    public void setNextPage(List<Request> nextPage) {
        this.nextPage = nextPage;
    }
}
