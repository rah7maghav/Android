package com.example.emp355.roompersistencelib;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emp355.R;

import static com.example.emp355.Constant.ADAPTER_POSITION;
import static com.example.emp355.Constant.USER_CONTACT;
import static com.example.emp355.Constant.USER_EMAILID;
import static com.example.emp355.Constant.USER_ID;
import static com.example.emp355.Constant.USER_NAME;
import static com.example.emp355.Constant.USER_PASS;

public class UpdateUserActivity extends AppCompatActivity {

    private EditText mName,mEmail,mPassword,mContactNo;
    private UserDao userDao;
    private int listposition=0,userid=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        NewUser newUser=null;

        MyDatabase myDatabase;

        mName=findViewById(R.id.edittext_name);
        mEmail=findViewById(R.id.edittext_email);
        mPassword=findViewById(R.id.edittext_password);
        mContactNo=findViewById(R.id.edittext_contactno);

        //gets the instance of MyDatabase class
        myDatabase=MyDatabase.getInstance(getApplicationContext());
        userDao=myDatabase.userDao();

        if(getIntent()!=null)
        {
            userid=getIntent().getIntExtra(USER_ID,-2);
            listposition=getIntent().getIntExtra(ADAPTER_POSITION,-1);

        }


        new MyAsync(userid).execute(newUser);


    }

    //method for updating data
    //sends the new data to FetchUser Activity for updation
    public void updateData(View view)
    {

        Intent intent=new Intent(UpdateUserActivity.this,FetchUser.class);
        intent.putExtra(USER_NAME,mName.getText().toString());
        intent.putExtra(USER_EMAILID,mEmail.getText().toString());
        intent.putExtra(USER_PASS,mPassword.getText().toString());
        intent.putExtra(USER_CONTACT,mContactNo.getText().toString());
        intent.putExtra(USER_ID,userid);
        intent.putExtra(ADAPTER_POSITION,listposition);
        setResult(RESULT_OK,intent);
        finish();

    }

    public class MyAsync extends AsyncTask<NewUser,Void,NewUser>
    {

        int userid;

        public MyAsync(int userid) {
            this.userid = userid;
        }


        @Override
        protected NewUser doInBackground(NewUser... newUsers) {
            if(userid>0) {
                //fetches the current user specific to userid
                newUsers[0]=userDao.getUserById(userid);
            }


            return newUsers[0];
        }


        @Override
        protected void onPostExecute(NewUser newUser) {
            //sets the data to edittext fields of current user
            if(userid>0) {
                mName.setText(newUser.getName());
                mPassword.setText(newUser.getPassword());
                mEmail.setText(newUser.getEmail());
                mContactNo.setText(newUser.getContactno());
            }
        }


    }
}
