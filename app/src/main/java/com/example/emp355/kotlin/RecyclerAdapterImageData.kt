package com.example.emp355.kotlin

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.emp355.R
import kotlinx.android.synthetic.main.imagelayout.view.*
import java.util.ArrayList

class RecyclerAdapterImageData(context:Context,imageUrlList:ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mContext: Context? = null

    private var mImageUrllist: ArrayList<String>? = null

    init {
        mContext=context
        mImageUrllist=imageUrlList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view:View=LayoutInflater.from(mContext).inflate(R.layout.imagelayout,parent,false)

        return ImageHolder(view)

    }

    override fun getItemCount(): Int {

        return mImageUrllist!!.size

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        Glide.with(mContext!!).load(mImageUrllist!!.get(position)).into((holder as ImageHolder).itemView.imageview_data)
    }


    class ImageHolder(itemView:View): RecyclerView.ViewHolder(itemView) {

    }
}