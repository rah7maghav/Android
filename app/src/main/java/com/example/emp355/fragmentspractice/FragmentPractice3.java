package com.example.emp355.fragmentspractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.emp355.R;
import com.example.emp355.fragments.FifthFragment;
import com.example.emp355.fragments.FourthFragment;

public class FragmentPractice3 extends AppCompatActivity implements FourthFragment.SendMessage {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_practice3);


    }

    @Override
    public void sendData(String message) {
        FifthFragment fifthFragment = new FifthFragment();
        //fifthFragment.displayReceivedData(message);
        Bundle bundle = new Bundle();
        bundle.putString("data", message);
        // FifthFragment fifthFragment=new FifthFragment();
        fifthFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame,fifthFragment).commit();


    }
}
