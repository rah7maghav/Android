package com.example.emp355.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Period implements Parcelable {

    private Open open;

    public Open getOpen() {
        return open;
    }

    public void setOpen(Open open) {
        this.open = open;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.open, flags);
    }

    public Period() {
    }

    protected Period(Parcel in) {
        this.open = in.readParcelable(Open.class.getClassLoader());
    }

    public static final Parcelable.Creator<Period> CREATOR = new Parcelable.Creator<Period>() {
        @Override
        public Period createFromParcel(Parcel source) {
            return new Period(source);
        }

        @Override
        public Period[] newArray(int size) {
            return new Period[size];
        }
    };
}
