package com.example.emp355.networking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emp355.R;
import com.example.emp355.model.MyObject;
import com.example.emp355.model.Photo;
import com.example.emp355.model.Result;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.emp355.Constant.FULL_DETAILS_DATA;

public class NetworkingFullDetailsActivity extends AppCompatActivity {

    //API KEY
    private static final String API_KEY="AIzaSyDQnCy64XWPpTf27kT4Y2kulQULjCDMMpw";

    //BASE URL FOR PHOTO
    private static final String PHOTO_URL="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key="+API_KEY+"&photoreference=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_full_details);

        TextView name,address,telephone,website,types1;
        ImageView logo,background;
        MyObject myObject;

        name=findViewById(R.id.text_detailsname);
        address=findViewById(R.id.text_detailsaddress);
        telephone=findViewById(R.id.text_detailsphone);
        website=findViewById(R.id.text_detailswebsite);
        logo=findViewById(R.id.imageview_detailslogo);
        background=findViewById(R.id.imageview_detailsbackground);
        types1=findViewById(R.id.text_detailstypes1);



        Intent intent=getIntent();
        if(intent!=null)
        {
            //fetching the data from NetworkingShowDetailsActivity
            myObject=intent.getParcelableExtra(FULL_DETAILS_DATA);

            Result resultObj=myObject.getResultobj();

            //setting data to textviews
            if(!resultObj.getFormatted_address().equals("formatted_address"))
            {
                address.setText(resultObj.getFormatted_address());
            }

            if(!resultObj.getName().equals("name"))
            {
                name.setText(resultObj.getName());
            }

            if(!resultObj.getWebsite().equals("website"))
            {
                website.setText(resultObj.getWebsite());
            }

            if(!resultObj.getFormatted_phone_number().equals("formatted_phone_number"))
            {
                telephone.setText(resultObj.getFormatted_phone_number());
            }

            List<String> typesList=resultObj.getTypes();
            if(typesList!=null && typesList.size()>0)
            {
                String finaltext="";
                for(int i=0;i<typesList.size();i++)
                {
                    finaltext=finaltext+" #"+typesList.get(i);
                }
                types1.setText(finaltext);
            }




            List<Photo> photoList=resultObj.getPhotos();


            if(photoList!=null && photoList.size()>0)
            {
                //setting image view using universal image loader
                if(!photoList.get(0).getPhoto_reference().equals("photo_reference"))
                {
                    String finalUrl=returnFinalUrl(photoList.get(0).getPhoto_reference());
                  /*  Log.d("photo", finalUrl);
                    Picasso.get()
                            .load(finalUrl)//string url
                            .placeholder(R.mipmap.ic_launcher)//image on loading
                            .into(logo);//imageview for showing image*/

                    ImageLoader.getInstance()
                            .displayImage(finalUrl//string url
                                    ,logo);//imageview for showing image



                    String finalUrl1=returnFinalUrl(photoList.get(1).getPhoto_reference());
                    Log.d("photo", finalUrl1);
                    ImageLoader.getInstance()
                            .displayImage(finalUrl1//string url
                                    ,background);//imageview for showing image

/*
                    Picasso.get()
                            .load(finalUrl1)//string url
                            .placeholder(R.mipmap.ic_launcher)//image on loading
                            .into(background);//imageview for showing image*/
                }

            }


        }






    }

    //builds the final url for image
    private String returnFinalUrl(String text)
    {
        String finalUrl=PHOTO_URL+text;

        return finalUrl;

    }
}
