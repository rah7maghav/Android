package com.example.emp355.viewpager;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.emp355.R;
import com.example.emp355.adapter.ViewPagerAdapter;
import com.example.emp355.fragments.AppleFragment;
import com.example.emp355.fragments.MangoFragment;
import com.example.emp355.fragments.OrangeFragment;

import static com.example.emp355.Constant.APPLE;
import static com.example.emp355.Constant.MANGO;
import static com.example.emp355.Constant.ORANGE;

public class ViewPagerPractice1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_practice1);


        ViewPager viewPager=(ViewPager)findViewById(R.id.viewpager);
        addTabs(viewPager);

        TabLayout tabLayout=(TabLayout)findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

    }


    private void addTabs(ViewPager viewPager)
    {
        ViewPagerAdapter viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new MangoFragment(),MANGO);
        viewPagerAdapter.addFragment(new OrangeFragment(),ORANGE);
        viewPagerAdapter.addFragment(new AppleFragment(),APPLE);

        viewPager.setAdapter(viewPagerAdapter);

    }
}
