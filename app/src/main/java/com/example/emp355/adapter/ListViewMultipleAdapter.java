package com.example.emp355.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.emp355.R;

public class ListViewMultipleAdapter extends BaseAdapter {

    private Context mContext;

    public ListViewMultipleAdapter(Context context) {
        mContext = context;
    }


    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

   @Override
    public int getItemViewType(int position) {

        if(position==0)
        {
            return 0;
        }

       if (position % 5 == 0) {
           return 1;

       }
       return 0;

   }


    @Override
    public int getCount() {
        return 50;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int type=getItemViewType(position);
        if (convertView == null)

        {
            holder = new ViewHolder();
            //convertView = LayoutInflater.from(mContext).inflate(R.layout.item, parent, false);

            if(type==0)
            {
              convertView=  LayoutInflater.from(mContext).inflate(R.layout.item,parent,false);
                holder.nameText=(convertView.findViewById(R.id.text_id));
                holder.nameText.setText("Data at pos "+ position);
            }
            else
            {
               convertView= LayoutInflater.from(mContext).inflate(R.layout.shapes,parent,false);

            }

            convertView.setTag(holder);
        } else{
            holder = (ViewHolder) convertView.getTag();
            if (holder.nameText != null) {
                holder.nameText.setText("Data at pos "+ position);
            }

        }




        //((TextView) convertView.findViewById(R.id.text_id)).setText("Android :: " + position);
        return convertView;
    }

    public class ViewHolder {
        public TextView nameText;
    }



}