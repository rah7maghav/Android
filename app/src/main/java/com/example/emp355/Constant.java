package com.example.emp355;

public interface Constant {

    String APPLE="Apple";
    String MANGO="Mango";
    String ORANGE="Orange";


    String FINISH="Finish";
    String REMOVE="Remove";
    String BOOKMARK="Bookmark";

    String TEXT="Text";
    String DRAWABLE_ID="Drawable_id";


    String WASHINGTON_JOURNEY="Washington Journey";
    String SOUTH_WOLD="The Hauge to Southwold";
    String GOA_TOUR="Goa Tour";
    String DATE="Apr 3, 2016";

    String LIKES_145="145 likes";
    String LIKES_170="170 likes";
    String LIKES_154="154 likes";
    String SHARE="Share";
    String ITEM_LIST="Itemlist";


    String MY_COURSES="MYCOURSES";

    String BOOKMARKS="BOOKMARKED";
    String NEARBY="NEARBY";

    String YES="Yes";
    String CANCEL="Cancel";

    String NO="No";
    String CLICK_CANCEL="You Clicked Cancel";
    String CLICK_NO="You Clicked No";


    String ODD_NOT_ALLOWED="Odd Number not allowed";
    String TIMER_OVER="Timer Over";




    String FIRST_NAME="FirstName";
    String LAST_NAME="LastName";
    String USERNAME="UserName";
    String PHONE_NUMBER="PhoneNumber";
    String EMAIL="Email";
    String PASSWORD="Password";
    String REF_CODE="RefCode";

    String LOGIN_SHARED_PREFERENCE_FILE="LoginDetails";

    String EMAIL_ENTERED ="EmailEntered";
    String LIKES_NUMBER="NumberofLikes";
    String BACK_AGAIN="Press back again to exit";

    String USER_ID="id";
    String ADAPTER_POSITION="position";

    String USER_NAME ="Username";
    String USER_EMAILID="Useremail";
    String USER_PASS="Userpassword";
    String USER_CONTACT="Usercontact";

    String IMAGES_URL_DATA="ImagesData";

    String SEARCH_DATA="SearchData";
    String FULL_DETAILS_DATA="FullDetailsData";
    String IMAGE_URL="ImageUrl";
    String SEARCHED_TEXT="SearchedText";
    String IMAGE_POSITION="ItemPostion";
    String DOWNLOAD_MESSAGE="DownloadMessage";
    String DOWNLOAD_SUCCESS_MESSAGE="Image Downloaded and Saved Successfully";
    String DOWNLOAD_FAILURE_MESSAGE="Cannot Downloaded Image,Retry after sometime";
    String DOWNLOAD_ALREADY_MESSAGE="Image Already Download";
    String IMAGE_FILENAME="ImageFileName";

}
