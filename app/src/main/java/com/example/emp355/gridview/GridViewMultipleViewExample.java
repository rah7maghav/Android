package com.example.emp355.gridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import com.example.emp355.R;
import com.example.emp355.adapter.GridViewMultipleAdapter;
import com.example.emp355.recyclerview.RecyclerViewAssign1;

public class GridViewMultipleViewExample extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_multiple_view_example);


        GridView gridView=(GridView)findViewById(R.id.gridview2);

       GridViewMultipleAdapter adapter=new GridViewMultipleAdapter(this);
        gridView.setAdapter(adapter);
    }


    public void Next(View view)
    {
        Intent i=new Intent(this, RecyclerViewAssign1.class);
        startActivity(i);
    }


}
