package com.example.emp355.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emp355.R;
import com.example.emp355.adapter.RecyclerAdapterViewPager;

import java.util.ArrayList;
import java.util.List;

import static com.example.emp355.Constant.BOOKMARK;
import static com.example.emp355.Constant.DRAWABLE_ID;
import static com.example.emp355.Constant.FINISH;
import static com.example.emp355.Constant.GOA_TOUR;

import static com.example.emp355.Constant.ITEM_LIST;
import static com.example.emp355.Constant.LIKES_145;
import static com.example.emp355.Constant.LIKES_154;
import static com.example.emp355.Constant.LIKES_170;
import static com.example.emp355.Constant.REMOVE;
import static com.example.emp355.Constant.SOUTH_WOLD;
import static com.example.emp355.Constant.TEXT;
import static com.example.emp355.Constant.WASHINGTON_JOURNEY;


public class ViewPagerFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerAdapterViewPager recyclerAdapterViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        //String thirdtext=null;
        ArrayList<String> list = new ArrayList<String>();
        int thirdicon = 0;
        Bundle bundle = getArguments();
        if (bundle != null) {
            //  thirdtext=bundle.getString(TEXT);
            list = bundle.getStringArrayList(ITEM_LIST);
            thirdicon = bundle.getInt(DRAWABLE_ID);
        }

        recyclerView = (RecyclerView) view.findViewById(R.id.viewpagerrecyclerview);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerAdapterViewPager = new RecyclerAdapterViewPager(getActivity(), list, thirdicon);
        recyclerView.setAdapter(recyclerAdapterViewPager);


    }

}
